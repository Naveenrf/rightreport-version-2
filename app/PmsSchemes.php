<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmsSchemes extends Model
{

    protected $fillable = ['name','amc'];

    public function pmsCorporation(){
        return $this->belongsTo('App\PmsCorporation', 'corporation_id');
    }
}
