<?php

namespace App\Http\Controllers;

use App\BondDetails;
use App\BondInterest;
use App\BondMaster;
use App\PasswordManagement;
use App\PmsSchemes;
use App\Sip;
use App\WithdrawDetails;
use \DateTime;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Group;
use App\Person;
use App\InvestmentDetails;
use Carbon\Carbon;
use App\SchemeDetails;
use App\HistoricNavs;
use Illuminate\Support\Facades\Auth;
use PHPExcel_Calculation_Financial;
use Excel;
use App\GroupMembers;
use App\Queries;
use App\User;
use App\scheme_type;
use PDF;
use App\historic_nse;
use App\historic_bse;
use App\amcnames;
use App\BondRedemption;
use App\midcap;
use App\smallcap;
use \NumberFormatter;
use PhpParser\Node\Expr\AssignOp\Div;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use ZipArchive;
use Response;
use App\PmsCorporation;
use App\PmsDetails;
use App\Dividend;
use Exception;

class UserController extends Controller
{

    public $rbi_fd = [
        ['date' => '2015-01-15', 'value' => '7.75'],
        ['date' => '2015-03-04', 'value' => '7.50'],
        ['date' => '2015-06-02', 'value' => '7.25'],
        ['date' => '2015-09-29', 'value' => '6.75'],
        ['date' => '2016-04-05', 'value' => '6.50'],
        ['date' => '2016-10-04', 'value' => '6.25'],
        ['date' => '2016-12-07', 'value' => '6.25'],
        ['date' => '2017-02-08', 'value' => '6.25'],
        ['date' => '2017-04-06', 'value' => '6.25'],
        ['date' => '2017-06-08', 'value' => '6.25'],
        ['date' => '2017-08-02', 'value' => '6.20']
    ];


    public function index()
    {
        if (\Auth::user()) {
            $schemes = SchemeDetails::all();
            $groups = Group::where('user_id', \Auth::user()->id)->where('active_state',1)->get();
            $persons = Person::where('user_id', \Auth::user()->id)->where('active_state',1)->get();
            $group_members = GroupMembers::where('user_id', \Auth::user()->id)->where('active_state',1)->get();
            $twoday = Carbon::now()->addDays(2);
            $today = Carbon::now();

            $queries = Queries::
            where("date", "<", $twoday)
                ->where("date", ">", $today)
                ->get();


            return view('home')->with(compact('groups', 'persons', 'group_members', 'schemes', 'queries'));

        } else {
            return view('login');
        }
    }

    public function addGroup(Request $request)
    {

        $validator = \Validator::make($request->all(), [

            'group_name' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $group_name = $request['group_name'];
            $group = new Group();
            $group->name = $group_name;
            $group->user_id = \Auth::user()->id;
            if ($group->save()) {
                return $group;
            } else {
                return response()->json('save failed');
            }
        }
    }

    public function removeGroup(Request $request)
    {

        $validator = \Validator::make($request->all(), [

            'group_id' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            $investor_id = array();

            $remove_group = Group::where('id', $request['group_id'])->where('user_id', \Auth::user()->id)->delete();

            $group_mem_id = array();
            $group_mem_id = GroupMembers::where('user_id', \Auth::user()->id)->where('group_id', $request['group_id'])->pluck('id')->toArray();
            foreach ($group_mem_id as $member_id) {
                $investor_id[] = $member_id;
            }
            $remove_investment = InvestmentDetails::where('user_id', \Auth::user()->id)->whereIn('investor_id', $investor_id)
                ->where('investor_type', 'subperson')
                ->delete();

            /*$remove_investment = InvestmentDetails::where('investor_id',$request['group_id'])
				->where('investor_type','group')->where('user_id',\Auth::user()->id)->delete();*/
            if ($remove_group && $remove_investment) {
                return response()->json(['msg' => '1']);
            } else {
                return response()->json(['msg' => '0']);
            }

        }

    }

    //  public function addPerson(Request $request){

    //  	  $validator = \Validator::make($request->all(),[

    // 	'person_name' => 'required',

    // ]);

    // if ($validator->fails()) {

    // 	return redirect()->back()->withErrors($validator->errors());

    // }
    // else{

    // 	$person_name = $request['person_name'];
    // 	$person = new Person();
    // 	$person->name = $person_name;
    // 	$person->user_id = \Auth::user()->id;
    // 	if($person->save()){
    // 		return $person;
    // 	}
    // 	else{
    // 		return response()->json('save failed');
    // 	}

    // }
    //  }

    public function addPerson(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'inv_name' => 'required',
            'pan' => 'required',
            'email_id' => 'required',
            'mobile_number' => 'required',
            'address' => 'required',

        ]);
        if ($request['type'] == 'edit') {
            $group_member = GroupMembers::where('member_pan',$request['pan'])->where('active_state',1)->count();
            $person = Person::where('person_pan',$request['pan'])->where('active_state',1)->count();

            if($group_member > 0 || $person > 0){
                return response()->json(["msg" => 0, 'response' => 'PAN Already Exists.']);
            }
            if ($request['investor_type'] == 'individual') {
                $save = Person::where('id', $request['group_id2'])
                    ->update(['user_id' => \Auth::user()->id,
                        'name' => $request['inv_name'],
                        'person_pan' => $request['pan'],
                        'email' => $request['email_id'],
                        'contact' => $request['mobile_number'],
                        'address' => $request['address']
                    ]);
                if ($save == 1) {
                    return response()->json(["msg" => 1]);
                } else {
                    return response()->json(["msg" => 0, 'response' => 'Cannot add the Member now.']);
                }
            } else {
                $save = GroupMembers::where('id', $request['group_id2'])
                    ->update(['user_id' => \Auth::user()->id,
                        'member_name' => $request['inv_name'],
                        'member_pan' => $request['pan'],
                        'email' => $request['email_id'],
                        'contact' => $request['mobile_number'],
                        'address' => $request['address']
                    ]);
                if ($save == 1) {
                    return response()->json(["msg" => 1]);
                } else {
                    return response()->json(["msg" => 0, 'response' => 'Cannot add the Member now.']);
                }
            }
        } else {

            $group_member = GroupMembers::where('member_pan',$request['pan'])->where('active_state',1)->count();
            $person = Person::where('person_pan',$request['pan'])->where('active_state',1)->count();

            if($group_member > 0 || $person > 0){
                return response()->json(["msg" => 0, 'response' => 'PAN Already Exists.']);
            }

            if ($request['investor_type'] == 'individual') {
                $save = Person::insert(['user_id' => \Auth::user()->id,
                    'name' => $request['inv_name'],
                    'person_pan' => $request['pan'],
                    'email' => $request['email_id'],
                    'contact' => $request['mobile_number'],
                    'address' => $request['address']
                ]);
                if ($save == 1) {
                    return response()->json(["msg" => 1]);
                } else {
                    return response()->json(["msg" => 0, 'response' => 'Cannot add the Member now.']);
                }
            } else {
                $save = GroupMembers::insert(['user_id' => \Auth::user()->id,
                    'group_id' => $request['group_id2'],
                    'member_name' => $request['inv_name'],
                    'member_pan' => $request['pan'],
                    'email' => $request['email_id'],
                    'contact' => $request['mobile_number'],
                    'address' => $request['address']
                ]);
                if ($save == 1) {
                    return response()->json(["msg" => 1]);
                } else {
                    return response()->json(["msg" => 0, 'response' => 'Cannot add the Member now.']);
                }


            }
        }

        // dd($request['inv_name']);
    }


    public function removePerson(Request $request)
    {

        $validator = \Validator::make($request->all(), [

            'person_id' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $remove_person = Person::where('id', $request['person_id'])->delete();
            $remove_investment = InvestmentDetails::where('user_id', \Auth::user()->id)->where('investor_id', $request['person_id'])
                ->where('investor_type', 'individual')->delete();

            if ($remove_person && $remove_investment) {
                return response()->json(['msg' => '1']);
            } else {
                return response()->json(['msg' => '0']);
            }

        }

    }


    public function addInvestment(Request $request)
    {


        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',
            'scheme_name' => 'required',
            'dop' => 'required',
            'amt_inv' => 'required',
            'purchase_nav' => 'required',
            'folio_number' => 'required',

        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());

        } else {

            $scheme_name = SchemeDetails::where('scheme_code', $request['scheme_name'])->get();
            $date = date('Y-m-d', strtotime($request['dop']));
            $units = $request['amt_inv'] / $request['purchase_nav'];
            $investment = new InvestmentDetails();
            $investment->user_id = \Auth::user()->id;
            $investment->investor_id = $request['investor_id'];
            $investment->investor_type = $request['investor_type'];
            $investment->scheme_name = $scheme_name[0]['scheme_name'];
            $investment->scheme_code = $request['scheme_name'];
            $investment->scheme_type = $scheme_name[0]['scheme_type'];
            $investment->amc = $scheme_name[0]['amc_name'];
            $investment->amount_inv = $request['amt_inv'];
            $investment->investment_type = $request['inv_types'];
            $investment->purchase_date = $date;
            $investment->purchase_nav = $request['purchase_nav'];
            $investment->units = $units;
            $investment->folio_number = $request['folio_number'];
            $investment->is_notseen = 1;
            $investment->is_inv = 0;
            $investment->option_type = $request['option_type'];
            $investment->not_aum = 0;


            //print_r($last_inv);

            if ($investment->save()) {

            

                $latest_inv = InvestmentDetails::where('id', $investment->id)->get();

                $last_inv = array();
                $yesterday_date = Carbon::now()->subDay(1)->toDateString();
                $yesterday_date = date('Y-m-d', strtotime($yesterday_date));

                $profit_or_loss = 0;
                $total_amount = 0;
                $total_m_value = 0;
                $absolute_returns_avg = 0;
                $count = 0;
                foreach ($latest_inv as $investment) {

                    $last_inv['investor_id'] = $investment->investor_id;
                    $last_inv['investment_id'] = $investment->id;
                    $last_inv['investor_type'] = $investment->investor_type;

                    $last_inv['scheme_name'] = $investment->scheme_name;
                    $last_inv['scheme_type'] = $investment->scheme_type;
                    $last_inv['purchase_date'] = $investment->purchase_date;
                    $last_inv['amount_inv'] = $investment->amount_inv;
                    $last_inv['purchase_nav'] = $investment->purchase_nav;
                    $last_inv['units'] = $investment->units;
                    $current_nav = HistoricNavs::where('scheme_code', $investment->scheme_code)->orderBy('date', 'aesc')->first();
                    $last_inv['current_nav'] = $current_nav['nav'];

                    $current_m_value = $current_nav['nav'] * $investment->units;
                    $last_inv['current_m_value'] = $current_m_value;

                    $initial_value = $investment->amount_inv;
                    $last_inv['p_or_loss'] = round((($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav)), 2);
                    $last_inv['folio_number'] = $investment->folio_number;
                    $absolute_returns = (($current_m_value - $initial_value) / $initial_value) * 100;

                    $today = Carbon::now()->subDay(0)->toDateString();
                    $today = date('Y-m-d', strtotime($today));
                    $purchase_date = $investment->purchase_date;

                    $date_diff = date_diff(date_create($purchase_date), date_create($today));
                    $date_diff = $date_diff->format("%a");

                    if ($date_diff == 0) {
                        $annualised_return = 0;
                    } else {
                        $annualised_return = (($absolute_returns * 365) / $date_diff);
                    }


                    $last_inv['ann_returns'] = $annualised_return;
                    $last_inv['abs_returns'] = $absolute_returns;

                    $total_amount_invested =
                    $last_inv['total_amt_inv'] = $absolute_returns;


                }

                //$investor_id = $request['investor_id'];
                $investment_details = InvestmentDetails::where('investor_id', $request['investor_id'])->where('investor_type', $request['investor_type'])->where('user_id', \Auth::user()->id)->get();


                foreach ($investment_details as $investment) {

                    if ($investment['is_inv'] == 0) {
                        $total_amount += $investment->amount_inv;
                    }
                    $current_nav = HistoricNavs::where('scheme_code', $investment->scheme_code)->orderBy('date', 'aesc')->first();
                    $current_m_value = ($investment->units) * $current_nav['nav'];
                    $total_m_value += $current_m_value;
                    if ($investment['is_inv'] == 0) {
                        $profit_or_loss += (($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav));
                    }
                    $count++;
                    $initial_value = ($investment->purchase_nav) * ($investment->units);
                    $absolute_returns = (($current_m_value - $initial_value) / $initial_value) * 100;
                    // $absolute_returns_avg += $absolute_returns;
                    $count++;
                }
                $absolute_returns_avg = (($total_m_value - $total_amount) / $total_amount) * 100;
                $absolute_returns_avg = round($absolute_returns_avg, 2);
                //$current_m_value = round($current_m_value,2);
                $total_amount_inv = round($total_amount, 2);
                $profit_or_loss = round($profit_or_loss, 2);
                $total_m_value = round($total_m_value, 2);

                //echo $total_m_value;


                return response()->json(['msg' => '1', 'investment' => $last_inv, 'total_amount_inv' => $total_amount_inv, 'absolute_returns_avg' => $absolute_returns_avg, 'profit_or_loss' => $profit_or_loss, 'total_m_value' => $total_m_value]);
            } else {
                return response()->json(['msg' => '0']);
            }


        }
    }


    public function redeemBond(Request $request){

        $validator = \Validator::make($request->all(), [
            'redeem-bond-id' => 'required'

        ]);


        if ($validator->fails()) {
            // dd('sfdds');
            return redirect()->back()->withErrors($validator->errors());

        }

        $bond = BondDetails::where('id', $request['redeem-bond-id'])->first();

        if($bond->bond_payout_type == 'cumulative'){
            
            $realisedProfit = $this->getInterestAccumalated($bond);

        
        }else{

            $realisedProfit = BondInterest::where('bond_id',$request['redeem-bond-id'])->sum('interest_paid');
            
        }

        $bond->status = 0;
        $bond->save();

        $bondredemption = new BondRedemption();

        $bondredemption->bond_id = $bond->id;
        $bondredemption->realised_profit = $realisedProfit;
        $bondredemption->date = date('Y-m-d');

        $bondredemption->save();


        return response()->json(['msg'=> true, 'response' => 'Successfully Redeemed the bond.']);


    }

    public function getNav()
    {


        // Increasing the Maximum execution time and setting it to default value at End of the function

        /*$scheme_code contains the scheme code of the 20 funds. First 5 are Debt funds, Followed by 4 balanced fund, and 11 Equity funds*/

        $scheme_code = SchemeDetails::get()->pluck('scheme_code');

        // dd($scheme_code);


        //$scheme_code = array("106212","112423","114239","128053","111803","133805","133926",  "104685","100122","118191","131666","100081","100520","118102","100349","112090","105758","103360","113177","101672","104481","107745","103196","101979","100499","101350","101862","102142","103174","103504","106235","112092","112496","112600","112938","117716","123690","101537","112096","107249");

        $count = 0;
        // $file = file("http://portal.amfiindia.com/spages//NAV0.txt");
        $file = file("https://www.amfiindia.com/spages/NAVAll.txt");

        foreach ($scheme_code as $searchthis) {
            foreach ($file as $line) {
                if (strpos($line, $searchthis) !== FALSE) {
                    $line_exp = explode(";", $line);
                    $date = date("Y-m-d", strtotime($line_exp[5]));
                    \Log::info($date . ' : ' . $line_exp[0] . ' : ' . $line_exp[4]);
                    $store_nav = new HistoricNavs();
                    $store_nav->scheme_code = $line_exp[0];
                    $store_nav->scheme_name = $line_exp[3];

                    $store_nav->nav = $line_exp[4];
                    $store_nav->date = $date;
                    $store_nav->save();

                    //echo $line_exp[3]."<br>";

                    // Storing in historic_navs

                    /*$historic_navs = new HistoricNav();
                    $historic_navs->scheme_code = $line_exp[0];
                    $historic_navs->scheme_name = $line_exp[2];
                    $historic_navs->date = $date;
                    $historic_navs->nav = $line_exp[4];
                    $historic_navs->save();*/


                }
            }
        }
    }

    public function getInvestment(Request $request)
    {


        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',
            'date' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());
            //return "date varala da";

        } else {

            set_time_limit(600);

            $dividend_data = Dividend::where('investor_id',$request['investor_id'])->where('investor_type',$request['investor_type'])->get(['id','scheme_name','investment_date','investment_amount','dividend_date','per_unit','dividend_amount']);
            $dividend_sum = $dividend_data->sum('dividend_amount');
            // dd($dividend_sum);

            $myDate = date('Y-m-d', strtotime($request['date']));
//            $nifty_day_one = historic_nse::where('date', '<=', $myDate)->orderBy('date', 'desc')->first();
//            $nifty_day_two = historic_nse::where('date', '<', $nifty_day_one['date'])->orderBy('date', 'desc')->first();
//            $nifty_val = (($nifty_day_one['closing_index'] - $nifty_day_two['closing_index']) / $nifty_day_two['closing_index']) * 100;
//            $nifty_val = round($nifty_val, 2);
//
//            $sensex_day_one = historic_bse::where('date', '<=', $myDate)->orderBy('date', 'desc')->first();
//            $sensex_day_two = historic_bse::where('date', '<', $sensex_day_one['date'])->orderBy('date', 'desc')->first();
//            $sensex_val = (($sensex_day_one['closing_index'] - $sensex_day_two['closing_index']) / $sensex_day_two['closing_index']) * 100;
//            $sensex_val = round($sensex_val, 2);
//
//            $smallcap_day_one = smallcap::where('date', '<=', $myDate)->orderBy('date', 'desc')->first();
//            $smallcap_day_two = smallcap::where('date', '<', $smallcap_day_one['date'])->orderBy('date', 'desc')->first();
//            $smallcap_val = (($smallcap_day_one['close'] - $smallcap_day_two['close']) / $smallcap_day_two['close']) * 100;
//            $smallcap_val = round($smallcap_val, 2);
//
//            $midcap_day_one = midcap::where('date', '<=', $myDate)->orderBy('date', 'desc')->first();
//            $midcap_day_two = midcap::where('date', '<', $midcap_day_one['date'])->orderBy('date', 'desc')->first();
//            $midcap_val = (($midcap_day_one['close'] - $midcap_day_two['close']) / $midcap_day_two['close']) * 100;
//            $midcap_val = round($midcap_val, 2);

            // dd($midcap_day_one,$midcap_day_two);

            $investor_type = $request['investor_type'];
            $investor_id = $request['investor_id'];


            if ($investor_type == "subperson") {

                $pan = GroupMembers::where('user_id', \Auth::user()->id)->where('id', $investor_id)->pluck('member_pan');

            }


            if ($investor_type == "individual") {

                $pan = Person::where('user_id', \Auth::user()->id)->where('id', $investor_id)->pluck('person_pan');
                //echo "The pan is".$pan[0];
            }

            $yesterday_date = Carbon::now()->subDay(1)->toDateString();
            $yesterday_date = date('Y-m-d', strtotime($yesterday_date));


            if ($request['date'] == 'empty') {

                $investments = InvestmentDetails::where('investor_id', $request['investor_id'])
                    ->where('investor_type', $request['investor_type'])
                    ->where('purchase_date', '<=', $change_date)
                    ->get();

                $investments = $investments->filter(function ($investment) {
                    return $investment->status == 'active' || $investment->status == 'paractive';
                });

                // dd($investment);

                $total_amount = InvestmentDetails::where('investor_id', $request['investor_id'])
                    ->where('investor_type', $request['investor_type'])
                    ->where('purchase_date', '<=', $change_date)
                    ->where('is_inv', 0)
                    ->get();

                $total_amount = $total_amount->filter(function ($investment) {
                    return $investment->status == 'active' || $investment->status == 'paractive';
                });

                $total_amount = $total_amount->sum('amount_inv');

            }

            if ($request['date'] != 'empty') {
                $change_date = $request['date'];
                $change_date = date('Y-m-d', strtotime($change_date));

                    
                    $investments = InvestmentDetails::where('purchase_date', '<=', $change_date)
                    ->where('investor_id', $request['investor_id'])
                    ->where('investor_type', $request['investor_type'])
                    ->where('status', '!=', 'inactive')
                    ->orderBy('purchase_date')
                    ->get();

            //    dd($investments->count(), $change_date);

                $total_amount = InvestmentDetails::where('investor_id', $request['investor_id'])
                    ->where('status', '!=', 'inactive')
                    ->where('investor_type', $request['investor_type'])
                    ->where('purchase_date', '<=', $change_date)
                    ->where('is_inv', 0)
                    ->get()->sum('amount_inv');


            }


            $count = 0;
            $investment_details = array();
            $current_m_value = 0;
            $profit_or_loss = 0;
            //$total_amount = 0;
            $total_m_value = 0;
            $absolute_returns_avg = 0;
            $nav_date = '';

            if (count($investments) == 0) {


                $withdraw_details = WithdrawDetails::where('investor_id', $request['investor_id'])
                    ->where('investor_type', $request['investor_type'])
                    ->orderBy('withdraw_date')
                    ->get();

                if (count($withdraw_details) == 0) {
                    return response()->json(['msg' => '0', 'investment_details' => $investment_details, 'total_amount' => $total_amount, 'total_m_value' => $total_m_value, 'profit_or_loss' => $profit_or_loss, 'absolute_return_avg' => $absolute_returns_avg, 'pan' => $pan, 'inv_id' => $investor_id, 'inv_type' => $investor_type]);
                } else {
                    $realised_profit = 0;
                    foreach ($withdraw_details as $withdraw) {
                        $withdraw['abs_return'] = round((($withdraw->withdraw_amount - $withdraw->invested_amount) / $withdraw->invested_amount) * 100, 2);
                        $date_diff = date_diff(date_create($withdraw->purchase_date), date_create($withdraw->withdraw_date));
                        $date_diff = $date_diff->format("%a");

                        if ($date_diff == 0) {
                            $annualised_return = 0;
                        } else {
                            $annualised_return = (($withdraw['abs_return'] * 365) / $date_diff);
                        }
                        $withdraw['ann_returns'] = round($annualised_return, 2);
                        $withdraw['profit_or_loss'] = round($withdraw->withdraw_amount - $withdraw->invested_amount);
                        $realised_profit += ($withdraw['profit_or_loss'] - (2 * $withdraw->stt));


                    }
                    $realised_profit += $dividend_sum;
                    $net_amount_invested = $fmt->format($this->getNetAmountInvested($total_m_value, $realised_profit, $profit_or_loss));




                    return response()->json(['msg' => '1',
                        'investment_details' => $investment_details,
                        'total_amount' => $total_amount,
                        'total_m_value' => $total_m_value,
                        'profit_or_loss' => $profit_or_loss,
                        'absolute_return_avg' => $absolute_returns_avg,
                        'pan' => $pan,
                        'inv_id' => $investor_id,
                        'inv_type' => $investor_type,
                        'nav_date' => $nav_date,
//                        'sensex_val' => $sensex_val,
//                        'nifty_val' => $nifty_val,
                                'xirr_value'=>0,
//                        'smallcap' => $smallcap_val,
//                        'midcap' => $midcap_val,
                        'withdraw_details' => $withdraw_details,
                        'realised_p_or_loss' => $fmt->format($realised_profit),
                        'net_amount_invested' => $net_amount_invested,
                        'dividend' => $dividend_data
                    ]);
                }


            } else {

                foreach ($investments as $investment) {

                    $Scheme_sub = SchemeDetails::where('scheme_code', $investment->scheme_code)->value('scheme_sub');


                    $investment_details[$count]['investor_id'] = $request['investor_id'];
                    $investment_details[$count]['investor_type'] = $request['investor_type'];
                    $investment_details[$count]['investment_id'] = $investment->id;
                    $investment_details[$count]['scheme_name'] = $investment->scheme_name;
                    $investment_details[$count]['scheme_type'] = $Scheme_sub;
                    $investment_details[$count]['dop'] = $investment->purchase_date;
                    $investment_details[$count]['amount_inv'] = $fmt->format($investment->amount_inv);
                    $investment_details[$count]['purchase_nav'] = $fmt->format($investment->purchase_nav);
                    $investment_details[$count]['units'] = $fmt->format(round($investment->units, 4));
                    $investment_details[$count]['type'] = 'inv';
                    $investment_details[$count]['inv_type'] = $investment->investment_type;
                    $investment_details[$count]['status'] = $investment->status;
                    $investment_details[$count]['trail_fee_percent'] = $investment->trail_fee_percent;
                    $investment_details[$count]['upfront_fee_percent'] = $investment->upfront_fee_percent;
                    $investment_details[$count]['last_fee_update'] = ($investment->last_fee_update != null ? date('d-m-Y', strtotime($investment->last_fee_update)) : null );
                    // $investment_details[$count]['nifty'] = $current_nifty_val;
                    $investment_details[$count]['acc_statement'] = $investment->acc_statement;




                    if ($request['date'] == 'empty') {
                        $current_nav = HistoricNavs::where('scheme_code', $investment->scheme_code)->orderBy('date', 'aesc')->first();

                        $nav_date = $current_nav['date'];
                        $current_nav = $current_nav['nav'];

                    }

                    if ($request['date'] != 'empty') {

                        // dd('hello');

                        $change_date = $request['date'];


                        $change_date = date('Y-m-d', strtotime($change_date));
                        //return $change_date;
                        //return $change_date;
                        $current_nav = HistoricNavs::where('scheme_code', $investment->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();

                        $nav_date = $current_nav['date'];
                        $current_nav = $current_nav['nav'];

                        $nav_date_one = HistoricNavs::where('scheme_code', $investment->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'desc')->first();

                        $nav_date_two = HistoricNavs::where('scheme_code', $investment->scheme_code)->where('date', '<', $nav_date_one['date'])->orderBy('date', 'desc')->first();
                        
                        try{
                            $navState = (($nav_date_one['nav'] - $nav_date_two['nav']) / $nav_date_two['nav']) * 100;

                        }catch(\Exception $e){
                            dd($nav_date_two, $investment);
                        }
                        
                        $navState = round($navState, 2);
                    }

                    $nav_date = date('d-m-Y', strtotime($nav_date));

                    $profi_loss = round((($investment->units) * $current_nav) - (($investment->units) * ($investment->purchase_nav)), 2);
                    $investment_details[$count]['current_nav'] = $current_nav;
                    $investment_details[$count]['current_market_value'] = $fmt->format(round(($investment->units) * $current_nav, 2));
                    $investment_details[$count]['folio_number'] = $investment->folio_number;
                    $investment_details[$count]['p_or_loss'] = $fmt->format($profi_loss);

                    if ($investment->status == "paractive") {
                        $investment_details[$count]['status'] = "(Partially Withdrawn)";
                    } else {
                        $investment_details[$count]['status'] = '';
                    }

                    $current_m_value = ($investment->units) * $current_nav;
                    $total_m_value += $current_m_value;



                    $initial_value = ($investment->purchase_nav) * ($investment->units);
                    $absolute_returns = (($current_m_value - $initial_value) / $initial_value) * 100;


                    $investment_details[$count]['abs_returns'] = round($absolute_returns, 2);



                    $today = Carbon::now()->subDay(0)->toDateString();
                    $today = date('Y-m-d', strtotime($today));
                    $purchase_date = $investment->purchase_date;


                    $date_diff = date_diff(date_create($purchase_date), date_create($today));
                    $date_diff = $date_diff->format("%a");



                    if ($date_diff == 0) {
                        $annualised_return = 0;
                    } else {
                        $annualised_return = (($absolute_returns * 365) / $date_diff);
                    }
                    $investment_details[$count]['ann_returns'] = round($annualised_return, 2);


                    $count++;
                }

                //echo ($absolute_returns_avg/$count);
                $profit_or_loss = $total_m_value - $total_amount;

                $absolute_returns_avg = (($total_m_value - $total_amount) / $total_amount) * 100;

                $absolute_returns_avg = round(($profit_or_loss / $total_amount) * 100, 2);
                $current_m_value = round($current_m_value, 2);
                $profit_or_loss = round($profit_or_loss, 2);
                $total_m_value = round($total_m_value, 2);




                $withdraw_details = WithdrawDetails::where('investor_id', $request['investor_id'])
                    ->where('investor_type', $request['investor_type'])
                    ->orderBy('withdraw_date')
                    ->get();

//				dd('hello');
                $realised_profit = 0;
                foreach ($withdraw_details as $withdraw) {

                    
                    if($withdraw->invested_amount != 0){
                        $withdraw['abs_return'] = round((($withdraw->withdraw_amount - $withdraw->invested_amount) / $withdraw->invested_amount) * 100, 2);

                    }else{
                        $withdraw['abs_return'] = 0;

                    }
                    $date_diff = date_diff(date_create($withdraw->purchase_date), date_create($withdraw->withdraw_date));
                    $date_diff = $date_diff->format("%a");

                    if ($date_diff == 0) {
                        $annualised_return = 0;
                    } else {
                        $annualised_return = (($withdraw['abs_return'] * 365) / $date_diff);
                    }
                    $withdraw['ann_returns'] = round($annualised_return, 2);
                    $withdraw['profit_or_loss'] = round($withdraw->withdraw_amount - $withdraw->invested_amount);
                    $withdraw['acc_statement'] = $withdraw->acc_statement;


                    $realised_profit += ($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount;

                }




                $xirr_Val = 0;
                $realised_profit += $dividend_sum;
                $net_amount_invested = $fmt->format($this->getNetAmountInvested($total_m_value, $realised_profit, $profit_or_loss));

                $total_amount = $fmt->format($total_amount);
                $total_m_value = $fmt->format($total_m_value);
                $profit_or_loss = $fmt->format($profit_or_loss);

                return response()->json(['msg' => '1',
                    'investment_details' => $investment_details,
                    'total_amount' => $total_amount,
                    'total_m_value' => $total_m_value,
                    'profit_or_loss' => $profit_or_loss,
                    'absolute_return_avg' => $absolute_returns_avg,
                    'pan' => $pan,
                    'inv_id' => $investor_id,
                    'inv_type' => $investor_type,
                    'nav_date' => $nav_date,
//                    'sensex_val' => $sensex_val,
//                    'nifty_val' => $nifty_val,
                    'xirr_value' => 0,
//                    'smallcap' => $smallcap_val,
//                    'midcap' => $midcap_val,
                    'withdraw_details' => $withdraw_details,
                    'realised_p_or_loss' => $fmt->format($realised_profit),
                    'net_amount_invested' => $net_amount_invested,
                    'dividend' => $dividend_data,
                ]);
            }


        }

    }

    public function downloadInvestments(Request $request)
    {

        ini_set('memory_limit', '-1');


        set_time_limit(600);
        $cis = $_SERVER['DOCUMENT_ROOT'] . '/cis.zip';
        $is = $_SERVER['DOCUMENT_ROOT'] . '/Investment Summary.pdf';
        if (file_exists($cis)) {
            unlink($cis);
        }
        if (file_exists($is)) {
            unlink($is);
        }

        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',
            'ex_nav_date' => 'required',
        ]);

        if ($validator->fails()) {


            dd($validator->errors());
            return redirect()->back()->withErrors($validator->errors());

        } else {



            $investor_id = $request['investor_id'];
            $investor_type = $request['investor_type'];

            $download_option = $request['ex_download_option'];
            $download_format = $request['ex_download_format'];

            if ($investor_type == 'cis') {

                $group_id = $request['investor_id'];
                $group_mem_id = array();
                $group_mem_id = GroupMembers::where('group_id', $group_id)->pluck('id')->toArray();

                $count = 0;

                $dividends_paid = Dividend::whereIn('investor_id', $group_mem_id)->where('investor_type', 'subperson')->orderBy('dividend_date','asc')->get();

                $cis_dividends = $dividends_paid->groupBy('scheme_name');

                $cis_dividends_pass = [];

                foreach ($cis_dividends as $scheme_name => $dividends){
                    foreach ($dividends as $dividend){


                        if(array_key_exists($dividend->investment_id, $cis_dividends_pass)){
                            $cis_dividends_pass[$dividend->investment_id]['scheme_name'] = $dividend->scheme_name;
                            $cis_dividends_pass[$dividend->investment_id]['investment_amount'] += $dividend->investment_amount;
                            $cis_dividends_pass[$dividend->investment_id]['dividend_paid'] += $dividend->dividend_amount;

                        }else{
                            $cis_dividends_pass[$dividend->investment_id]['scheme_name'] = $dividend->scheme_name;
                            $cis_dividends_pass[$dividend->investment_id]['investment_amount'] = $dividend->investment_amount;
                            $cis_dividends_pass[$dividend->investment_id]['dividend_paid'] = $dividend->dividend_amount;
                        }
                    }
                }


                $group_pms_investment = PmsDetails::whereIn('pmsinvestor_id',$group_mem_id)->where('pmsinvestor_type', 'App\GroupMembers')->get()->groupBy('name');
                $pms_pass_entry = [];

                foreach($group_pms_investment as $pms_name => $pms_entry){

                    $pms_pass_entry[] = array(
                        'name' => $pms_name,
                        'investment_amount' => $pms_entry->sum('amount_invested'),
                        'realised_profit' => ($pms_entry->sum('current_value') - $pms_entry->sum('amount_invested')),
                        'current_value' => $pms_entry->sum('current_value'),
                    );


                }


                $group_bond_investment = BondDetails::whereIn('bondinvestor_id',$group_mem_id)
                ->where('bondinvestor_type', 'App\GroupMembers')
                ->where('status','=','1')
                ->get()->groupBy('name');
                $bond_pass_entry = [];


                foreach($group_bond_investment as $bond_name => $bond_entry){
                    $interest_accumalated = 0;
                    $interest_paid = 0;


                    foreach($bond_entry as $bond_inv){
                        $interest_accumalated += $this->getInterestAccumalated($bond_inv);
                        $interest_paid += $bond_inv->interestPaid()->sum('interest_paid');
                    }

                    $bond_pass_entry[] = array(
                        'name' => $bond_name,
                        'investment_amount' => $bond_entry->sum('amount_invested'),
                        'realised_profit' => $interest_paid,
                        'current_value' => $interest_accumalated + $bond_entry->sum('amount_invested'),
                        'interest_accumalated' => $interest_accumalated,
                    );


                }



                $grand_total = 0;
                $grand_market_value = 0;

                $investor_id = array();
                $separate_inv = array();


                $inv_pass = array();

                $change_date = date('Y-m-d');
                $consolidatedInvestment = [];
                foreach ($group_mem_id as $member_id) {
                    $investor_id[] = $member_id;
                    $consolidatedInvestment[$member_id]['SIP'] = 0;
                    $consolidatedInvestment[$member_id]['Mutual Funds'] = 0;
                    $consolidatedInvestment[$member_id]['Liquid Funds'] = 0;
                    $consolidatedInvestment[$member_id]['Fixed Income Product'] = 0;

                    $consolidatedGroupInvestment['SIP'] = 0;
                    $consolidatedGroupInvestment['Mutual Funds'] = 0;
                    $consolidatedGroupInvestment['Liquid Funds'] = 0;
                    $consolidatedGroupInvestment['Fixed Income Product'] = 0;
                }


                // Group id 6 is for Excel group since they wanted CIS report in Scheme Name order and rest of them in date wise order
                if ($group_id == 6) {
                    $investment_detail = InvestmentDetails::where('status', '!=', 'inactive')->whereIn('investor_id', $investor_id)->where('investor_type', 'subperson')->where('purchase_date', '<=', $change_date)->orderBy('scheme_name', 'ASC')->get();
                } else {
                    $investment_detail = InvestmentDetails::where('status', '!=', 'inactive')->whereIn('investor_id', $investor_id)->where('investor_type', 'subperson')->where('purchase_date', '<=', $change_date)->orderBy('purchase_date', 'asc')->get();

                }


                $today = Carbon::now()->subDays(0)->toDateString();


                foreach ($investment_detail as $sep_inv) {

                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Scheme Name'] = $sep_inv->scheme_name;
                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Amount Invested'] = $sep_inv->amount_inv;
                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date'] = date('d-M-Y', strtotime($sep_inv->purchase_date));

                    $current_nav = HistoricNavs::where('scheme_code', $sep_inv->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();

                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase NAV'] = round($sep_inv->purchase_nav, 4);
                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Units'] = round($sep_inv->units, 3);

                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current NAV'] = round($current_nav['nav'], 4);



                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'] = round($current_nav['nav'] * $sep_inv->units, 2);

                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'] = round(($current_nav['nav'] * $sep_inv->units) - ($sep_inv->amount_inv), 2);

                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss']) / ($sep_inv->amount_inv)) * 100, 2);


                    $date_diff = date_diff(date_create($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date']), date_create($today));
                    $date_diff = $date_diff->format("%a");
                    if ($date_diff == 0) {
                        $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = 0;
                    } else {
                        $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns']) * (365 / $date_diff)), 2);
                    }

                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Folio Number'] = $sep_inv->folio_number;
                    $separate_inv[$sep_inv->investor_id][$sep_inv->id]['invest_type'] = $sep_inv->investment_type;

                    $grand_market_value += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

//                    $grand_market_value = round($grand_market_value, 2);

                    if($sep_inv->investment_type == 2){
                        //SIP
                        $consolidatedInvestment[$sep_inv->investor_id]['SIP'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                        $consolidatedGroupInvestment['SIP'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                    }else if($sep_inv->investment_type == 3){
                        $consolidatedInvestment[$sep_inv->investor_id]['Liquid Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                        $consolidatedGroupInvestment['Liquid Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                    }else{
                        //Onetime
                        if($sep_inv->scheme_type == 'equity'){
                            $consolidatedInvestment[$sep_inv->investor_id]['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                            $consolidatedGroupInvestment['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                        }else if($sep_inv->scheme_type == 'balanced'){
                            $consolidatedInvestment[$sep_inv->investor_id]['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                            $consolidatedGroupInvestment['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                        }else{
                            $consolidatedInvestment[$sep_inv->investor_id]['Fixed Income Product'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                            $consolidatedGroupInvestment['Fixed Income Product'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                        }
                    }

                }


                // Right till here
//                 Grand Market value = SIP + MF + Liquid + FIP


                foreach ($consolidatedInvestment as $id => $investment){


                    $bond_entry = BondDetails::where('bondinvestor_id',$id)->where('bondinvestor_type', 'App\GroupMembers')->where('status','=','1')->get();
                    $interest_accumalated = 0;
                    $interest_paid = 0;


                    foreach($bond_entry as $bond_inv){

                        if($bond_inv->bondRedemptions != null){
                            $realised_profit_or_loss += BondRedemption::with('bonds')->where('bond_id',$bond_inv->id)->value('realised_profit'); 
                        }

                        $interest_accumalated = $this->getInterestAccumalated($bond_inv);
                        $consolidatedInvestment[$id]['Fixed Income Product'] += $interest_accumalated + $bond_inv->amount_invested;
                        $consolidatedGroupInvestment['Fixed Income Product'] += $interest_accumalated + $bond_inv->amount_invested;

                    }
                }

                //Consolidated Data calculation should get over here.

                if(count(array_diff_key(array_flip($investor_id), $separate_inv)) > 0){
                    foreach(array_flip(array_diff_key(array_flip($investor_id), $separate_inv)) as $difference_key){
                        $separate_inv[$difference_key] = array();
                    }
                }



                foreach (array_keys($separate_inv) as $id){
                    $separate_inv[$id]['one_time'] = InvestmentDetails::where('investor_id', $id)->where('investor_type', 'subperson')->where('status', '!=', 'inactive')->where('investment_type', 1)->count();

                    $separate_inv[$id]['sip'] = InvestmentDetails::where('investor_id', $id)->where('investor_type', 'subperson')->where('status', '!=', 'inactive')->where('investment_type', 2)->count();

                    $separate_inv[$id]['liquid'] = InvestmentDetails::where('investor_id', $id)->where('investor_type', 'subperson')->where('status', '!=', 'inactive')->where('investment_type', 3)->count();

                }



                foreach ($investment_detail as $investments) {
                    if (array_key_exists($investments->scheme_code, $inv_pass)) {


                        $inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

                        $inv_pass[$investments->scheme_code]['Amount Invested'] += $investments->amount_inv;

                        $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();

                        $inv_pass[$investments->scheme_code]['Units'] += round(($investments->units), 4);
                        $inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

                        $current_market_value = (($inv_pass[$investments->scheme_code]['Units']) * $current_nav['nav']);

                        $current_market_value = round($current_market_value, 2);
                        $inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

                        $inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

                        $inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested']) / ($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100, 2);


                    } else {

                        $inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

                        $inv_pass[$investments->scheme_code]['Amount Invested'] = $investments->amount_inv;

                        $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
                        $inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

                        $inv_pass[$investments->scheme_code]['Units'] = round(($investments->units), 4);

                        $current_market_value = (($investments->units) * $current_nav['nav']);

                        $current_market_value = round($current_market_value, 2);

                        $inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

                        $inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

                        $inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested']) / ($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100, 2);


                    }

                    $grand_total += $investments->amount_inv;

                    $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
                    $current_nav = $current_nav['nav'];
                    $units_held = $investments->units;

                    $current_market_value = $units_held * $current_nav;

//                    $grand_market_value += $current_market_value;
//
//                    $grand_market_value = round($grand_market_value, 2);


                    $count++;
                }



                $profit_or_loss_total = $grand_market_value - $grand_total;

                $abs_total = (($grand_market_value - $grand_total) / $grand_total) * 100;
                $abs_total = round($abs_total, 2);

                $cis_wd_export = [];
                $realised_profit_or_loss = 0;
                foreach ($investor_id as $id) {
                    $withdraw_details = WithdrawDetails::where('investor_id', $id)->where('investor_type', 'subperson')->orderBy('withdraw_date','asc')->get();
                    $wd_export = [];
                    foreach ($withdraw_details as $withdraw) {
                        $realised_profit_or_loss += ($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount;

                        $cis_wd_export[] = array(
                            'Investor Name' => GroupMembers::where('id', $withdraw->investor_id)->value('member_name'),
                            'Scheme Name' => $withdraw->scheme_name,
                            'Amount Invested' => $withdraw->invested_amount,
                            'Purchase Date' => $withdraw->purchase_date,
                            'Purchase NAV' => $withdraw->purchase_nav,
                            'Units' => $withdraw->units,
                            'Redemption NAV' => bcdiv(($withdraw->withdraw_amount - $withdraw->stt) / $withdraw->units, 1, 4),
                            'Withdraw Value' => $withdraw->withdraw_amount,
                            'Realised Profit/Loss' => (($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount),
                            'Redemption Date' => $withdraw->withdraw_date,
                            //                                       'Absolute Returns' => round((($withdraw->withdraw_amount - $withdraw->invested_amount)/$withdraw->invested_amount)*100, 2),
                            'Exit Load' => $withdraw->exit_load,
                            'STT' => $withdraw->stt,
                            'Folio Number' => $withdraw->folio_number,

                        );
                    }
                }



                $mutual_fund_inv_total = $grand_total;
                $mutual_fund_current_value = $grand_market_value;
                $mutual_fund_unreal_p_or_loss = $profit_or_loss_total;






                foreach($bond_pass_entry as $bond){
                    $realised_profit_or_loss += $bond['realised_profit'];
                    $grand_market_value += ($bond['investment_amount'] + $bond['interest_accumalated']);
                    $grand_total += $bond['investment_amount'];

                    //Adding Bond Interest Accumalated to Unrealised Profit or Loss
                    $profit_or_loss_total += $bond['interest_accumalated'];
                

                }

                


                foreach($pms_pass_entry as $pms){
                    $grand_market_value += $pms['current_value'];
                    $grand_total += $pms['investment_amount'];

                    $profit_or_loss_total += ($pms['current_value'] - $pms['investment_amount']);
                }


                $realised_profit_or_loss += $dividends_paid->sum('dividend_amount');

                //                            dd($inv_pass);

//                $investmentSub = [];
//
//                foreach ($inv_pass as $schemeCode => $investment){
//                    $schemeType = SchemeDetails::where('scheme_code', $schemeCode)->value('scheme_type');
//                    $investmentSub[$schemeType][] = $investment;
//                }

//                dd($grand_market_value, $consolidatedGroupInvestment, array_sum(array_values($consolidatedGroupInvestment)));


//                return PDF::loadView('mastercispdf', [
//                    'inv' => $inv_pass,
//                    'download_format' => $request['ex_download_format'],
//                    'download_option' => $request['ex_download_option'],
////                    'invSub' => $investmentSub ,
//                    'dividends'=> $cis_dividends_pass,
//                    'mf_inv_total'=>$mutual_fund_inv_total,
//                    'mf_current_value'=> $mutual_fund_current_value,
//                    'mf_p_or_loss'=>$mutual_fund_unreal_p_or_loss,
//                    'bonds' => $bond_pass_entry,
//                    'pms' => $pms_pass_entry,
//                    'date' => $change_date,
//                    'total' => $grand_total,
//                    'current' => $grand_market_value,
//                    'unreal_pl' => $profit_or_loss_total,
//                    'real_pl' => $realised_profit_or_loss,
//                    'abs' => $abs_total,
//                    'consolidated_investment' => $consolidatedInvestment,
//                    'consolidated_group_inv' => $consolidatedGroupInvestment,
//                    'group_id' => $group_id,
//                ])->setPaper('a4', 'landscape')->setWarnings(false)->stream('CIS Report.pdf');

                $pdf = PDF::loadView('mastercispdf', [
                    'inv' => $inv_pass,
                    'dividends'=> $cis_dividends_pass,
                    'mf_inv_total'=>$mutual_fund_inv_total,
                    'mf_current_value'=> $mutual_fund_current_value,
                    'mf_p_or_loss'=>$mutual_fund_unreal_p_or_loss,
                    'bonds' => $bond_pass_entry,
                    'pms' => $pms_pass_entry,
                    'date' => $change_date,
                    'total' => $grand_total,
                    'current' => $grand_market_value,
                    'unreal_pl' => $profit_or_loss_total,
                    'real_pl' => $realised_profit_or_loss,
                    'group_id' => $group_id,
                    'consolidated_investment' => $consolidatedInvestment,
                    'consolidated_group_inv' => $consolidatedGroupInvestment,
                    'download_format' => $request['ex_download_format'],
                    'download_option' => $request['ex_download_option'],
                    'abs' => $abs_total])->setPaper('a4', 'landscape')->setWarnings(false)->save('CIS Report.pdf');


                $names = GroupMembers::whereIn('id', array_keys($separate_inv))->pluck('member_name');

                $file_content = \Excel::create('CIS Report', function ($excel) use ($inv_pass, $cis_wd_export, $separate_inv, $change_date, $names, $download_option) {
                    $excel->sheet('CIS Report', function ($sheet) use ($inv_pass, $cis_wd_export) {
                        $sheet->fromArray($inv_pass);
                        $sheet->setWidth('A', 50);
                        $sheet->setWidth('B', 15);
                        $sheet->setWidth('C', 20);
                        $sheet->setWidth('D', 18);
                        $sheet->setWidth('E', 20);
                        $sheet->setWidth('F', 20);
                        $sheet->setWidth('G', 20);


                        $sheet->fromArray($cis_wd_export);
                        $sheet->setWidth('A', 20);
                        $sheet->setWidth('B', 50);
                        $sheet->setWidth('C', 20);
                        $sheet->setWidth('D', 15);
                        $sheet->setWidth('E', 15);
                        $sheet->setWidth('F', 15);
                        $sheet->setWidth('G', 15);
                        $sheet->setWidth('H', 25);
                        $sheet->setWidth('I', 25);
                        $sheet->setWidth('J', 18);
                        $sheet->setWidth('K', 25);
                        $sheet->setWidth('L', 20);


                    });

                    $bondRedemptions = [];


                    foreach ($separate_inv as $investor_id => $inv) {


                        //Dividends Calculation here

                        $dividends = Dividend::where('investor_id', $investor_id)->where('investor_type', 'subperson')->orderBy('dividend_date','asc')->get()->toArray();

                        //bond calculation starts here


                        $bond_overall_details = $this->calculateBondOverallDetails($investor_id, 'App\GroupMembers');


                        $bond_investor = GroupMembers::where('id', $investor_id)->first();


                        $bond_investment = $bond_investor->bondInvestment;

                        foreach ($bond_investment as $bond_inv){

                            if($bond_inv->bondRedemptions != null){
                                $bondRedemptions[] = BondRedemption::with('bonds')->where('bond_id',$bond_inv->id)->first(); 
                            }

                            unset($bond_inv['interest_paid']);
                            $bond_inv['interest_accumalated'] = $this->getInterestAccumalated($bond_inv);
                            $bond_inv['total_interest_paid'] = $bond_inv->interestPaid()->sum('interest_paid');
                        }


                        $bond_paid_out_interest = [];
                        $count = 0;

                        foreach ($bond_investment as $bond_inv){
//                dd($inv->interestPaid);

                            foreach ($bond_inv->interestPaid as $interest){
                                $bond_paid_out_interest[$count]['id'] = $interest->id;
                                $bond_paid_out_interest[$count]['bond_name'] = $bond_inv->name;
                                $bond_paid_out_interest[$count]['bond_id'] = $bond_inv->id;
                                $bond_paid_out_interest[$count]['investment_amount'] = $bond_inv->amount_invested;
                                $bond_paid_out_interest[$count]['date_of_purchase'] = date('d-m-Y', strtotime($bond_inv->date_of_purchase));
                                $bond_paid_out_interest[$count]['date_of_issue'] = date('d-m-Y', strtotime($bond_inv->date_of_issue));
                                $bond_paid_out_interest[$count]['interest_frequency'] = $bond_inv->interest_frequency;
                                $bond_paid_out_interest[$count]['interest_rate'] = $bond_inv->interest_rate;
                                $bond_paid_out_interest[$count]['interest_accumalated'] = 0;
                                $bond_paid_out_interest[$count]['interest_paid'] = $interest->interest_paid;
                                $bond_paid_out_interest[$count]['bond_receipt'] = $bond_inv->bond_receipt;
                                $bond_paid_out_interest[$count]['interest_payout_date'] = date('d-m-Y', strtotime($interest->interest_payout_date));

                                $count++;
                            }


                        }



//                Bond calculation ends here


                        //pms starts here


                        $pms_investor = GroupMembers::where('id', $investor_id)->first();




                        $pms_overall_details = $this->calculatePmsOverallDetails($pms_investor);

                        $pms_investment = $pms_overall_details['investment'];


                        //pms ends here



                        $member_name = $pms_investor->member_name;
                        $member_pan =  $pms_investor->member_pan;
                        $member_email = $pms_investor->email;
                        $member_contact = $pms_investor->contact;
                        $member_add = $pms_investor->address;



                        $withdraw_details = WithdrawDetails::where('investor_id', $investor_id)->where('investor_type', 'subperson')->orderBy('withdraw_date','asc')->get();
                        $inv_name = GroupMembers::where('id', $investor_id)->get()->toArray();
                        $wd_export = [];
                        foreach ($withdraw_details as $withdraw) {
                            $wd_export[] = array(
                                'Investor Name' => $inv_name[0]['member_name'],
                                'Scheme Name' => $withdraw->scheme_name,
                                'Amount Invested' => $withdraw->invested_amount,
                                'Purchase Date' => $withdraw->purchase_date,
                                'Purchase NAV' => $withdraw->purchase_nav,
                                'Units' => $withdraw->units,
                                'Redemption NAV' => bcdiv(($withdraw->withdraw_amount - $withdraw->stt) / $withdraw->units, 1, 4),
                                'Withdraw Value' => $withdraw->withdraw_amount,
                                'Realised Profit/Loss' => (($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount),
                                'Redemption Date' => $withdraw->withdraw_date,
                                //                                       'Absolute Returns' => round((($withdraw->withdraw_amount - $withdraw->invested_amount)/$withdraw->invested_amount)*100, 2),
                                'Exit Load' => $withdraw->exit_load,
                                'STT' => $withdraw->stt,
                                'Folio Number' => $withdraw->folio_number,

                            );
                        }


                        $pdf = PDF::loadView('cispdf', [
                            'inv' => $inv,
                            'withdraw' => $wd_export,
                            'pms_inv'=>$pms_investment,
                            'dividends'=> $dividends,
                            'bond_inv' => $bond_investment,
                            'bond_interest' => $bond_paid_out_interest ,
                            'bond_redemptions' => $bondRedemptions,
                            'name' => $member_name,
                            'date' => $change_date,
                            'pan' => $member_pan,
                            'email' => $member_email,
                            'contact' => $member_contact,
                            'download_option' => $download_option,
                            'address' => $member_add])->setPaper('a4', 'landscape')->setWarnings(false)->save($member_name . '.pdf');

                        unset($inv['one_time']);
                        unset($inv['sip']);
                        unset($inv['liquid']);

                        $excel->sheet($member_name, function ($sheet) use ($inv, $wd_export) {
                            $sheet->fromArray($inv);
                            $sheet->setWidth('A', 20);
                            $sheet->setWidth('B', 50);
                            $sheet->setWidth('C', 20);
                            $sheet->setWidth('D', 15);
                            $sheet->setWidth('E', 15);
                            $sheet->setWidth('F', 15);
                            $sheet->setWidth('G', 15);
                            $sheet->setWidth('H', 25);
                            $sheet->setWidth('I', 25);
                            $sheet->setWidth('J', 18);
                            $sheet->setWidth('K', 25);
                            $sheet->setWidth('L', 20);


                            $sheet->fromArray($wd_export);
                            $sheet->setWidth('A', 20);
                            $sheet->setWidth('B', 50);
                            $sheet->setWidth('C', 20);
                            $sheet->setWidth('D', 15);
                            $sheet->setWidth('E', 15);
                            $sheet->setWidth('F', 15);
                            $sheet->setWidth('G', 15);
                            $sheet->setWidth('H', 25);
                            $sheet->setWidth('I', 25);
                            $sheet->setWidth('J', 18);
                            $sheet->setWidth('K', 25);
                            $sheet->setWidth('L', 20);

                        });


                    }
                })->store('xlsx', $_SERVER['DOCUMENT_ROOT']);




                $zip = new ZipArchive;
                if ($zip->open('cis.zip', ZipArchive::CREATE) === TRUE) {
                    foreach ($names as $value) {
                        $zip->addFile($value . '.pdf');
                    }
                    $zip->addFile('CIS Report.xlsx');
                    $zip->addFile('CIS Report.pdf');
                    $zip->close();
                    foreach ($names as $value) {
                        $path = $_SERVER['DOCUMENT_ROOT'] . '/' . $value . '.pdf';

                        if(file_exists($path)){
                            unlink($path);
                        }
                    }
                    $xls = $_SERVER['DOCUMENT_ROOT'] . '/CIS Report.xlsx';
                    // unlink($xls);


                    if(file_exists($xls)){
                        unlink($xls);
                    }

                    $pdf = $_SERVER['DOCUMENT_ROOT'] . '/CIS Report.pdf';
                    

                    if(file_exists($pdf)){
                        unlink($pdf);
                    }
                }

                $file = public_path() . "/cis.zip";

                $headers = array(
                    'Content-Type: application/zip',
                );

                return response()->download($file, 'cis.zip', $headers);

            } //cis ends
            else {
                $change_date = $request['ex_nav_date'];
                $investor_id = $request['investor_id'];
                $investor_type = $request['investor_type'];


                $change_date = Carbon::now()->subDay()->toDateString();
//                dd($change_date);
                $investment_details = InvestmentDetails::where('status', '!=', 'inactive')
                    ->where('investor_type', $investor_type)
                    ->where('investor_id', $investor_id)
                    ->where('purchase_date', '<=', $change_date)->orderBy('purchase_date', 'asc')->get();




                $investor_detail = [];
                $name = '';
                $email = '';
                $address = '';
                $contact = '';
                $pan = '';
                if ($investor_type == 'individual') {

                    $pms_details = PmsDetails::where('pmsinvestor_type','App\Person')->where('pmsinvestor_id', $investor_id)->get();
                    $bond_details = BondDetails::where('bondinvestor_type','App\Person')->where('bondinvestor_id', $investor_id)->where('status','=','1')->get();
                    $dividends = Dividend::where('investor_type', 'individual')->where('investor_id', $investor_id)->get();

                    $investor_detail = Person::where('id', $investor_id)->get();
                    $name = $investor_detail[0]['name'];
                    $email = $investor_detail[0]['email'];
                    $address = $investor_detail[0]['address'];
                    $contact = $investor_detail[0]['contact'];
                    $pan = $investor_detail[0]['person_pan'];
                } elseif ($investor_type == 'subperson') {

                    $pms_details = PmsDetails::where('pmsinvestor_type','App\GroupMembers')->where('pmsinvestor_id', $investor_id)->get();
                    $bond_details = BondDetails::where('bondinvestor_type','App\GroupMembers')->where('bondinvestor_id', $investor_id)->where('status','=','1')->get();
                    $dividends = Dividend::where('investor_type', 'subperson')->where('investor_id', $investor_id)->get();


                    $investor_detail = GroupMembers::where('id', $investor_id)->get();
                    $name = $investor_detail[0]['member_name'];
                    $email = $investor_detail[0]['email'];
                    $address = $investor_detail[0]['address'];
                    $contact = $investor_detail[0]['contact'];
                    $pan = $investor_detail[0]['member_pan'];
                }



//                bond calculation starts here


                $bond_overall_details = $this->calculateBondOverallDetails($investor_id, ($investor_type == "individual" ? 'App\Person' : 'App\GroupMembers'));


                if ($request['investor_type'] == "subperson") {
                    $bond_investor = GroupMembers::where('id', $investor_id)->first();
                } else {
                    $bond_investor = Person::where('id', $investor_id)->first();
                }

                $bond_investment = $bond_investor->bondInvestment;
                $bondRedemptions = [];

                // dd('sadf');
                foreach ($bond_investment as $inv){

                    if($inv->bondRedemptions != null){
                        $bondRedemptions[] = BondRedemption::with('bonds')->where('bond_id',$inv->id)->first(); 
                    }

                    unset($inv['interest_paid']);
                    $inv['interest_accumalated'] = $this->getInterestAccumalated($inv);
                    $inv['total_interest_paid'] = $inv->interestPaid()->sum('interest_paid');
                }


                $bond_paid_out_interest = [];
                $count = 0;

                foreach ($bond_investment as $inv){
//                dd($inv->interestPaid);

                    foreach ($inv->interestPaid as $interest){
                        $bond_paid_out_interest[$count]['id'] = $interest->id;
                        $bond_paid_out_interest[$count]['bond_name'] = $inv->name;
                        $bond_paid_out_interest[$count]['bond_id'] = $inv->id;
                        $bond_paid_out_interest[$count]['investment_amount'] = $inv->amount_invested;
                        $bond_paid_out_interest[$count]['date_of_purchase'] = date('d-m-Y', strtotime($inv->date_of_purchase));
                        $bond_paid_out_interest[$count]['date_of_issue'] = date('d-m-Y', strtotime($inv->date_of_issue));
                        $bond_paid_out_interest[$count]['interest_frequency'] = $inv->interest_frequency;
                        $bond_paid_out_interest[$count]['interest_rate'] = $inv->interest_rate;
                        $bond_paid_out_interest[$count]['interest_accumalated'] = 0;
                        $bond_paid_out_interest[$count]['interest_paid'] = $interest->interest_paid;
                        $bond_paid_out_interest[$count]['bond_receipt'] = $inv->bond_receipt;
                        $bond_paid_out_interest[$count]['interest_payout_date'] = date('d-m-Y', strtotime($interest->interest_payout_date));

                        $count++;
                    }


                }


//                Bond calculation ends here



                //pms starts here


                if ($request['investor_type'] == "subperson") {
                    $pms_investor = GroupMembers::where('id', $investor_id)->first();
                } else {
                    $pms_investor = Person::where('id', $investor_id)->first();
                }




                $pms_overall_details = $this->calculatePmsOverallDetails($pms_investor);

                $pms_investment = $pms_overall_details['investment'];


                //pms ends here




                $export = array();


                $export['one_time'] = InvestmentDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->where('investment_type', 1)->where('status', '!=', 'inactive')->count();
                $export['sip'] = InvestmentDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->where('investment_type', 2)->where('status', '!=', 'inactive')->count();
                $export['liquid'] = InvestmentDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->where('investment_type', 3)->where('status', '!=', 'inactive')->count();


                //$investment_details = $investment_details->toArray();

//                dd($investment_details);

                $yesterday_date = Carbon::now()->subDay(1)->toDateString();
                $yesterday_date = date('Y-m-d', strtotime($yesterday_date));
                foreach ($investment_details as $investment_detail) {

                    $current_nav = HistoricNavs::where('scheme_code', $investment_detail->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
//                        dd($current_nav, $change_date);

                    $current_market_value = ($investment_detail->units) * $current_nav['nav'];
                    $p_or_loss = (($investment_detail->units) * $current_nav['nav']) - (($investment_detail->units) * ($investment_detail->purchase_nav));

                    $today = Carbon::now()->subDay(0)->toDateString();
                    $today = date('Y-m-d', strtotime($today));
                    $purchase_date = $investment_detail->purchase_date;

                    $date_diff = date_diff(date_create($purchase_date), date_create($today));
                    $date_diff = $date_diff->format("%a");

                    $abs_returns = (($current_market_value - $investment_detail->amount_inv) / $investment_detail->amount_inv) * 100;
                    if ($date_diff == 0) {
                        $ann_returns = 0;
                    } else {
                        $ann_returns = (($abs_returns * 365) / $date_diff);
                    }

                    $date = date_create($investment_detail->purchase_date);
                    $purchase_date = date_format($date, "d-M-y");

                    $export[] = array(

                        'Scheme Name' => $investment_detail->scheme_name,
                        //'Scheme Type' => $investment_detail->scheme_type,
                        'Folio Number' => $investment_detail->folio_number,
                        'Purchase date' => $purchase_date,
                        'Amount Invested' => $investment_detail->amount_inv,
                        'Purchase NAV' => $investment_detail->purchase_nav,
                        'Units' => round($investment_detail->units, 4),
                        'Current NAV' => $current_nav['nav'],
                        'Current Market Value' => round($current_market_value, 2),
                        'Unrealised Profit/Loss' => round($p_or_loss, 2),
                        'Absolute Returns' => round($abs_returns, 2),
                        'Annualised Returns' => round($ann_returns, 2),
                        'invest_type' => $investment_detail->investment_type
                    );
                }

                $withdraw_details = WithdrawDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->orderBy('withdraw_date','asc')->get();
                $wd_export = [];
                foreach ($withdraw_details as $withdraw) {

                    if($withdraw->units == 0){
                        dd($withdraw);
                    }
                    $wd_export[] = array(

                        'Scheme Name' => $withdraw->scheme_name,
                        'Folio Number' => $withdraw->folio_number,
                        'Purchase Date' => $withdraw->purchase_date,
                        'Amount Invested' => $withdraw->invested_amount,
                        'Purchase NAV' => $withdraw->purchase_nav,
                        'Units' => $withdraw->units,
                        'Redemption NAV' => bcdiv(($withdraw->withdraw_amount - $withdraw->stt) / $withdraw->units, 1, 4),
                        'Redemption Date' => $withdraw->withdraw_date,
                        'Withdraw Value' => $withdraw->withdraw_amount,
                        'Realised Profit/Loss' => (($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount),
//                                       'Absolute Returns' => round((($withdraw->withdraw_amount - $withdraw->invested_amount)/$withdraw->invested_amount)*100, 2),
                        'Exit Load' => $withdraw->exit_load,
                        'STT' => $withdraw->stt,


                    );
                }

//                    dd($export);


               return $pdf = PDF::loadView('cispdf', ['inv' => $export,
                    'dividends'=>$dividends,
                    'pms_inv'=>$pms_investment,
                    'bond_inv'=>$bond_investment,
                    'bond_interest' => $bond_paid_out_interest,
                    'bond_redemptions' => $bondRedemptions,
                    'withdraw' => $wd_export,
                    'date' => $change_date,
                    'name' => $name,
                    'address' => $address,
                    'email' => $email,
                    'contact' => $contact,
                    'download_option' => $download_option,
                    'pan' => $pan])->setPaper('a4', 'landscape')->setWarnings(false)->stream('Investment Summary.pdf');
                //print_r($export);
                unset($export['one_time']);
                unset($export['sip']);
                unset($export['liquid']);
                $file_content = \Excel::create('Investment Summary', function ($excel) use ($export, $wd_export) {


                    $excel->sheet('Investment Summary', function ($sheet) use ($export, $wd_export) {
                        $sheet->fromArray($export);
                        $sheet->setWidth('A', 20);
                        $sheet->setWidth('B', 50);
                        $sheet->setWidth('C', 20);
                        $sheet->setWidth('D', 15);
                        $sheet->setWidth('E', 15);
                        $sheet->setWidth('F', 15);
                        $sheet->setWidth('G', 15);
                        $sheet->setWidth('H', 25);
                        $sheet->setWidth('I', 25);
                        $sheet->setWidth('J', 18);
                        $sheet->setWidth('K', 25);
                        $sheet->setWidth('L', 20);


                        $sheet->fromArray($wd_export);
                        $sheet->setWidth('A', 20);
                        $sheet->setWidth('B', 50);
                        $sheet->setWidth('C', 20);
                        $sheet->setWidth('D', 15);
                        $sheet->setWidth('E', 15);
                        $sheet->setWidth('F', 15);
                        $sheet->setWidth('G', 15);
                        $sheet->setWidth('H', 25);
                        $sheet->setWidth('I', 25);
                        $sheet->setWidth('J', 18);
                        $sheet->setWidth('K', 25);
                        $sheet->setWidth('L', 20);

                    });

//                    $excel->sheet('Investment Summary', function ($sheet) use ($export) {
//                        $sheet->fromArray($export);
//                    });
//
//                    $excel->sheet('Withdraw Summary', function ($sheet) use ($wd_export) {
//                        $sheet->fromArray($wd_export);
//                    });

                })->store('xlsx', $_SERVER['DOCUMENT_ROOT']);

                $zip = new ZipArchive;
                if ($zip->open('Investment Summary.zip', ZipArchive::CREATE) === TRUE) {
                    // ADDING FILES TO ZIP
                    $zip->addFile('Investment Summary.pdf');
                    $zip->addFile('Investment Summary.xlsx');
                    $zip->close();
                    // REMOVING FILES FROM SERVER
                    $pdf = $_SERVER['DOCUMENT_ROOT'] . '/Investment Summary.pdf';
                    $xls = $_SERVER['DOCUMENT_ROOT'] . '/Investment Summary.xlsx';

                    if(file_exists($pdf)){
                        unlink($pdf);
                    }

                    if(file_exists($xls)){
                        unlink($xls);
                    }
                    
                }

                $file = public_path() . "/Investment Summary.zip";

                $headers = array(
                    'Content-Type: application/zip',
                );

                return response()->download($file, 'Investment Summary.zip', $headers);
            }
            //individual report ends

        }
    }


    public function addSubPerson(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'sub_person_name' => 'required',
            'group_id' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $sub_person_name = $request['sub_person_name'];
            $group_id = $request['group_id'];
            //return $group_id;
            $sub_person = new GroupMembers();
            $sub_person->user_id = \Auth::user()->id;
            $sub_person->group_id = $group_id;
            $sub_person->member_name = $sub_person_name;
            if ($sub_person->save()) {
                return $sub_person;
            } else {
                return response()->json('save failed');
            }

        }
    }

    public function removeSubPerson(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'sub_person_id' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $remove_sub_person = GroupMembers::where('user_id', \Auth::user()->id)->where('id', $request['sub_person_id'])->delete();
            $investments = InvestmentDetails::where('user_id', \Auth::user()->id)->where('investor_id', $request['sub_person_id'])->where('investor_type', 'subperson')->get();

            $remove_investments = InvestmentDetails::where('user_id', \Auth::user()->id)->where('investor_id', $request['sub_person_id'])->where('investor_type', 'subperson')->delete();

            if (count($investments) == 0) {

                //echo "zero inv";
                if ($remove_sub_person) {
                    return response()->json(['msg' => '1']);
                } else {
                    return response()->json(['msg' => '0']);
                }
            }

            if (count($investments) != 0) {
                //echo "investment is there";
                if ($remove_sub_person && $remove_investments) {
                    return response()->json(['msg' => '1']);
                } else {
                    return response()->json(['msg' => '0']);
                }
            }
        }
    }


    public function cis(Request $request)
    {
        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

        $validator = \Validator::make($request->all(), [

            'group_id' => 'required',
            'date' => 'required',

        ]);


        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            set_time_limit(600);


//            dd($request['date']);
            $group_id = $request['group_id'];
            $group_mem_id = array();
            $group_mem_id = GroupMembers::where('group_id', $group_id)->pluck('id')->toArray();


            $withdraw_details = WithdrawDetails::where('investor_type', 'subperson')->whereIn('investor_id', $group_mem_id)->get();
            $realised_p_or_l = 0;

            foreach ($withdraw_details as $withdraw) {

                // Subtracting two times of STT from the withdraw amout because the withdraw_amount is entered with the formula
                // Withdraw_amount = withdraw_amount - exit_load + stt
                $realised_p_or_l += (($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount);
            }


            $count = 0;
            $cis_details = array();
            $yesterday_date = Carbon::now()->subDay(1)->toDateString();
            $yesterday_date = date('Y-m-d', strtotime($yesterday_date));
            $investment_detail = array();
            $total_m_value = 0;
            $profit_or_loss = 0;
            $absolute_returns_avg = 0;
            $total_amount_inv = 0;
            $scheme_inv_total = 0;
            $current_market_value = 0;
            $xirr_market_val = 0;

            $grand_total = 0;
            $grand_market_value = 0;
            $grand_profit_or_loss = 0;
            $grand_absolute_returns = 0;
            $units = 0;
            $change_date = '';

            $current_nav = '';

            $unique_scheme = array();

            $check_them = array();
            $investor_ids = array();

            $inv_pass = array();
            $cis_date = '';

            foreach ($group_mem_id as $member_id) {
                $investor_id[] = $member_id;
            }


            if ($request['date'] == 'empty') {

//						$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$investor_id)
//						->where('investor_type','subperson')
//						->where('purchase_date','<=',$yesterday_date)
//						->get();

                $investment_detail = InvestmentDetails::where('investor_id', $investor_id)
                    ->where('investor_type', 'subperson')
                    ->where('purchase_date', '<=', $change_date)
                    ->get();

                $investment_detail = $investment_detail->filter(function ($investment) {
                    return $investment->status == 'active' || $investment->status == 'paractive';
                });
            }
            if ($request['date'] != 'empty') {

//                $change_date = $request['date'];
                $change_date = date('Y-m-d', strtotime($request['date']));
//						$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$investor_id)
//						->where('investor_type','subperson')
//						->where('purchase_date','<=',$change_date)
//						->get();

                $investment_detail = InvestmentDetails::whereIn('investor_id', $investor_id)
                    ->where('investor_type', 'subperson')
                    ->where('purchase_date', '<=', $change_date)
                    ->where('status','!=','inactive')
                    ->get();

//                $investment_detail = $investment_detail->filter(function ($investment) {
//                    return $investment->status == 'active' || $investment->status == 'paractive';
//                });
            }

//					dd(count($investment_detail));



            foreach ($investment_detail as $investments) {

//                dd($investments);
                $Scheme_sub = SchemeDetails::where('scheme_code', $investments->scheme_code)->value('scheme_sub');
//                dd($Scheme_sub);



                // 						/*sensex & nifty cal*/
                // $inv_date = $investments->purchase_date;
                // $inv_amt = $investments->amount_inv;
                // $dateOrder = date('Y-m-d',strtotime ($request['date']));

                // $get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
                // $buy_sensex = $inv_amt/$get_sensex['closing_index'];
                // $current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
                // $current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
                // $current_sensex_val = round($current_sensex_val, 2);

                // $get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
                // $buy_nifty = $inv_amt/$get_nifty['closing_index'];
                // $current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
                // $current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
                // $current_nifty_val = round($current_nifty_val, 2);

//                if ($request['date'] == 'empty') {
//                    $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->orderBy('date', 'aesc')->first();
//                    //return $current_nav;
//                    //echo $current_nav['nav'];
//                    $nav_date = $current_nav['date'];
//                    $change_date = date('d-m-Y', strtotime($nav_date));
//
//                    $current_nav = $current_nav['nav'];
//
//                    //echo $current_nav['nav'];
//                    //echo $nav_date;
//                    //echo $current_nav;
//                    //echo "empty date";
//
//                }

//                if ($request['date'] != 'empty') {

//                    $change_date = $request['date'];
                    //return $change_date;
                    $change_date = date('Y-m-d', strtotime($request['date']));
                    //return $change_date;
                    $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
//                    $nav_date_one = HistoricNavs::where('scheme_code', $investments->scheme_code)->orderBy('date', 'desc')->first();
//                    $nav_date_two = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<', $nav_date_one['date'])->orderBy('date', 'desc')->first();
//                    $navState = (($nav_date_one['nav'] - $nav_date_two['nav']) / $nav_date_two['nav']) * 100;
//                    $navState = round($navState, 2);
//                     dd($current_nav['date']);
                    //return $current_nav;
                    $nav_date = $current_nav['date'];
                    $change_date = $nav_date;
                    //return $nav_date;
                    $current_nav = $current_nav['nav'];

                    //echo "non empty";
                    //return $nav_date;

                    /*foreach ($current_nav as $nav) {
									$pass_nav = $nav->nav;
								}*/
//                }

                if (array_key_exists($investments->scheme_code, $inv_pass)) {
                    // $inv_pass[$investments->scheme_code]['sensex'] = $current_sensex_val;
                    // $inv_pass[$investments->scheme_code]['nifty'] = $current_nifty_val;
                    // $inv_pass[$investments->scheme_code]['delta'] = $navState;

                    $inv_pass[$investments->scheme_code]['total_amount_inv'] += $investments->amount_inv;
                    $inv_pass[$investments->scheme_code]['scheme_name'] = $investments->scheme_name;


                    $inv_pass[$investments->scheme_code]['units'] += ($investments->units);
                    //$inv_pass[$investments->scheme_code]['units'] = $units;
                    $inv_pass[$investments->scheme_code]['current_nav'] = $current_nav;


                    $current_market_value = (($inv_pass[$investments->scheme_code]['units']) * $current_nav);

                    $current_market_value = round($current_market_value, 2);
                    $inv_pass[$investments->scheme_code]['current_market_value'] = $current_market_value;

                    $inv_pass[$investments->scheme_code]['profit_or_loss'] = (($inv_pass[$investments->scheme_code]['current_market_value']) - ($inv_pass[$investments->scheme_code]['total_amount_inv']));

                    $inv_pass[$investments->scheme_code]['absolute_returns'] = round(((($inv_pass[$investments->scheme_code]['current_market_value']) - $inv_pass[$investments->scheme_code]['total_amount_inv']) / ($inv_pass[$investments->scheme_code]['total_amount_inv'])) * 100, 2);
                } else {
                    // $inv_pass[$investments->scheme_code]['sensex'] = $current_sensex_val;
                    // $inv_pass[$investments->scheme_code]['nifty'] = $current_nifty_val;
                    // $inv_pass[$investments->scheme_code]['delta'] = $navState;

                    $inv_pass[$investments->scheme_code]['total_amount_inv'] = $investments->amount_inv;
                    $inv_pass[$investments->scheme_code]['scheme_name'] = $investments->scheme_name;


                    $inv_pass[$investments->scheme_code]['current_nav'] = $current_nav;

                    $inv_pass[$investments->scheme_code]['units'] = ($investments->units);

                    $current_market_value = (($investments->units) * $current_nav);

                    $current_market_value = round($current_market_value, 2);

                    $inv_pass[$investments->scheme_code]['current_market_value'] = $current_market_value;

                    $inv_pass[$investments->scheme_code]['profit_or_loss'] = (($inv_pass[$investments->scheme_code]['current_market_value']) - ($inv_pass[$investments->scheme_code]['total_amount_inv']));



                    $inv_pass[$investments->scheme_code]['absolute_returns'] = round(((($inv_pass[$investments->scheme_code]['current_market_value']) - $inv_pass[$investments->scheme_code]['total_amount_inv']) / ($inv_pass[$investments->scheme_code]['total_amount_inv'])) * 100, 2);

                }

                $grand_total += $investments->amount_inv;

                //$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
                //$current_nav = $current_nav['nav'];
                $units_held = $investments->units;

                $current_market_value = $units_held * $current_nav;

                $grand_market_value += $current_market_value;



                $count++;
            }


            $profit_or_loss = $grand_market_value - $grand_total;
            $grand_profit_or_loss += $profit_or_loss;
            $grand_absolute_returns = ((($grand_market_value - $grand_total) / $grand_total) * 100);

            $grand_total = round($grand_total, 2);
            $grand_market_value = round($grand_market_value, 2);
            $grand_profit_or_loss = round($grand_profit_or_loss, 2);
            $grand_absolute_returns = round($grand_absolute_returns, 2);


            if ($count == 0) {
                return response()->json(['msg' => '0']);
            } else {

                $export_date = date('Y-m-d', strtotime($change_date));
                $change_date = date('d-m-Y', strtotime($change_date));

                //return response()->json(['msg'=> '1','response'=>$cis_details]);
//                $absolute_returns_avg = round(($absolute_returns_avg / $count), 2);
//                $current_market_value = round($current_market_value, 2);
//                $profit_or_loss = round($profit_or_loss, 2);
//                $total_m_value = round($total_m_value, 2);



                //$xirrVal = round((PHPExcel_Calculation_Financial::XIRR($amountArray, $dateArray)) * 100, 2);
//                $xirrVal = $xirrVal * 100;

                $xirrVal = 0;
                $grand_total = $fmt->format($grand_total);

//						if($realised_p_or_l == 0){
//                            $net_amount_invested = $grand_total;
//
//                        }else{
//                            $net_amount_invested = $grand_market_value - (abs($realised_p_or_l) + abs($grand_profit_or_loss));
//                        }

                $net_amount_invested = $fmt->format($this->getNetAmountInvested($grand_market_value, $realised_p_or_l, $grand_profit_or_loss));

                $grand_profit_or_loss = $fmt->format($grand_profit_or_loss);
                $grand_market_value = $fmt->format($grand_market_value);
                $realised_p_or_l = $fmt->format(round($realised_p_or_l, 0));
                // dd($inv_pass);

                return response()->json(['msg' => '1',
                    'realised_p_or_loss' => $realised_p_or_l,
                    'absolute_returns_avg' => $grand_absolute_returns,
                    'total_amount_inv' => $grand_total,
                    'profit_or_loss' => $grand_profit_or_loss,
                    'total_m_value' => $grand_market_value,
                    'cis_details' => $inv_pass,
                    'date' => $change_date, 'export_date' => $export_date,
                    'xirr_value' => 0,
                    'net_amount_invested' => $net_amount_invested,
                ]);

                //print_r($inv_pass);
            }


        }
    }


    public function rename(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'id' => 'required',
            'rename_type' => 'required',
            'new_name' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            if ($request['rename_type'] == 'group') {
                $rename = Group::where('user_id', \Auth::user()->id)->where('id', $request['id'])->update(['name' => $request['new_name']]);
                if ($rename) {
                    return response()->json(['msg' => '1']);
                }
            } else if ($request['rename_type'] == 'individual') {

                $rename = Person::where('user_id', \Auth::user()->id)->where('id', $request['id'])->update(['name' => $request['new_name']]);
                if ($rename) {
                    return response()->json(['msg' => '1']);
                }
            } else if ($request['rename_type'] == 'subperson') {

                $rename = GroupMembers::where('user_id', \Auth::user()->id)->where('id', $request['id'])->update(['member_name' => $request['new_name']]);
                if ($rename) {
                    return response()->json(['msg' => '1']);
                }
            }
        }
    }

    public function deleteInvestment(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'inv_id' => 'required',
            'investor_id' => 'required',
            'investor_type' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $delete_inv = InvestmentDetails::where('user_id', \Auth::user()->id)->where('id', $request['inv_id'])->where('investor_id', $request['investor_id'])->where('investor_type', $request['investor_type'])->delete();

            $delete_withdraw = WithdrawDetails::where('user_id', \Auth::user()->id)->where('investment_id', $request['inv_id'])->delete();

            if ($delete_inv) {
                $investments = InvestmentDetails::where('user_id', \Auth::user()->id)->where('investor_id', $request['investor_id'])
                    ->where('investor_type', $request['investor_type'])
                    ->get();

                $count = 0;
                $investment_details = array();
                $current_m_value = 0;
                $profit_or_loss = 0;
                $total_amount = 0;
                $total_m_value = 0;
                $absolute_returns_avg = 0;

                $yesterday_date = Carbon::now()->subDay(1)->toDateString();
                $yesterday_date = date('Y-m-d', strtotime($yesterday_date));

                $total_amount = InvestmentDetails::where('user_id', \Auth::user()->id)->where('investor_id', $request['investor_id'])
                    ->where('investor_type', $request['investor_type'])
                    ->sum('amount_inv');

                if (empty($total_amount)) {
                    $total_amount = 0;
                }

                if (count($investments) == 0) {
                    return response()->json(['msg' => '1', 'investment_details' => $investment_details, 'total_amount' => $total_amount, 'total_m_value' => $total_m_value, 'profit_or_loss' => $profit_or_loss, 'absolute_return_avg' => $absolute_returns_avg]);
                } else {

                    foreach ($investments as $investment) {

                        $investment_details[$count]['investor_id'] = $request['investor_id'];
                        $investment_details[$count]['investor_type'] = $request['investor_type'];
                        $investment_details[$count]['investment_id'] = $investment->id;
                        $investment_details[$count]['scheme_name'] = $investment->scheme_name;
                        $investment_details[$count]['scheme_type'] = $investment->scheme_type;
                        $investment_details[$count]['dop'] = $investment->purchase_date;
                        $investment_details[$count]['amount_inv'] = $investment->amount_inv;
                        $investment_details[$count]['purchase_nav'] = $investment->purchase_nav;
                        $investment_details[$count]['inv_type'] = $investment->investment_type;
                        $investment_details[$count]['inv_type'] = $investment->investment_type;
                        $investment_details[$count]['units'] = round($investment->units, 4);


                        //$current_nav = HistoricNavs::where('scheme_code','106212')->first()->pluck('nav');
                        $current_nav = HistoricNavs::where('scheme_code', $investment->scheme_code)->orderBy('date', 'aesc')->first();
                        //echo $current_nav['nav'];
                        //echo $investment->dop;
                        $investment_details[$count]['current_nav'] = $current_nav['nav'];
                        $investment_details[$count]['current_market_value'] = round(($investment->units) * $current_nav['nav'], 2);
                        $investment_details[$count]['folio_number'] = $investment->folio_number;
                        $investment_details[$count]['p_or_loss'] = round((($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav)), 2);


                        $current_m_value = ($investment->units) * $current_nav['nav'];
                        $total_m_value += $current_m_value;
                        $profit_or_loss += (($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav));

                        $initial_value = ($investment->purchase_nav) * ($investment->units);
                        $absolute_returns = (($current_m_value - $initial_value) / $initial_value) * 100;
                        //echo $absolute_returns;
                        $investment_details[$count]['abs_returns'] = round($absolute_returns, 2);

                        $absolute_returns_avg += $absolute_returns;
                        //echo $absolute_returns;
                        //echo $absolute_returns_avg;

                        $today = Carbon::now()->subDay(0)->toDateString();
                        $today = date('Y-m-d', strtotime($today));
                        $purchase_date = $investment->purchase_date;

                        $date_diff = date_diff(date_create($purchase_date), date_create($today));
                        $date_diff = $date_diff->format("%a");

                        if ($date_diff == 0) {
                            $annualised_return = 0;
                        } else {
                            $annualised_return = (($absolute_returns * 365) / $date_diff);
                        }
                        $investment_details[$count]['ann_returns'] = round($annualised_return, 2);


                        $count++;
                    }

                    //echo ($absolute_returns_avg/$count);

                    $absolute_returns_avg = round(($absolute_returns_avg / $count), 2);
                    $current_m_value = round($current_m_value, 2);
                    $profit_or_loss = round($profit_or_loss, 2);
                    $total_m_value = round($total_m_value, 2);

                    //print_r($total_amount);
                    //print_r($current_m_value);
                    //print_r($profit_or_loss);

                    //return $investment_details;
                    //print_r($current_nav);*/
                    return response()->json(['msg' => '1', 'investment_details' => $investment_details, 'total_amount' => $total_amount, 'total_m_value' => $total_m_value, 'profit_or_loss' => $profit_or_loss, 'absolute_return_avg' => $absolute_returns_avg]);
                }
            } else {
                return response()->json(['msg' => '0']);
            }

        }
    }


    public function addPan(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'investor_id' => 'required',
            'investor_type' => 'required',
            'pan' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $investor_id = $request['investor_id'];
            $investor_type = $request['investor_type'];
            $pan = $request['pan'];

            if ($investor_type == "individual") {

                $add_pan = Person::where('id', $investor_id)->update(['person_pan' => $pan]);

                if ($add_pan) {
                    return response()->json(['msg' => "1", 'pan' => $pan]);
                } else {
                    return response()->json(['msg' => "0"]);
                }

            }
            if ($investor_type == "subperson") {
                $add_pan = GroupMembers::where('id', $investor_id)->update(['member_pan' => $pan]);

                if ($add_pan) {
                    return response()->json(['msg' => "1", 'pan' => $pan]);
                } else {
                    return response()->json(['msg' => "0"]);
                }

            }
        }
    }


    public function addQuery(Request $request)
    {

        $validator = \Validator::make($request->all(), [

            'investor_id' => 'required',
            'query_text' => 'required',
            'query_date' => 'required',
            'investor_type' => 'required',


        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $new_query = new Queries();
            $new_query->investor_id = $request['investor_id'];
            $new_query->investor_type = $request['investor_type'];
            $new_query->query = $request['query_text'];

            $date = date('Y-m-d', strtotime($request['query_date']));
            //return $date;
            $new_query->date = $date;
            $new_query->status = 0;

            $save_query = $new_query->save();

            if ($save_query) {
                $investor_id = $request['investor_id'];
                $queries = Queries::where('investor_id', $investor_id)->where('investor_type', $request['investor_type'])->get();
                return response()->json(['msg' => '1', 'query' => $queries]);
            }


        }

    }

    public function getQuery(Request $request)
    {

        $validator = \Validator::make($request->all(), [

            'investor_id' => 'required',
            'investor_type' => 'required'

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $investor_id = $request['investor_id'];
            $queries = Queries::where('investor_id', $investor_id)->where('investor_type', $request['investor_type'])->get();
            return response()->json(['msg' => '1', 'query' => $queries]);

        }

    }

    public function updateQuery(Request $request)
    {

        $validator = \Validator::make($request->all(), [

            'query_id' => 'required',
            'date' => 'required',
            'status' => 'required',
            'query' => 'required',
            'investor_id' => 'required',

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $date = date('Y-m-d', strtotime($request['date']));

            if ($request['status'] == "InProcess") {
                $status = 0;
            }

            if ($request['status'] == "Complete") {
                $status = 1;
            }
            if ($request['status'] == "CompleteComplete") {
                $status = 1;
                //return $request;
            }


            $query_id = $request['query_id'];
            $update_query = Queries::where('id', $query_id)->update(['date' => $date, 'status' => $status, 'query' => $request['query']]);

            $queries = Queries::where('investor_id', $request['investor_id'])->get();

            if ($update_query) {
                return response()->json(['msg' => '1', 'query' => $queries]);
            } else {
                return response()->json(['msg' => '0']);
            }


        }

    }

    public function deleteQuery(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'investor_id' => 'required',
            'query_id' => 'required'

        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {

            $delete_query = Queries::where('id', $request['query_id'])->delete();

            if ($delete_query) {
                $queries = Queries::where('investor_id', $request['investor_id'])->get();
                return response()->json(['msg' => '1', 'query' => $queries]);
            }
        }
    }

    public function checkPass(Request $request)
    {

        $validator = \Validator::make($request->all(), [

            'pass' => 'required',
            'type' => 'required',


        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        } else {


            $password = $request['pass'];

            $hashed = \Hash::make($password);

            $admin_pass = PasswordManagement::where('type',$request['type'])->value('password');

//            dd($request->all(), $admin_pass);

            if (\Hash::check($password, $admin_pass)) {
                return response()->json(['msg' => '1']);
            } else {
                return response()->json(['msg' => '0']);
            }
        }

    }


    // public function portfolio()
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
    // 	$SchemeDetail = SchemeDetails::get()
    //                     ->groupBy('scheme_sub');
    //        $scheme_type = scheme_type::pluck('scheme_sub');
    //        $total_amount_invested = 0;

    //        $type_count = count($scheme_type) - 1;
    //        $scheme_array = [];
    //        for ($i=0; $i <= $type_count ; $i++) {
    //        	$data_set = [];
    //        	if (isset($SchemeDetail[$scheme_type[$i]])) {
    //        		$total_amount = 0;
    //        		array_push($data_set, $scheme_type[$i]);
    //        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
    //        			$invest_amount = InvestmentDetails::where('scheme_code',$value['scheme_code'])
    //        							->sum('units');
    //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
    //        							->orderBy('date', 'desc')
    //        							->first();
    //        			$sum_amount = $invest_amount * $historic_navs['nav'];
    //        			$total_amount += $sum_amount;
    //           	}
    //           	$total_amount = round($total_amount, 2);
    //           	$total_amount_invested += $total_amount;
    //           	array_push($data_set, $total_amount);
    //           	array_push($scheme_array, $data_set);
    //        	}
    //        }

    //        $final_array = [];
    //        $equity = 0;
    //        $debt = 0;
    //        $balanced = 0;
    //        foreach ($scheme_array as $value) {
    //        	$data_set_array = [];
    //        	$avg = ($value[1]/$total_amount_invested) * 100;
    //        	$avg = round($avg, 2);

    //        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
    //        	if ($type == 'equity') {
    //        		$equity += $value[1];
    //        	}else if ($type == 'balanced') {
    //        		$balanced += $value[1];
    //        	}else{
    //        		$debt += $value[1];
    //        	}
    //        	$amount = $fmt->format($value[1]);
    //        	array_push($data_set_array, $value[0], $amount, $avg);
    //        	array_push($final_array, $data_set_array);
    //        }

    //        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
    //        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
    //        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

    //        $equity = $fmt->format($equity);
    //        $debt = $fmt->format($debt);
    //        $balanced = $fmt->format($balanced);
    //        $total_amount_invested = $fmt->format($total_amount_invested);

    // 	return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
    // }

    // public function getAmc()
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
    // 	$grand_total_inv = 0;
    // 	$grand_total_crt = 0;
    // 	$amcNames = amcnames::pluck('name');
    // 	$schemebyamc = SchemeDetails::get()
    // 					->groupBy('amc_name');
    // 	$nameCount = count($amcNames);
    // 	$final_data_set = [];
    // 	for ($i=0; $i < $nameCount  ; $i++) {
    // 		$data_set = [];
    // 		if (isset($schemebyamc[$amcNames[$i]])) {
    // 			array_push($data_set, $amcNames[$i]);
    // 			$amount_invs = 0;
    // 			$current_val = 0;
    // 			foreach ($schemebyamc[$amcNames[$i]] as $value) {
    // 				$investments = InvestmentDetails::where('scheme_code',$value['scheme_code'])->get();
    //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
    //        							->orderBy('date', 'desc')
    //        							->first();

    //        			foreach ($investments as $investment) {

    // 	        			$amount_invs += $investment['amount_inv'];
    // 	        			$current_val += $investment['units'] * $historic_navs['nav'];
    //    				}
    // 			}

    // 			$current_val = round($current_val, 2);
    // 			$grand_total_inv += $amount_invs;
    // 			$grand_total_crt += $current_val;
    // 			array_push($data_set, $amount_invs);
    // 			array_push($data_set, $current_val);
    // 			array_push($final_data_set, $data_set);
    // 		}
    // 	}


    // 	$final_array = [];
    // 	foreach ($final_data_set as $value) {
    //        	$data_set_array = [];
    //        	$avg = 0;
    //        		$avg = ($value[1]/$grand_total_inv) * 100;
    //         	$avg = round($avg, 2);

    //        	// array_push($data_set_array, $value[0], $value[1], $value[2], $avg);
    // 		$amount = $fmt->format($value[1]);
    // 		$amount2 = $fmt->format($value[2]);
    //         $data_set_array['name'] = $value[0];
    //         $data_set_array['AmountInvet'] = $amount;
    //         $data_set_array['CurrentVal'] = $amount2;
    //         $data_set_array['Avg'] = $avg;
    //        	array_push($final_array, $data_set_array);
    //        }

    //        $total['AmountInvet'] = $fmt->format($grand_total_inv);
    //        $total['CurrentVal'] = $fmt->format($grand_total_crt);
    //        $total['Avg'] = 100;
    //        // dd($final_array);
    //        return view('amc',['amc_arrya' => $final_array, 'grand_total' => $total]);


    // }

    // public function amcDetails(Request $request)
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

    // 	$name = $request['name'];
    // 	$grand_total_inv = 0;
    // 	$grand_total_crt = 0;
    // 	if ($name == 'L'){
    // 		$name = "L&T";
    // 	}
    // 	$schemes = SchemeDetails::where('amc_name',$name)->pluck('scheme_code');
    // 	$count = count($schemes);
    // 	$investment_details = [];
    // 	for ($i=0; $i < $count ; $i++) {
    // 		$HistoricNav = HistoricNavs::where('scheme_code',$schemes[$i])
    //        							->orderBy('date', 'desc')
    //        							->first();
    //        	$investments = InvestmentDetails::where('scheme_code',$schemes[$i])
    //        							->get();
    //        	foreach ($investments as $investment) {
    //        		$investment_detail = [];
    //        		$pan = "";
    //        		$investor_type = $investment['investor_type'];
    // 			$current_val = $investment['units'] * $HistoricNav['nav'];
    // 			$current_val = round($current_val, 2);
    //        		if ($investor_type == "subperson") {
    // 				$pan = GroupMembers::where('id',$investment['investor_id'])->value('member_pan');
    // 			}else if ($investor_type == "individual") {
    // 				$pan = Person::where('id',$investment['investor_id'])->value('person_pan');
    // 			}

    // 			$grand_total_inv += $investment['amount_inv'];
    // 			$grand_total_crt += $current_val;

    // 			$investment_detail['name'] = $investment['scheme_name'];
    // 			$investment_detail['pan'] = $pan;
    // 			$investment_detail['invest_amount'] = $fmt->format($investment['amount_inv']);
    // 			$investment_detail['current_val'] = $fmt->format($current_val);
    // 			$investment_detail['date'] = $investment['purchase_date'];
    // 			array_push($investment_details, $investment_detail);
    //        	}

    // 	}
    // 	$grand_total_inv = $fmt->format($grand_total_inv);
    // 	$grand_total_crt = $fmt->format($grand_total_crt);
    // 	return response()->json(['msg'=>'1', 'investments'=>$investment_details, 'investamt'=>$grand_total_inv, 'CurrentVal'=>$grand_total_crt]);
    // }

    // public function portfolioBygroup($id)
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
    // 	$total_amount_invested = 0;

    // 	$SchemeDetail = SchemeDetails::get()
    //                     ->groupBy('scheme_sub');
    //        $scheme_type = scheme_type::pluck('scheme_sub');
    // 	$group_id = $id;
    // 	$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$group_id)->pluck('id')->toArray();
    // 	$change_date = Carbon::now();
    // 	$change_date = date('Y-m-d',strtotime($change_date));
    // 	$type_count = count($scheme_type) - 1;
    //        $scheme_array = [];
    //        for ($i=0; $i <= $type_count ; $i++) {
    //        	$data_set = [];
    //        	if (isset($SchemeDetail[$scheme_type[$i]])) {
    //        		$total_amount = 0;
    //        		array_push($data_set, $scheme_type[$i]);
    //        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
    //        			$invest_amount = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$group_mem_id)
    // 									->where('investor_type','subperson')
    // 									->where('purchase_date','<=',$change_date)
    // 									->where('scheme_code',$value['scheme_code'])
    // 									->sum('units');
    //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
    //        							->orderBy('date', 'desc')
    //        							->first();
    //        			$sum_amount = $invest_amount * $historic_navs['nav'];
    //        			$total_amount += $sum_amount;
    //           	}
    //           	$total_amount = round($total_amount, 2);
    //           	$total_amount_invested += $total_amount;
    //           	array_push($data_set, $total_amount);
    //           	array_push($scheme_array, $data_set);
    //        	}
    //        }

    // 	$final_array = [];
    //        $equity = 0;
    //        $debt = 0;
    //        $balanced = 0;
    //        foreach ($scheme_array as $value) {
    //        	$data_set_array = [];
    //        	$avg = 0;
    //    		$avg = ($value[1]/$total_amount_invested) * 100;
    //        	$avg = round($avg, 2);

    //        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
    //        	if ($type == 'equity') {
    //        		$equity += $value[1];
    //        	}else if ($type == 'balanced') {
    //        		$balanced += $value[1];
    //        	}else{
    //        		$debt += $value[1];
    //        	}
    // 		$amount = $fmt->format($value[1]);
    //        	array_push($data_set_array, $value[0], $amount, $avg);
    //        	array_push($final_array, $data_set_array);
    //        }

    //        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
    //        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
    //        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

    //        $equity = $fmt->format($equity);
    //        $debt = $fmt->format($debt);
    //        $balanced = $fmt->format($balanced);
    //        $total_amount_invested = $fmt->format($total_amount_invested);

    //        return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
    // }

    // public function portfolioByperson($id)
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
    // 	$total_amount_invested = 0;

    // 	$SchemeDetail = SchemeDetails::get()
    //                     ->groupBy('scheme_sub');
    //        $scheme_type = scheme_type::pluck('scheme_sub');
    //        $change_date = Carbon::now();
    // 	$change_date = date('Y-m-d',strtotime($change_date));
    // 	$type_count = count($scheme_type) - 1;
    //        $scheme_array = [];
    //        for ($i=0; $i <= $type_count ; $i++) {
    //        	$data_set = [];
    //        	if (isset($SchemeDetail[$scheme_type[$i]])) {
    //        		$total_amount = 0;
    //        		array_push($data_set, $scheme_type[$i]);
    //        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
    //        			$invest_amount = InvestmentDetails::where('investor_id',$id)
    // 									->where('purchase_date','<=',$change_date)
    // 									->where('scheme_code',$value['scheme_code'])
    // 									->sum('units');
    //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
    //        							->orderBy('date', 'desc')
    //        							->first();
    //        			$sum_amount = $invest_amount * $historic_navs['nav'];
    //        			$total_amount += $sum_amount;
    //           	}
    //           	$total_amount = round($total_amount, 2);
    //           	$total_amount_invested += $total_amount;
    //           	array_push($data_set, $total_amount);
    //           	array_push($scheme_array, $data_set);
    //        	}
    //        }

    // 	$final_array = [];
    //        $equity = 0;
    //        $debt = 0;
    //        $balanced = 0;
    //        foreach ($scheme_array as $value) {
    //        	$data_set_array = [];
    //        	$avg = 0;
    //    		$avg = ($value[1]/$total_amount_invested) * 100;
    //        	$avg = round($avg, 2);
    //        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
    //        	if ($type == 'equity') {
    //        		$equity += $value[1];
    //        	}else if ($type == 'balanced') {
    //        		$balanced += $value[1];
    //        	}else{
    //        		$debt += $value[1];
    //        	}

    //        	$amount = $fmt->format($value[1]);
    //        	array_push($data_set_array, $value[0], $amount, $avg);
    //        	array_push($final_array, $data_set_array);
    //        }


    //        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
    //        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
    //        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

    //        $equity = $fmt->format($equity);
    //        $debt = $fmt->format($debt);
    //        $balanced = $fmt->format($balanced);
    //        $total_amount_invested = $fmt->format($total_amount_invested);


    //        return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
    // }

    public function cispdf()
    {
        //  	   	$validator = \Validator::make($request->all(),[
        //  	   		'investor_id' => 'required',
        //  	   		'investor_type' => 'required',
        //  	   		'ex_nav_date' => 'required',
        // ]);
        $cis = $_SERVER['DOCUMENT_ROOT'] . '/cis.zip';
        if ($cis == true) {
            unlink($cis);
        }
        set_time_limit(600);
        $group_id = 2;
        $group_mem_id = array();
        $group_mem_id = GroupMembers::where('user_id', \Auth::user()->id)->where('group_id', $group_id)->pluck('id')->toArray();

        $count = 0;
        $cis_details = array();
        $yesterday_date = Carbon::now()->subDay(1)->toDateString();
        $yesterday_date = date('Y-m-d', strtotime($yesterday_date));
        $investment_detail = array();
        $total_m_value = 0;
        $profit_or_loss = 0;
        $absolute_returns_avg = 0;
        $total_amount_inv = 0;
        $scheme_inv_total = 0;
        $current_market_value = 0;

        $grand_total = 0;
        $grand_market_value = 0;
        $grand_profit_or_loss = 0;
        $grand_absolute_returns = 0;
        $units = 0;

        $unique_scheme = array();

        $check_them = array();
        $investor_ids = array();
        $investor_id = array();
        $separate_inv = array();

        $inv_pass = array();

        $change_date = $yesterday_date;
        foreach ($group_mem_id as $member_id) {
            $investor_id[] = $member_id;
        }

        $investment_detail = InvestmentDetails::where('user_id', \Auth::user()->id)->whereIn('investor_id', $investor_id)->where('investor_type', 'subperson')->where('purchase_date', '<=', $change_date)->get();


        foreach ($investment_detail as $sep_inv) {


            $investor_name = GroupMembers::where('id', $sep_inv->investor_id)->pluck('member_name');

            $investor_name = $investor_name[0];
            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Investor Name'] = $investor_name;
            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Scheme Name'] = $sep_inv->scheme_name;
            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Amount Invested'] = $sep_inv->amount_inv;
            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date'] = date('d-M-Y', strtotime($sep_inv->purchase_date));

            $current_nav = HistoricNavs::where('scheme_code', $sep_inv->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();

            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase NAV'] = $sep_inv->purchase_nav;
            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Units'] = $sep_inv->units;

            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current NAV'] = $current_nav['nav'];

            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'] = round($current_nav['nav'] * $sep_inv->units, 2);

            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'] = round(($current_nav['nav'] * $sep_inv->units) - ($sep_inv->amount_inv), 2);

            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss']) / ($sep_inv->amount_inv)) * 100, 2);

            $today = Carbon::now()->subDays(0)->toDateString();

            $date_diff = date_diff(date_create($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date']), date_create($today));
            $date_diff = $date_diff->format("%a");

            if ($date_diff == 0) {
                $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = 0;
            } else {
                $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns']) * (365 / $date_diff)), 2);
            }

            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Folio Number'] = $sep_inv->folio_number;


        }


        foreach ($investment_detail as $investments) {

            if (array_key_exists($investments->scheme_code, $inv_pass)) {


                $inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

                $inv_pass[$investments->scheme_code]['Amount Invested'] += $investments->amount_inv;


                $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();


                $inv_pass[$investments->scheme_code]['Units'] += round(($investments->units), 4);
                //$inv_pass[$investments->scheme_code]['units'] = $units;
                $inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];


                $current_market_value = (($inv_pass[$investments->scheme_code]['Units']) * $current_nav['nav']);

                $current_market_value = round($current_market_value, 2);
                $inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

                $inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

                $inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested']) / ($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100, 2);


            } else {

                $inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

                $inv_pass[$investments->scheme_code]['Amount Invested'] = $investments->amount_inv;

                $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
                $inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

                $inv_pass[$investments->scheme_code]['Units'] = round(($investments->units), 4);

                $current_market_value = (($investments->units) * $current_nav['nav']);

                $current_market_value = round($current_market_value, 2);

                $inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

                $inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

                $inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested']) / ($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100, 2);


            }

            $grand_total += $investments->amount_inv;

            $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->orderBy('date', 'aesc')->first();
            $current_nav = $current_nav['nav'];
            $units_held = $investments->units;

            $current_market_value = $units_held * $current_nav;

            $grand_market_value += $current_market_value;


            $count++;
        }

        $final_array = array();

        $count = 0;
        $pro_array = array();
        $member_name = '';
        $names = [];
        foreach ($separate_inv as $inv) {

            foreach ($inv as $in) {
                $member_name = $in['Investor Name'];
            }

//			               		dd($inv);
            $pdf = PDF::loadView('cispdf', ['inv' => $inv])->setPaper('a4', 'landscape')->setWarnings(false)->save($member_name . '.pdf');
            // return $pdf->download($member_name.'.pdf');
            // array_push($final_array, $pdf);
            // array_push($names, $member_name);

        }

        $zip = new ZipArchive;
        if ($zip->open('cis.zip', ZipArchive::CREATE) === TRUE) {
            foreach ($names as $value) {
                $zip->addFile($value . '.pdf');
            }

            $zip->close();
            foreach ($names as $value) {
                $path = $_SERVER['DOCUMENT_ROOT'] . '/' . $value . '.pdf';
                unlink($path);
            }
        }
        $file = public_path() . "/cis.zip";
        $headers = array(
            'Content-Type: application/zip',
        );
        return response()->download($file, 'cis.zip', $headers);
    }

    public function indexupdate()
    {

        // $nifty = file('http://www.nseindia.com/homepage/Indices1.json')

        $client = new \GuzzleHttp\Client();
        $res = $client->get('https://www.nseindia.com/live_market/dynaContent/live_watch/stock_watch/niftyStockWatch.json');
        $data = json_decode($res->getBody(), true);
        // dd($data);
        $niftydate = date('Y-m-d', strtotime($data['time']));
        $latest_data = $data['latestData'];
        $open_nifty = str_replace(',', '', $latest_data[0]['open']);
        $high_nifty = str_replace(',', '', $latest_data[0]['high']);
        $low_nifty = str_replace(',', '', $latest_data[0]['low']);
        $close_hifty = str_replace(',', '', $latest_data[0]['ltp']);
        // dd($open_nifty);
        $save = historic_nse::insert(['date' => $niftydate,
            'opening_index' => $open_nifty,
            'high' => $high_nifty,
            'low' => $low_nifty,
            'closing_index' => $close_hifty
        ]);


        $sensex = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=16');

        foreach ($sensex as $value) {

            $array = explode("@", $value);
            $date_split = explode("|", $array[9]);
            $mydate = date('Y-m-d', strtotime($date_split[0]));
            // dd($array)
            $save = historic_bse::insert(['date' => $mydate,
                'opening_index' => $array[2],
                'high' => $array[3],
                'low' => $array[4],
                'closing_index' => $array[5]
            ]);
        }


        $smallcap_data = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=82');

        foreach ($smallcap_data as $value) {

            $array = explode("@", $value);
            $date_split = explode("|", $array[9]);
            $mydate = date('Y-m-d', strtotime($date_split[0]));
            $save = smallcap::insert(['date' => $mydate,
                'open' => $array[2],
                'high' => $array[3],
                'low' => $array[4],
                'close' => $array[5]
            ]);


        }

        $midcap_data = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=81');

        foreach ($midcap_data as $value) {

            $array = explode("@", $value);
            $date_split = explode("|", $array[9]);
            $mydate = date('Y-m-d', strtotime($date_split[0]));
            $save = midcap::insert(['date' => $mydate,
                'open' => $array[2],
                'high' => $array[3],
                'low' => $array[4],
                'close' => $array[5]
            ]);


        }
        dd('sucessfuly update!!!!!');
    }


    // public function benchmark($id)
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

    // 	$esensex_total = 0;
    // 	$nifty_total = 0;

    // 	$compound = 0;
    // 	$rbi_current = 0;

    // 	$etotal_current = 0;
    // 	$etotal_inv = 0;

    // 	$dtotal_current = 0;
    // 	$dtotal_inv = 0;

    // 	$btotal_current = 0;
    // 	$btotal_inv = 0;

    // 	$equity = [];
    // 	$debt = [];
    // 	$balanced = [];

    // 	$today = Carbon::now();
    // 	$dateOrder = date('Y-m-d',strtotime($today));
    // 	$investments = InvestmentDetails::where('investor_id',$id)
    // 			->where('investor_type','individual')
    // 			->get();
    // 	$investor_name = Person::where('id',$id)->value('name');


    // 	foreach ($investments as $investment) {
    // 		$data_set = [];
    // 		$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
    // 		$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
    // 		if ($Scheme_type == 'equity') {
    // 			$etotal_current += $historic_navs['nav'] * $investment['units'];

    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$etotal_inv += $inv_amt;

    // 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
    // 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
    // 			$current_sensex_val = round($current_sensex_val, 2);
    // 			$esensex_total += $current_sensex_val;

    // 			$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_nifty = $inv_amt/$get_nifty['closing_index'];
    // 			$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
    // 			$current_nifty_val = round($current_nifty_val, 2);
    // 			$nifty_total += $current_nifty_val;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['if_sensex'] = $fmt->format($current_sensex_val);
    // 			$data_set['if_nifty'] = $fmt->format($current_nifty_val);
    // 			// dd($data_set);
    // 			array_push($equity, $data_set);

    // 		}elseif ($Scheme_type == 'debt') {

    // 			$rbi = $this->rbi_fd;
    // 			// $rbi = array_reverse($rbi);
    // 			$dtotal_current += $historic_navs['nav'] * $investment['units'];
    // 			// dd($rbi);
    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$dtotal_inv += $inv_amt;
    // 			$rate = 0;
    // 			foreach ($rbi as $value) {
    // 				if ($inv_date <= $value['date']) {
    // 					break;
    // 				}
    // 				$rate = $value['value'];
    // 			}

    // 			$rbi_set = ($rate/100) * $inv_amt;

    // 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
    // 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
    // 			$diff_in_days = $to->diffInDays($from);
    // 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
    // 			$rbi_current += $rbi_val + $inv_amt;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

    // 			array_push($debt, $data_set);

    // 		}else{
    // 			$btotal_current += $historic_navs['nav'] * $investment['units'];

    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$btotal_inv += $inv_amt;

    // 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
    // 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
    // 			$current_sensex_val = round($current_sensex_val, 2);
    // 			$current_sensex_val = (60/100) * $current_sensex_val;

    // 			$rbi = $this->rbi_fd;
    // 			$rate = 0;
    // 			foreach ($rbi as $value) {
    // 				if ($inv_date <= $value['date']) {
    // 					break;
    // 				}
    // 				$rate = $value['value'];
    // 			}

    // 			$rbi_set = ($rate/100) * $inv_amt;

    // 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
    // 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
    // 			$diff_in_days = $to->diffInDays($from);
    // 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
    // 			$rbi_val = $rbi_val + $inv_amt;
    // 			$rbi_val = (40/100) * $rbi_val;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

    // 			$compound += $current_sensex_val + $rbi_val;

    // 			array_push($balanced, $data_set);

    // 		}
    // 	}
    // 	// dd($debt);

    // 	$etotal_current = round($etotal_current, 2);
    // 	$dtotal_current = round($dtotal_current, 2);
    // 	$btotal_current = round($btotal_current, 2);
    // 	$sensex_diff = $esensex_total - $etotal_current;
    // 	$nifty_diff = $nifty_total - $etotal_current;
    // 	$rbi_deff = $rbi_current - $dtotal_current;
    // 	$compound_deff = $compound - $btotal_current;
    // 	$sensex_diff = round($sensex_diff, 2);
    // 	$nifty_diff = round($nifty_diff, 2);
    // 	$compound = round($compound, 2);
    // 	$rbi_current = round($rbi_current, 2);
    // 	$rbi_deff = round($rbi_deff, 2);
    // 	$compound_deff = round($compound_deff, 2);

    // 	// dd($etotal_current,$dtotal_current,$btotal_current);
    // 	$etotal_current = $fmt->format($etotal_current);
    // 	$dtotal_current = $fmt->format($dtotal_current);
    // 	$btotal_current = $fmt->format($btotal_current);
    // 	$esensex_total = $fmt->format($esensex_total);
    // 	$nifty_total = $fmt->format($nifty_total);
    // 	$etotal_inv = $fmt->format($etotal_inv);
    // 	$dtotal_inv = $fmt->format($dtotal_inv);
    // 	$btotal_inv = $fmt->format($btotal_inv);
    // 	$rbi_current = $fmt->format($rbi_current);
    // 	$compound = $fmt->format($compound);

    // 	return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
    // }

    // public function bmGroup($id)
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

    // 	$esensex_total = 0;
    // 	$nifty_total = 0;

    // 	$bsensex_total = 0;

    // 	$etotal_current = 0;
    // 	$etotal_inv = 0;

    // 	$dtotal_current = 0;
    // 	$dtotal_inv = 0;

    // 	$btotal_current = 0;
    // 	$btotal_inv = 0;

    // 	$compound = 0;
    // 	$rbi_current = 0;


    // 	$equity = [];
    // 	$debt = [];
    // 	$balanced = [];

    // 	$today = Carbon::now();
    // 	$dateOrder = date('Y-m-d',strtotime($today));
    // 	$investments = InvestmentDetails::where('investor_id',$id)
    // 			->where('investor_type','subperson')
    // 			->get();
    // 	$investor_name = GroupMembers::where('id',$id)->value('member_name');


    // 	foreach ($investments as $investment) {
    // 		$data_set = [];
    // 		$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
    // 		$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
    // 		if ($Scheme_type == 'equity') {
    // 			$etotal_current += $historic_navs['nav'] * $investment['units'];

    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$etotal_inv += $inv_amt;

    // 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
    // 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
    // 			$current_sensex_val = round($current_sensex_val, 2);
    // 			$esensex_total += $current_sensex_val;

    // 			$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_nifty = $inv_amt/$get_nifty['closing_index'];
    // 			$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
    // 			$current_nifty_val = round($current_nifty_val, 2);
    // 			$nifty_total += $current_nifty_val;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['if_sensex'] = $fmt->format($current_sensex_val);
    // 			$data_set['if_nifty'] = $fmt->format($current_nifty_val);
    // 			// dd($data_set);
    // 			array_push($equity, $data_set);

    // 		}elseif ($Scheme_type == 'debt') {

    // 			$rbi = $this->rbi_fd;
    // 			$rbi = array_reverse($rbi);
    // 			$dtotal_current += $historic_navs['nav'] * $investment['units'];

    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$dtotal_inv += $inv_amt;

    // 			$rbi_set = 0;
    // 			foreach ($rbi as $value) {
    // 				if ($inv_date <= $value['date']) {
    // 					$temp = $value['value'];
    // 					$rbi_set = ($temp/100) * $inv_amt;
    // 					break;
    // 				}
    // 			}


    // 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
    // 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
    // 			$diff_in_days = $to->diffInDays($from);
    // 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
    // 			$rbi_current += $rbi_val + $inv_amt;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

    // 			array_push($debt, $data_set);

    // 		}else{
    // 			$btotal_current += $historic_navs['nav'] * $investment['units'];

    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$btotal_inv += $inv_amt;

    // 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
    // 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
    // 			$current_sensex_val = round($current_sensex_val, 2);
    // 			$bsensex_total += $current_sensex_val;
    // 			$current_sensex_val = (60/100) * $current_sensex_val;

    // 			$rbi = $this->rbi_fd;
    // 			$rate = 0;
    // 			foreach ($rbi as $value) {
    // 				if ($inv_date <= $value['date']) {
    // 					break;
    // 				}
    // 				$rate = $value['value'];
    // 			}

    // 			$rbi_set = ($rate/100) * $inv_amt;

    // 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
    // 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
    // 			$diff_in_days = $to->diffInDays($from);
    // 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
    // 			$rbi_val = $rbi_val + $inv_amt;
    // 			$rbi_val = (40/100) * $rbi_val;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

    // 			$compound += $current_sensex_val + $rbi_val;

    // 			array_push($balanced, $data_set);

    // 		}
    // 	}

    // 	$etotal_current = round($etotal_current, 2);
    // 	$dtotal_current = round($dtotal_current, 2);
    // 	$btotal_current = round($btotal_current, 2);
    // 	$sensex_diff = $esensex_total - $etotal_current;
    // 	$nifty_diff = $nifty_total - $etotal_current;
    // 	$rbi_deff = $rbi_current - $dtotal_current;
    // 	$compound_deff = $compound - $btotal_current;
    // 	$sensex_diff = round($sensex_diff, 2);
    // 	$nifty_diff = round($nifty_diff, 2);
    // 	$compound = round($compound, 2);
    // 	$rbi_current = round($rbi_current, 2);
    // 	$rbi_deff = round($rbi_deff, 2);
    // 	$compound_deff = round($compound_deff, 2);

    // 	$etotal_current = $fmt->format($etotal_current);
    // 	$dtotal_current = $fmt->format($dtotal_current);
    // 	$btotal_current = $fmt->format($btotal_current);
    // 	$esensex_total = $fmt->format($esensex_total);
    // 	$nifty_total = $fmt->format($nifty_total);
    // 	$etotal_inv = $fmt->format($etotal_inv);
    // 	$dtotal_inv = $fmt->format($dtotal_inv);
    // 	$btotal_inv = $fmt->format($btotal_inv);
    // 	$rbi_current = $fmt->format($rbi_current);
    // 	$compound = $fmt->format($compound);

    // 	return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
    // }

    // public function bmCIS($id)
    // {
    // 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

    // 	$esensex_total = 0;
    // 	$nifty_total = 0;

    // 	$bsensex_total = 0;

    // 	$etotal_current = 0;
    // 	$etotal_inv = 0;

    // 	$dtotal_current = 0;
    // 	$dtotal_inv = 0;

    // 	$btotal_current = 0;
    // 	$btotal_inv = 0;

    // 	$compound = 0;
    // 	$rbi_current = 0;

    // 	$equity = [];
    // 	$debt = [];
    // 	$balanced = [];

    // 	$today = Carbon::now();
    // 	$dateOrder = date('Y-m-d',strtotime($today));

    // 	$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$id)->pluck('id')->toArray();

    // 	$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$group_mem_id)
    // 					->where('investor_type','subperson')
    // 					->get();

    // 	foreach ($investment_detail as $investment) {
    // 		$investor_name = GroupMembers::where('id',$investment['investor_id'])->value('member_name');
    // 		$data_set = [];
    // 		$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
    // 		$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
    // 		if ($Scheme_type == 'equity') {
    // 			$etotal_current += $historic_navs['nav'] * $investment['units'];

    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$etotal_inv += $inv_amt;

    // 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
    // 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
    // 			$current_sensex_val = round($current_sensex_val, 2);
    // 			$esensex_total += $current_sensex_val;

    // 			$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_nifty = $inv_amt/$get_nifty['closing_index'];
    // 			$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
    // 			$current_nifty_val = round($current_nifty_val, 2);
    // 			$nifty_total += $current_nifty_val;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['if_sensex'] = $fmt->format($current_sensex_val);
    // 			$data_set['if_nifty'] = $fmt->format($current_nifty_val);
    // 			// dd($data_set);
    // 			array_push($equity, $data_set);

    // 		}elseif ($Scheme_type == 'debt') {

    // 			$rbi = $this->rbi_fd;
    // 			// $rbi = array_reverse($rbi);
    // 			$dtotal_current += $historic_navs['nav'] * $investment['units'];
    // 			// dd($rbi);
    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$dtotal_inv += $inv_amt;
    // 			$rate = 0;
    // 			foreach ($rbi as $value) {
    // 				if ($inv_date <= $value['date']) {
    // 					break;
    // 				}
    // 				$rate = $value['value'];
    // 			}

    // 			$rbi_set = ($rate/100) * $inv_amt;

    // 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
    // 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
    // 			$diff_in_days = $to->diffInDays($from);
    // 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
    // 			$rbi_current = $rbi_val + $inv_amt;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

    // 			array_push($debt, $data_set);

    // 		}else{
    // 			$btotal_current += $historic_navs['nav'] * $investment['units'];

    // 			$inv_date = $investment['purchase_date'];
    // 			$inv_amt = $investment['amount_inv'];
    // 			$btotal_inv += $inv_amt;

    // 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
    // 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
    // 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
    // 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
    // 			$current_sensex_val = round($current_sensex_val, 2);
    // 			$bsensex_total += $current_sensex_val;
    // 			$current_sensex_val = (60/100) * $current_sensex_val;

    // 			$rbi = $this->rbi_fd;
    // 			$rate = 0;
    // 			foreach ($rbi as $value) {
    // 				if ($inv_date <= $value['date']) {
    // 					break;
    // 				}
    // 				$rate = $value['value'];
    // 			}

    // 			$rbi_set = ($rate/100) * $inv_amt;

    // 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
    // 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
    // 			$diff_in_days = $to->diffInDays($from);
    // 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
    // 			$rbi_val = $rbi_val + $inv_amt;
    // 			$rbi_val = (40/100) * $rbi_val;

    // 			$data_set['name'] = $investor_name;
    // 			$data_set['dop'] = $inv_date;
    // 			$data_set['inv_amt'] = $fmt->format($inv_amt);
    // 			$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

    // 			$compound += $current_sensex_val + $rbi_val;

    // 			array_push($balanced, $data_set);

    // 		}
    // 	}
    // 	// dd($equity);
    // 	$etotal_current = round($etotal_current, 2);
    // 	$dtotal_current = round($dtotal_current, 2);
    // 	$btotal_current = round($btotal_current, 2);
    // 	$sensex_diff = $esensex_total - $etotal_current;
    // 	$nifty_diff = $nifty_total - $etotal_current;
    // 	$rbi_deff = $rbi_current - $dtotal_current;
    // 	$compound_deff = $compound - $btotal_current;
    // 	$sensex_diff = round($sensex_diff, 2);
    // 	$nifty_diff = round($nifty_diff, 2);
    // 	$compound = round($compound, 2);
    // 	$rbi_current = round($rbi_current, 2);
    // 	$rbi_deff = round($rbi_deff, 2);
    // 	$compound_deff = round($compound_deff, 2);

    // 	$etotal_current = $fmt->format($etotal_current);
    // 	$dtotal_current = $fmt->format($dtotal_current);
    // 	$btotal_current = $fmt->format($btotal_current);
    // 	$esensex_total = $fmt->format($esensex_total);
    // 	$nifty_total = $fmt->format($nifty_total);
    // 	$etotal_inv = $fmt->format($etotal_inv);
    // 	$dtotal_inv = $fmt->format($dtotal_inv);
    // 	$btotal_inv = $fmt->format($btotal_inv);
    // 	$rbi_current = $fmt->format($rbi_current);
    // 	$compound = $fmt->format($compound);

    // 	return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
    // }

    public function csvUpdate()
    {
        // set_time_limit(2400);


        dd('smallcap & midcap update sucessfuly');


    }

    public function getPerson(Request $request)
    {
        // dd($request['investor_type']);
        if ($request['investor_type'] == 'subperson') {
            $data = GroupMembers::where('id', $request['investor_id'])->get();
            return response()->json(['msg' => 1, 'data' => $data]);
            // dd($data);
        } else {
            $data = Person::where('id', $request['investor_id'])->get();
            return response()->json(['msg' => 2, 'data' => $data]);
        }
    }

    public function invUpdate(Request $request)
    {
        // edit_name
        // inv_id
        // dd($request['inv_id']);
        $save = InvestmentDetails::where('id', $request['inv_id'])
            ->update(['investment_type' => $request['edit_name']]);
        if ($save == 1) {
            return response()->json(['msg' => 1]);
        } else {
            return response()->json(['msg' => 2]);
        }
    }

    public function updateAum(Request $request)
    {
        // http_response_code(500);
        // dd($request->all());
        $save = InvestmentDetails::where('id', $request['inv_id'])
            ->update(['not_aum' => $request['aum_state']]);
        if ($save == 1) {
            return response()->json(['msg' => 1]);
        } else {
            return response()->json(['msg' => 2]);
        }

    }

    public function redeemAmount(Request $request)
    {
        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);
        $validator = \Validator::make($request->all(), [

            'inv-id' => 'required',
            'redeem-units' => 'required',
            'redeem-date' => 'required',
            'stt' => 'required',
            'exit-load' => 'required',
            'all-units' => 'required',
//            'price-nav' => 'required',

        ]);

//        die();

        if ($validator->fails()) {

            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found']);

        } else {

//            dd($_POST);

            $redeem_date = date('Y-m-d', strtotime($request['redeem-date']));
            $inv_id = $request['inv-id'];
            $redemption_units = $request['redeem-units'];

            $investment_det = InvestmentDetails::where('id', $inv_id)->first()->toArray();
//            return response()->json($investment_det);

            $total_available_inv = InvestmentDetails::where('id', $inv_id)
//                ->where('investor_id',$investment_det['investor_id'])
//                ->where('folio_number',$investment_det['folio_number'])
//                ->where('status','active')
//                ->orWhere('status','paractive')
//                ->orderBy('purchase_date','asc')
                ->get();

            // dd($total_available_inv);

            $total_withdraw_amount = WithdrawDetails::where('investment_id', $inv_id)
//                ->where('investor_id',$investment_det['investor_id'])
//                ->where('folio_number',$investment_det['folio_number'])
                ->get();


//            dd($total_available_inv, $total_withdraw_amount);


            $current_nav = HistoricNavs::where('scheme_code', $investment_det['scheme_code'])->where('date', '<=', $redeem_date)->orderBy('date', 'aesc')->first();
//            dd($current_nav['nav'], $current_nav, $redeem_date);
            // $current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();
            $total_units_held = $total_available_inv->sum('units');
            $total_withdrawn_units = $total_withdraw_amount->sum('units');
            $total_available_units = $total_units_held;
//            $amount_left = $total_available_amount;
//            return response()->json([$total_units_held, $total_withdrawn_units]);

            //checking whether the entered redemtion amount is greater than the available amount
            if ($total_available_units < $request['redeem-units']) {
//                $total_available_amount = round($total_available_amount, 2);
                // return response()->json([$total_available_amount, $redemption_amount]);
//                return response()->json(['msg'=>2, 'response'=>'Redemption amount is more than the available amount - Rs.'.$fmt->format($total_available_amount)]);
                return response()->json(['msg' => 2, 'response' => 'Redemption units is more than the available units', 'units' => $total_available_units]);
            } else {

                if ($request['all-units'] == "yes") {
                    foreach ($total_available_inv as $inv) {
                        if ($request['exit-load'] >= 0) {
                            $redemption_amount = ($inv->units * $current_nav['nav']) - ($request['exit-load'] + $request['stt']);
                        }

                        $amc_name = SchemeDetails::where('scheme_code', $inv->scheme_code)->pluck('amc_name');
                        $amc_name = $amc_name[0];


                        $withdraw = new WithdrawDetails();
                        $withdraw->user_id = $inv->user_id;
                        $withdraw->investment_id = $inv->id;
                        $withdraw->investor_id = $inv->investor_id;
                        $withdraw->investor_type = $inv->investor_type;
                        $withdraw->scheme_name = $inv->scheme_name;
                        $withdraw->scheme_code = $inv->scheme_code;
                        $withdraw->stt = $request['stt'];
                        $withdraw->amc = $amc_name;
                        $withdraw->exit_load = $request['exit-load'];
                        $withdraw->invested_amount = $inv->units * $inv->purchase_nav;
                        $withdraw->withdraw_amount = round($redemption_amount, 2);
                        $withdraw->purchase_date = $inv->purchase_date;
                        $withdraw->scheme_type = $inv->scheme_type;
                        $withdraw->withdraw_date = date('Y-m-d', strtotime($redeem_date));
                        $withdraw->purchase_nav = $inv->purchase_nav;
                        $withdraw->current_nav = $current_nav['nav'];
                        $withdraw->units = $inv->units;
//                        $withdraw->price_nav = $request['price-nav'];
                        $withdraw->folio_number = $inv->folio_number;
                        $withdraw->save();

                        InvestmentDetails::where('id', $inv->id)->update(['status' => 'inactive']);
                    }
                } else {

                    foreach ($total_available_inv as $inv) {
                        if ($request['exit-load'] >= 0) {
                            $redemption_amount = ($request['redeem-units'] * $current_nav['nav']) - ($request['exit-load'] + $request['stt']);
                        }
                        $amc_name = SchemeDetails::where('scheme_code', $inv->scheme_code)->pluck('amc_name');
                        $amc_name = $amc_name[0];


                        $withdraw = new WithdrawDetails();
                        $withdraw->user_id = $inv->user_id;
                        $withdraw->investment_id = $inv->id;
                        $withdraw->investor_id = $inv->investor_id;
                        $withdraw->investor_type = $inv->investor_type;
                        $withdraw->scheme_name = $inv->scheme_name;
                        $withdraw->scheme_code = $inv->scheme_code;
                        $withdraw->stt = $request['stt'];
                        $withdraw->amc = $amc_name;
                        $withdraw->exit_load = $request['exit-load'];
                        $withdraw->invested_amount = $redemption_units * $inv->purchase_nav;
                        $withdraw->withdraw_amount = round($redemption_amount, 2);
                        $withdraw->purchase_date = $inv->purchase_date;
                        $withdraw->scheme_type = $inv->scheme_type;
                        $withdraw->withdraw_date = date('Y-m-d', strtotime($redeem_date));
                        $withdraw->purchase_nav = $inv->purchase_nav;
                        $withdraw->current_nav = $current_nav['nav'];
                        $withdraw->units = $request['redeem-units'];
//                        $withdraw->price_nav = $request['price-nav'];
                        $withdraw->folio_number = $inv->folio_number;
                        $withdraw->save();

                        InvestmentDetails::where('id', $inv->id)->update(['status' => 'paractive', 'amount_inv' => ($inv->amount_inv - round(($redemption_units * $inv->purchase_nav), 0)), 'units' => ($inv->units - $redemption_units)]);

                    }

                }

//                foreach($total_available_inv as $inv){
//                    $value = $inv->units * $current_nav['nav'];
//                    if($redemption_amount != 0){
//                        if($value >= $redemption_amount){
////                            return response()->json([$amount_left, $value, $redemption_amount]);
//
//
//
//
//                            $withdraw = new WithdrawDetails();
//                            $withdraw->user_id = $inv->user_id;
//                            $withdraw->investment_id = $inv->id;
//                            $withdraw->investor_id = $inv->investor_id;
//                            $withdraw->investor_type = $inv->investor_type;
//                            $withdraw->scheme_name = $inv->scheme_name;
//                            $withdraw->scheme_code = $inv->scheme_code;
//                            $withdraw->invested_amount = round(($redemption_amount/$current_nav['nav']) * $inv->purchase_nav, 0);
//                            $withdraw->withdraw_amount = round($redemption_amount, 2);
//                            $withdraw->purchase_date = $inv->purchase_date;
//                            $withdraw->scheme_type = $inv->scheme_type;
//                            $withdraw->withdraw_date = date('Y-m-d',strtotime(Carbon::now()->toDateString()));
//                            $withdraw->purchase_nav = $inv->purchase_nav;
//                            $withdraw->current_nav = $current_nav['nav'];
//                            $withdraw->units = round($redemption_amount/$current_nav['nav'], 2);
//                            $withdraw->folio_number = $inv->folio_number;
//
//                            $withdraw->save();
//
//
//
//
//
//                            if($value > 0){
//                                InvestmentDetails::where('id',$inv->id)->update(['status'=>'paractive','amount_inv'=>($inv->amount_inv - round(($redemption_amount/$current_nav['nav']) * $inv->purchase_nav, 0))]);
//                            }else{
//                                InvestmentDetails::where('id',$inv->id)->update(['status'=>'inactive']);
////                                $inv->update(['status'=>'inactive']);
//                            }
//
//                            $amount_left -= $redemption_amount;
////
//                            $value -= $redemption_amount;
//                            $redemption_amount = 0;
//
//
//                        }else{
//                            $amount_left -= $value;
//                            $redemption_amount -= $value;
//
//                            $withdraw = new WithdrawDetails();
//                            $withdraw->user_id = $inv->user_id;
//                            $withdraw->investment_id = $inv->id;
//                            $withdraw->investor_id = $inv->investor_id;
//                            $withdraw->investor_type = $inv->investor_type;
//                            $withdraw->scheme_name = $inv->scheme_name;
//                            $withdraw->scheme_code = $inv->scheme_code;
//                            $withdraw->purchase_date = $inv->purchase_date;
//                            $withdraw->invested_amount = round(($value/$current_nav['nav']) * $inv->purchase_nav, 0);
//                            $withdraw->withdraw_amount = round($value, 2);
//                            $withdraw->scheme_type = $inv->scheme_type;
//                            $withdraw->withdraw_date = date('Y-m-d',strtotime(Carbon::now()->toDateString()));
//                            $withdraw->purchase_nav = $inv->purchase_nav;
//                            $withdraw->current_nav = $current_nav['nav'];
//                            $withdraw->units = round($value/$current_nav['nav'], 2);
//                            $withdraw->folio_number = $inv->folio_number;
//
//                            $withdraw->save();
//                            InvestmentDetails::where('id',$inv->id)->update(['status'=>'inactive']);
//
//                        }
////                        return response()->json([$total_units_held * $current_nav['nav'], $amount_left, $redemption_amount]);
//                    }
//
//
//                }

                $withdraw = WithdrawDetails::where('investment_id', $request['inv-id'])->get();

                return response()->json(['msg' => 1]);
//                return $this->getInvestment();
            }
        }
    }


    public function updateAmcNames()
    {
        $investment_details = InvestmentDetails::all();
        foreach ($investment_details as $investment) {
            $scheme = SchemeDetails::where('scheme_code', $investment->scheme_code)->pluck('amc_name');
//	        echo $scheme[0]."<br>";
            InvestmentDetails::where('id', $investment->id)->update(['amc' => $scheme[0]]);
        }


        echo "----------- WIthdraw -----------";

        $withdraw_details = WithdrawDetails::all();
        foreach ($withdraw_details as $withdraw) {
            $scheme = SchemeDetails::where('scheme_code', $withdraw->scheme_code)->pluck('amc_name');
//            echo $scheme[0]."<br>";
            WithdrawDetails::where('id', $withdraw->id)->update(['amc' => $scheme[0]]);
        }
    }




    private function getDaysDifference($purchase_date, $month_end_date, $redemption_date, $status, $inv_id, $scheme_code, $month, $amc)
    {


        $no_of_days = date('t', strtotime($month_end_date));

//        $month_start = date('Y-m-d', strtotime())

//        dd($month_start, $month_end_date, $amc);

        if ($month == "06") {


            $month_start = date('Y-m-01', strtotime($month_end_date));



            if($amc == "franklin"){
                $month_end_date = date('Y-m-30', strtotime($month_end_date));
            }elseif($amc == "reliance"){
                $month_end_date = date('Y-m-20', strtotime($month_end_date));
            }elseif($amc == "tata"){
                $month_end_date = date('Y-m-21', strtotime($month_end_date));
            }elseif($amc == "l&t"){
                $month_end_date = date('Y-m-23', strtotime($month_end_date));
            }
            else{
                $month_end_date = date('Y-m-22', strtotime($month_end_date));

            }




        } elseif ($month == "07") {


            if($amc == "franklin"){

                $month_start = date('Y-07-01', strtotime($month_end_date));
                $month_end_date = date('Y-m-d', strtotime($month_end_date));

            }else if($amc == "reliance"){
                $month_start = date('Y-06-21', strtotime($month_end_date));
                $month_end_date = date('Y-m-d', strtotime($month_end_date));
            }else if($amc == "tata"){

                $month_start = date('Y-06-22', strtotime($month_end_date));
                $month_end_date = date('Y-m-d', strtotime($month_end_date));
            }
            else if($amc == "l&t"){
                $month_start = date('Y-06-24', strtotime($month_end_date));
                $month_end_date = date('Y-m-d', strtotime($month_end_date));
            }else{
                $month_start = date('Y-06-23', strtotime($month_end_date));
                $month_end_date = date('Y-m-d', strtotime($month_end_date));

            }


        }else{
            $month_start = date('Y-m-01', strtotime($month_end_date));
            $month_end_date = date('Y-m-d', strtotime($month_end_date));
        }


        $days_invested;
        $average_nav;




        if ($status == "active") {





            $days_invested = date_diff(date_create($purchase_date), date_create($month_end_date));
            $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$purchase_date, $month_end_date])->avg('nav');


            if(($month_start > $purchase_date)){

                $days_invested= date_diff(date_create($month_start), date_create($month_end_date));
                

            }






                if ((int)$days_invested->format('%R%a') > $no_of_days) {

//

                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$month_start, $month_end_date])->avg('nav');
                    $days_invested = $no_of_days;

                    if($month == '07'){
                        $days_invested = date_diff(date_create($month_start), date_create($month_end_date));
                        $days_invested = (int)$days_invested->format('%R%a') + 1;
                    }

//                    if($inv_id == '1248'){
//                        dd($days_invested);
//                    }
                    return [$days_invested, $average_nav];

                }


            $days_invested = (int)$days_invested->format('%R%a') + 1;




            return [$days_invested, $average_nav];




        } else if ($status == "inactive") {

            $redemption_date = date('Y-m-d', strtotime($redemption_date));


            try {



                if (($purchase_date >= $month_start) && ($month_end_date <= $redemption_date)) {

                    $days_invested = date_diff(date_create($purchase_date), date_create($month_end_date));
                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$purchase_date, $month_end_date])->avg('nav');


                } else if (($purchase_date >= $month_start) && ($redemption_date <= $month_end_date)) {
                    //if purchase a redemption was done with in a month of purchase

                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$purchase_date, $redemption_date])->avg('nav');
                    $days_invested = date_diff(date_create($purchase_date), date_create($redemption_date));

                } else if (($month_start >= $purchase_date) && ($month_end_date <= $redemption_date)) {
                    $days_invested = date_diff(date_create($month_start), date_create($month_end_date));
                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$month_start, $month_end_date])->avg('nav');


                } else if (($month_start > $purchase_date) && ($month_end_date > $redemption_date)) {
                    $days_invested = date_diff(date_create($month_start), date_create($redemption_date));
                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$month_start, $redemption_date])->avg('nav');

                } else if (($month_start > $purchase_date) && ($month_start > $redemption_date)) {
                    // Just getting difference of days to give 0
                    return [0, 0];

                }

                else if ($month_start == $redemption_date || $month_end_date == $redemption_date){
                    $current_nav = HistoricNavs::where('scheme_code', $scheme_code)->where('date', '<=', $redemption_date)->orderBy('date', 'aesc')->first();
                    $current_nav = $current_nav['nav'];

                    $days_invested = 1;

                }else if($month_end_date == $purchase_date || $month_start == $purchase_date){
                    $current_nav = HistoricNavs::where('scheme_code', $scheme_code)->where('date', '<=', $purchase_date)->orderBy('date', 'aesc')->first();
                    $current_nav = $current_nav['nav'];

                    $days_invested = 1;
                }

                $days_invested = (int)$days_invested->format('%R%a');

                if ($days_invested < 0) {
                    return [0, 0];

                }
                return [$days_invested + 1, $average_nav];

            } catch (\ErrorException $s) {
//                dd($purchase_date, $redemption_date, $month_start, $month_end_date, $inv_id);

            }


        } else {
            try {

                if (($purchase_date >= $month_start) && ($month_end_date <= $redemption_date)) {

                    $days_invested = date_diff(date_create($purchase_date), date_create($month_end_date));
                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$purchase_date, $month_end_date])->avg('nav');

                } else if (($purchase_date >= $month_start) && ($redemption_date <= $month_end_date)) {
                    //if purchase a redemption was done with in a month of purchase

                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$purchase_date, $redemption_date])->avg('nav');
                    $days_invested = date_diff(date_create($purchase_date), date_create($redemption_date));

                } else if (($month_start >= $purchase_date) && ($month_end_date <= $redemption_date)) {
                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$month_start, $month_end_date])->avg('nav');
                    $days_invested = date_diff(date_create($month_start), date_create($month_end_date));

                } else if (($month_start > $purchase_date) && ($month_end_date > $redemption_date)) {

                    $days_invested = date_diff(date_create($month_start), date_create($redemption_date));
                    $average_nav = HistoricNavs::where('scheme_code', $scheme_code)->whereBetween('date', [$month_start, $redemption_date])->avg('nav');

                } else if (($month_start > $purchase_date) && ($month_start > $redemption_date)) {
                    // Just getting difference of days to give 0
                    return [0, 0];

                }

                $days_invested = (int)$days_invested->format('%R%a');

                if ($days_invested < 0) {
                    return [0, 0];

                }
                return [$days_invested + 1, $average_nav];

            } catch (\ErrorException $s) {
//                dd($purchase_date, $redemption_date, $month_start, $month_end_date, $inv_id , 'blahhh');

            }
        }


    }

//    private function getAverageNav($start_date, $end_date){
//        $average_nav = HistoricNavs::where('scheme_code',$scheme_code)->whereBetween('date',[$month_end_date, $purchase_date])->groupBy('date')->avg('nav');
//
//    }


    private function getNetAmountInvested($current_market_value, $realised_profit_or_loss, $unreal_profit_or_loss)
    {
//        dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);


        $net_amount_invested = 0;

        if ($realised_profit_or_loss < 0) {

            if ($unreal_profit_or_loss > 0) {
                $net_amount_invested = ($current_market_value - $unreal_profit_or_loss) + abs($realised_profit_or_loss);
//
            }

        }


        if ($realised_profit_or_loss > 0) {

            if ($unreal_profit_or_loss < 0) {
                $net_amount_invested = ($current_market_value + abs($unreal_profit_or_loss)) - $realised_profit_or_loss;
//                dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
            }

        }

        if ($realised_profit_or_loss > 0) {

            if ($unreal_profit_or_loss > 0) {
                $net_amount_invested = $current_market_value - (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss));
//                dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
            }

        }


        if ($realised_profit_or_loss < 0) {

            if ($unreal_profit_or_loss < 0) {
                $net_amount_invested = $current_market_value + (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss));
//                dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
            }

        }


        if ($realised_profit_or_loss == 0) {
            if ($unreal_profit_or_loss > 0) {
                $net_amount_invested = $current_market_value - $unreal_profit_or_loss;
            }
        }

        if ($realised_profit_or_loss == 0) {
            if ($unreal_profit_or_loss < 0) {
                $net_amount_invested = $current_market_value + (abs($unreal_profit_or_loss));
            }
        }

//        dd($net_amount_invested);

        return $net_amount_invested;
    }


    public function deleteRedemption(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'withdraw_id' => 'required',

        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found']);

        } else {
            $withdraw = WithdrawDetails::where('id', $request['withdraw_id'])->first();
//            dd($withdraw);
            $withdraws_of_investment = WithdrawDetails::where('investment_id', $withdraw->investment_id)->get();
            $withdraw_count = $withdraws_of_investment->count();

            $investment = InvestmentDetails::where('id', $withdraw->investment_id)->first();

            if ($investment->status == "inactive") {


                if ($withdraw_count == 1) {
                    InvestmentDetails::where('id', $withdraw->investment_id)->update(['status' => 'active'
                    ]);
                } else {
                    InvestmentDetails::where('id', $withdraw->investment_id)->update([
                        'status' => 'paractive',
                        'amount_inv' => ($investment->amount_inv + ($withdraw->units * $withdraw->purchase_nav)),
                        'units' => ($investment->units + $withdraw->units)
                    ]);
                }


            } else if ($investment->status == "paractive") {


                if ($withdraw_count == 1) {
                    //make the investement active
                    InvestmentDetails::where('id', $withdraw->investment_id)->update(['status' => 'active',
                        'amount_inv' => ($investment->amount_inv + ($withdraw->units * $withdraw->purchase_nav)),
                        'units' => ($investment->units + $withdraw->units)]);
                } else {
                    //make the investment paractive
                    InvestmentDetails::where('id', $withdraw->investment_id)->update([
                        'status' => 'paractive',
                        'amount_inv' => ($investment->amount_inv + ($withdraw->units * $withdraw->purchase_nav)),
                        'units' => ($investment->units + $withdraw->units)
                    ]);

                }

            }

            $delete = WithdrawDetails::where('id', $request['withdraw_id'])->delete();

            if ($delete) {
                return response()->json(['msg' => 1, 'response' => 'Withdraw Deleted Successfully. Kindly reload and check']);

            }

            return response()->json(['msg' => 1, 'response' => 'Withdraw Deleted Successfully. Kindly reload and check']);


        }
    }


    public function showBondsPage()
    {

//        $bond_details = BondDetails::all()->groupBy('bondinvestor_id');
//
//        $group_ids = [];
//
//        foreach ($bond_details as $inv_id => $bond_detail){
//            foreach ($bond_detail as $bond){
//
//                if($bond->bondinvestor_type == "App\GroupMembers"){
//                    $id =  GroupMembers::where('id',$bond->bondinvestor_id)->first();
//                    $group_ids[] = $id->group_id;
//                }else{
//
//                    $id =
//
//                }
//
//            }
//
//        }
//
//        dd($group_ids);
        if (\Auth::user()) {


            $schemes = SchemeDetails::all();
            $groups = Group::where('user_id', \Auth::user()->id)->where('active_state',1)->get();
            $persons = Person::where('user_id', \Auth::user()->id)->where('active_state',1)->get();
            $group_members = GroupMembers::where('user_id', \Auth::user()->id)->where('active_state',1)->get();
            $twoday = Carbon::now()->addDays(2);
            $today = Carbon::now();

            $queries = Queries::
            where("date", "<", $twoday)
                ->where("date", ">", $today)
                ->get();


            return view('bonds-home')->with(compact('groups', 'persons', 'group_members', 'schemes', 'queries'));

        } else {
            return view('login');
        }
    }




    public function showSipList(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',

        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found']);

        }

        $sip_investments = InvestmentDetails::where('investor_id', $request['investor_id'])->where('investor_type', $request['investor_type'])->where('investment_type', 2)->get()->groupBy('scheme_name');


    }


    public function editInvestment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'investment-id' => 'required',
            'trail-fee' => 'required',
            'upfront-fee' => 'required',

        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found']);

        }

        $investment = InvestmentDetails::where('id', $request['investment-id'])->update([
            'trail_fee_percent' => $request['trail-fee'],
            'upfront_fee_percent' => $request['upfront-fee'],
            'last_fee_update' => date('Y-m-d'),
        ]);

//        dd(InvestmentDetails::where('id',$request['investment-id'])->get());


        if ($investment) {
            return response()->json(['msg' => 1, 'response' => 'Investement details have been updated. Kindly refresh']);
        }
        return response()->json(['msg' => 2, 'response' => 'Failed to update investment. Kindly refresh and redo']);

    }


    private function updateBrokerage()
    {
        foreach (InvestmentDetails::all() as $inv) {
            $inv->update([
                'trail_fee_percent' => 0.8,
                'upfront_fee_percent' => 0.3,
            ]);
        }
    }


    public function addBondInvestment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',
            'name' => 'required',
            'bond-payout-type' => 'required',
            'bond-type' => 'required',
            'doi' => 'required',
            'tob' => 'required',
//            'due-date' => 'required',
            'interest-rate' => 'required',
//            'interest-frequency' => 'required',
//            'date-of-purchase' => 'required',
//            'date-of-interest' => 'required',
            // 'amount-invested' => 'required',
//            'moc' => 'required',
            'bond-receipt-no' => 'required',
            'credit-rating' => 'required',
            'initial-value' => 'required',
            'final-value' => 'required',
        ]);



        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found', 'error' => $validator->errors()]);

        }



        try {

            if ($request['investor_type'] == 'group_member') {
                $investor = GroupMembers::where('id', $request['investor_id'])->first();
            } else {
                $investor = Person::where('id', $request['investor_id'])->first();
            }

            if($request['bond-payout-type'] == 'non-cumulative'){
                if($request['initial-value'] != $request['final-value']){
                    return response()->json(['msg' => 2,  'error' => 'Initial Value Should be equal to Final Value in Non Cumulative bonds.']);
                }
            }

            if($request['bond-payout-type'] == 'cumulative'){
                if($request['initial-value'] >= $request['final-value']){
                    return response()->json(['msg' => 2,  'error' => 'Final Value Should be greater than initial value for Cumulative Bonds.']);
                }
            }


            $real_interest_rate = (pow(($request['final-value']/$request['initial-value']),(1/$request['tob']))-1)*100;





            $bond_overall_details = $this->calculateBondOverallDetails($request['investor_id'], ($request['investor_type'] == "individual" ? 'App\Person' : 'App\GroupMembers'));


            $bond_investment = $investor->bondInvestment()->create([

                'name' => $request['name'],
                'bond_payout_type' => $request['bond-payout-type'],
                'bond_type' => $request['bond-type'],
                'date_of_issue' => date('Y-m-d', strtotime($request['doi'])),
                'bond_term' => $request['tob'],
//                'due_date' => date('Y-m-d', strtotime($request['due-date'])),
//                'date_of_purchase' => date('Y-m-d', strtotime($request['date-of-purchase'])),
                'interest_rate' => $request['interest-rate'],
//                'interest_frequency' => $request['interest-frequency'],
//                'date_of_interest' => date('Y-m-d', strtotime($request['date-of-interest'])),
                'amount_invested' => $request['initial-value'],
                'initial_value' => $request['initial-value'],
                'final_value' => $request['final-value'],
                'real_interest_rate' => $real_interest_rate,
//                'moc' => $request['moc'],
                'amc' => BondMaster::where('name', $request['name'])->value('amc'),
                'bond_receipt' => $request['bond-receipt-no'],
                'credit_rating' => $request['credit-rating'],


            ]);


            if ($bond_investment) {
//                dd('inserted');
                return response()->json(['status' => true,

                    'investment' => $investor->bondInvestment,
                    'total_amount'=>$bond_overall_details['total_amount'],
                    'total_interest_paid'=>$bond_overall_details['total_interest_paid'],
                    'total_interest_accumalated' => $bond_overall_details['total_interest_accumalated']

                    ]);
            }


        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()]);
        }

    }


    public function getBondInvestment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',

        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found']);

        }

        try {

        //    dd($request['investor_id'], $request['investor_type']);

            $bond_overall_details = $this->calculateBondOverallDetails($request['investor_id'], ($request['investor_type'] == "individual" ? 'App\Person' : 'App\GroupMembers'));
            

            if ($request['investor_type'] == "group_member") {
                $investor = GroupMembers::where('id', $request['investor_id'])->first();
            } else {
                $investor = Person::where('id', $request['investor_id'])->first();
            }

            $investment = $investor->bondInvestment()->where('status',1)->get();

            foreach ($investment as $inv){

                unset($inv['interest_paid']);
                $inv['interest_accumalated'] = $this->getInterestAccumalated($inv);
                $inv['total_interest_paid'] = $inv->interestPaid()->sum('interest_paid');
            }


            $paid_out_interest = [];
            $count = 0;

            foreach ($investment as $inv){
//                dd($inv->interestPaid);

                foreach ($inv->interestPaid as $interest){
                    $paid_out_interest[$count]['id'] = $interest->id;
                    $paid_out_interest[$count]['bond_name'] = $inv->name;
                    $paid_out_interest[$count]['bond_id'] = $inv->id;
                    $paid_out_interest[$count]['investment_amount'] = $inv->amount_invested;
                    $paid_out_interest[$count]['date_of_purchase'] = date('d-m-Y', strtotime($inv->date_of_purchase));
                    $paid_out_interest[$count]['date_of_issue'] = date('d-m-Y', strtotime($inv->date_of_issue));
                    $paid_out_interest[$count]['interest_frequency'] = $inv->interest_frequency;
                    $paid_out_interest[$count]['interest_rate'] = $inv->interest_rate;
                    $paid_out_interest[$count]['interest_accumalated'] = 0;
                    $paid_out_interest[$count]['interest_paid'] = $interest->interest_paid;
                    $paid_out_interest[$count]['bond_receipt'] = $inv->bond_receipt;
                    $paid_out_interest[$count]['interest_payout_date'] = date('d-m-Y', strtotime($interest->interest_payout_date));

                    $count++;
                }


            }


            $investor_type = $request['investor_type'] == "individual" ? 'App\Person' : 'App\GroupMembers';
            $investor_id = $request['investor_id'];

            $bondRedemptions = BondDetails::where('bondinvestor_id', $investor_id)
            ->where('bondinvestor_type', $investor_type)
            ->where('status','=','0')
            ->get();

            $redemptionDetails = [];

            foreach($bondRedemptions as $redemption){
                // dd($redemption->bondRedemptions);

                $redemptionDetails[] = [
                    'name' => $redemption->name,
                    'amount_invested' => $redemption->amount_invested,
                    'date_of_issue' => date('d/m/Y',strtotime($redemption->date_of_issue)),
                    'realised_profit' => (float)$redemption->bondRedemptions->realised_profit,
                    'redemption_date' => date('d/m/Y', strtotime($redemption->bondRedemptions->date)),
                    'bond_receipt' => $redemption->bond_receipt
                ];
            }

            



            return response()->json(['msg' => true, 'investments' => $investment,
                'interest' => $paid_out_interest,
                'total_amount'=>$bond_overall_details['total_amount'],
                'total_interest_paid'=>$bond_overall_details['total_interest_paid'],
                'total_interest_accumalated' => $bond_overall_details['total_interest_accumalated'],
                'redemption_details' => $redemptionDetails
                ]);

        } catch (\Exception $e) {
            return response()->json(['msg' => false, 'response' => $e->getMessage()]);

        }

    }


    public function addBondInterest(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'bond-id' => 'required',
            'interest-payout-date' => 'required',
            'interest-paid' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found', 'error' => $validator->errors()]);

        }

        try {

            $bond_investment = BondDetails::where('id', $request['bond-id'])->first();
            $investor_id = $bond_investment->bondinvestor_id;
            $investor_type = $bond_investment->bondinvestor_type;


//            dd($bond_overall_details);

            $save_interest = $bond_investment->interestPaid()->create([

                'interest_payout_date' => date('Y-m-d', strtotime($request['interest-payout-date'])),
                'interest_paid' => $request['interest-paid'],

            ]);

            $bond_overall_details = $this->calculateBondOverallDetails($investor_id, $investor_type);




            $bond_interest = $bond_investment->interestPaid;

            $paid_out_interest = [];

            $count = 0;
            foreach ($bond_interest as $interest) {
//                dd($interest->bondDetails->name, $interest->bond_id);

                $paid_out_interest[$count]['bond_name'] = $interest->bondDetails->name;
                $paid_out_interest[$count]['bond_id'] = $interest->bondDetails->id;
                $paid_out_interest[$count]['investment_amount'] = $interest->bondDetails->amount_invested;
                $paid_out_interest[$count]['date_of_purchase'] = $interest->bondDetails->date_of_purchase;
                $paid_out_interest[$count]['date_of_issue'] = $interest->bondDetails->date_of_issue;
                $paid_out_interest[$count]['interest_frequency'] = $interest->bondDetails->interest_frequency;
                $paid_out_interest[$count]['interest_rate'] = $interest->bondDetails->interest_rate;
                $paid_out_interest[$count]['interest_accumalated'] = 0;
                $paid_out_interest[$count]['interest_paid'] = $interest->interest_paid;
                $paid_out_interest[$count]['bond_receipt'] = $interest->bondDetails->bond_receipt;
                $paid_out_interest[$count]['interest_payout_date'] = $interest->interest_payout_date;

                $count++;
            }

            if ($bond_investment) {

                return response()->json(['status' => true, 'interest' => $paid_out_interest,
                    'total_amount'=>$bond_overall_details['total_amount'],
                    'total_interest_paid'=>$bond_overall_details['total_interest_paid'],
                    'total_interest_accumalated' => $bond_overall_details['total_interest_accumalated']
                ]);

            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()]);
        }


    }


    private function calculateBondOverallDetails($investor_id, $investor_type){

        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);


        $bond_inv_details = BondDetails::where('bondinvestor_id', $investor_id)
        ->where('bondinvestor_type', $investor_type)
        ->where('status','=',1)
        ->get();

        // dd($bond_inv_details);


        $interest_accumalated = 0;
        foreach ($bond_inv_details as $bond){
            // dd($this->getInterestAccumalated($bond), $bond);
            $interest_till_date = $this->getInterestAccumalated($bond);
            $interest_accumalated += $interest_till_date;

        }

       





        $bond_investments = BondDetails::where('bondinvestor_id', $investor_id)->where('bondinvestor_type', $investor_type)->where('status','=','1')->pluck('id');

        $total_amount = BondDetails::whereIn('id',$bond_investments)->sum('amount_invested');


        $realisedProfit = BondDetails::where('bondinvestor_id', $investor_id)->where('bondinvestor_type', $investor_type)->where('status','=','0')->pluck('id');
        
        $realisedProfit = BondRedemption::whereIn('bond_id',$realisedProfit)->sum('realised_profit');

        $total_interest_paid = BondInterest::whereIn('bond_id',$bond_investments)->sum('interest_paid');

        $total_interest_paid += $realisedProfit;



//        dd($bond_investments, $total_interest_paid);
        return ['total_amount'=>$fmt->format($total_amount), 'total_interest_paid' => $fmt->format($total_interest_paid), 'total_interest_accumalated' => $fmt->format(round($interest_accumalated,2))];
    }


    public function deleteBondInterest(Request $request){
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => false, 'response' => 'Sufficient data is not found', 'error' => $validator->errors()]);

        }

        $id = $request['id'];

        $delete = BondInterest::find($id)->delete();

        if($delete){
            return response()->json(['msg' => true, 'response' => 'Bond Interest Successfully deleted']);
        }
    }

    public function deleteBondInvestment(Request $request){
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => false, 'response' => 'Sufficient data is not found', 'error' => $validator->errors()]);

        }

        $id = $request['id'];

        $delete_bond = BondDetails::find($id)->delete();

        if($delete_bond){
            BondInterest::where('bond_id', $id)->delete();
            return response()->json(['msg' => true, 'response' => 'Bond Interest Successfully deleted']);

        }else{
            return response()->json(['msg' => false, 'response' => 'Bond Interest Successfully deleted']);
        }
    }

    public function downloadBondInvestment(Request $request){

        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());



        }

        if ($request['investor_type'] == "group_member") {
            $investor = GroupMembers::where('id', $request['investor_id'])->first();
        } else {
            $investor = Person::where('id', $request['investor_id'])->first();
        }

        $investment = $investor->bondInvestment;


        foreach ($investment as $inv){

            $export[] = array(

                'Bond Name' => $inv->name,
                'Bond Ledger Number' => $inv->bond_receipt,
                'Payout Frequency' => $inv->interest_frequency,
                'Date of Issue' => $inv->date_of_issue,
                'Interest Rate' => $inv->interest_rate,
                'Interest Payout Date' => $inv->date_of_interest,
                'Interest Paid' => $inv->interestPaid()->sum('interest_paid')
            );
        }


        $file_content = \Excel::create('Bond Investment Summary', function ($excel) use ($export) {
            $excel->sheet('Bond Investment Summary', function ($sheet) use ($export) {
                $sheet->fromArray($export);
            });
        })->store('xlsx', $_SERVER['DOCUMENT_ROOT']);


        $file = public_path() . "/Bond Investment Summary.xlsx";


        return response()->download($file, 'Bond Investment Summary.xlsx');

    }


    private function getInterestAccumalated($bond){

        try{
            $bondInterests = BondInterest::where('bond_id',$bond->id)->sum('interest_paid');
            if($bondInterests == null){
                $bondInterests = 0;
            }

            // if($bond->initial_value == ''){
            //     $bond->initial_value = $bond->amount_invested;
            // }

            $noOfDaysSinceInvestment = date_diff(date_create($bond->date_of_issue),date_create(date('Y-m-d')))->format('%a');
            // dd($noOfDaysSinceInvestment);
            // dd($bond->initial_value);
            // dd(($noOfDaysSinceInvestment/($bond->bond_term * 365)));

            // dd((pow(((100 + $bond->interest_rate)/100), ($noOfDaysSinceInvestment/($bond->bond_term * 365)))));
            // dd((100 + $bond->interest_rate)/100, $noOfDaysSinceInvestment/($bond->bond_term * 365));
            $accruedInterestsTillDate = $bond->amount_invested * (pow( ((100 + $bond->interest_rate)/100), ($noOfDaysSinceInvestment/365) ));
            
            // dd($accruedInterestsTillDate, $bondInterests, $bond, $noOfDaysSinceInvestment);

            $accruedInterestsTillDate = round($accruedInterestsTillDate - $bond->amount_invested, 2);

            $pendingInterests = $accruedInterestsTillDate - $bondInterests;
        }catch(\Exception $e){

            dd($e, $bond);

        }

        // dd($pendingInterests);

        return round($pendingInterests, 2);


            //new method ends here.
//        b
    }

    public function showPmsPage(){
        if (\Auth::user()) {
            $schemes = PmsSchemes::all();
            $groups = Group::where('user_id', \Auth::user()->id)->get();
            $persons = Person::where('user_id', \Auth::user()->id)->get();
            $group_members = GroupMembers::where('user_id', \Auth::user()->id)->get();
            $twoday = Carbon::now()->addDays(2);
            $today = Carbon::now();

            $queries = Queries::
            where("date", "<", $twoday)
                ->where("date", ">", $today)
                ->get();


            return view('pms-home')->with(compact('groups', 'persons', 'group_members', 'schemes', 'queries'));

        } else {
            return view('login');
        }
    }



    public function addPmsInvestment(Request $request)
    {

//        dd('hello');

        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',
            'scheme_name' => 'required',
            'doi' => 'required',
            'capital_invested' => 'required',
            'coi' => 'required',
            'amount_invested' => 'required',
            'current_value' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found', 'error' => $validator->errors()]);

        }


        try {
            if ($request['investor_type'] == 'group_member') {
                $investor = GroupMembers::where('id', $request['investor_id'])->first();
            } else {
                $investor = Person::where('id', $request['investor_id'])->first();
            }


//            dd(PmsSchemes::where('name',$request['scheme_name'])->first()->pmsCorporation);


            $bond_investment = $investor->pmsInvestment()->create([

                'name' => $request['scheme_name'],
                'corporation' => PmsSchemes::where('name',$request['scheme_name'])->first()->pmsCorporation->name,
                'investment_date' => date('Y-m-d', strtotime($request['doi'])),
                'capital_invested' => $request['capital_invested'],
                'coi' => $request['coi'],
                'amount_invested' => $request['amount_invested'],
                'current_value' => $request['current_value'],


            ]);

            $pms_overall_details = $this->calculatePmsOverallDetails($investor);


            return response()->json(['status' => true, 'investments' => $pms_overall_details['investment'],
                'total_amount_invested' => $pms_overall_details['total_amount_invested'],
                'total_current_value' => $pms_overall_details['total_current_value'],
                'total_profit_or_loss' => $pms_overall_details['total_profit_or_loss'],

            ]);

//
//            if ($bond_investment) {
//                return response()->json(['status' => true,
//
//                    'investment' => $investor->pmsInvestment,
//
//                ]);
//            }


        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getLine()]);
        }

    }


    public function getPmsInvestment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',

        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'response' => 'Sufficient data is not found']);

        }

        try {


            if ($request['investor_type'] == "group_member") {
                $investor = GroupMembers::where('id', $request['investor_id'])->first();
            } else {
                $investor = Person::where('id', $request['investor_id'])->first();
            }




            $pms_overall_details = $this->calculatePmsOverallDetails($investor);


            return response()->json(['msg' => true, 'investments' => $pms_overall_details['investment'],
                'total_amount_invested' => $pms_overall_details['total_amount_invested'],
                'total_current_value' => $pms_overall_details['total_current_value'],
                'total_profit_or_loss' => $pms_overall_details['total_profit_or_loss'],

            ]);

        } catch (\Exception $e) {

        }

    }


    public function deletePmsInvestment(Request $request){
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['msg' => false, 'response' => 'Sufficient data is not found', 'error' => $validator->errors()]);

        }

        $id = $request['id'];


        $delete_pms = PmsDetails::find($id)->delete();

        if($delete_pms){
            return response()->json(['msg' => true, 'response' => 'PMS Entry Successfully deleted']);

        }else{
            return response()->json(['msg' => false, 'response' => 'PMS Entry Deletion Failed']);
        }
    }

    private function calculatePmsOverallDetails($investor){
        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

        $investment = $investor->pmsInvestment;

        $total_amount_invested = $investment->sum('amount_invested');
        $total_current_value = $investment->sum('current_value');
        $total_profit_or_loss =$total_current_value - $total_amount_invested;





        foreach ($investment as $inv){
            $inv['total_gain'] = $inv->current_value - $inv->amount_invested;
            $inv['abs_return'] = round(($inv['total_gain']/$inv->amount_invested) * 100, 2);

            $today = Carbon::now()->subDay(0)->toDateString();
            $today = date('Y-m-d', strtotime($today));

            $date_diff = date_diff(date_create($inv->investment_date), date_create($today));
            $date_diff = $date_diff->format("%a");

            if ($date_diff == 0) {
                $annualised_return = 0;
            } else {
                $annualised_return = round((($inv['abs_return'] * 365) / $date_diff), 2);
            }

            $inv['ann_return'] = $annualised_return;
        }


        return ['investment'=>$investment, 'total_amount_invested' => $total_amount_invested, 'total_current_value' => $total_current_value, 'total_profit_or_loss'=>$total_profit_or_loss];

    }


    public function downloadPmsInvestment(Request $request){

        $validator = \Validator::make($request->all(), [
            'investor_id' => 'required',
            'investor_type' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());



        }

        if ($request['investor_type'] == "group_member") {
            $investor = GroupMembers::where('id', $request['investor_id'])->first();
        } else {
            $investor = Person::where('id', $request['investor_id'])->first();
        }

        $investment = $investor->pmsInvestment;


        foreach ($investment as $inv){

            $inv['total_gain'] = $inv->current_value - $inv->amount_invested;
            $inv['abs_return'] = round(($inv['total_gain']/$inv->amount_invested) * 100, 2);

            $today = Carbon::now()->subDay(0)->toDateString();
            $today = date('Y-m-d', strtotime($today));

            $date_diff = date_diff(date_create($inv->investment_date), date_create($today));
            $date_diff = $date_diff->format("%a");

            if ($date_diff == 0) {
                $annualised_return = 0;
            } else {
                $annualised_return = round((($inv['abs_return'] * 365) / $date_diff), 2);
            }

            $inv['ann_return'] = $annualised_return;

//            dd($inv);

            $export[] = array(

                'Name' => $inv->name,
                'Corporation' => $inv->corporation,
                'Investment Date' => $inv->investment_date,
                'Capital Invested' => $inv->capital_invested,
                'Cost of Investment' => $inv->coi,
                'Amount Invested' => $inv->amount_invested,
                'Current Value' => $inv->current_value,
                'Total Profit/Loss' => $inv->total_gain,
                'Absolute Returns' => $inv->abs_return,
                'Annualised Returns' => $inv->ann_return,
            );
        }


        $file_content = \Excel::create('Pms Investment Summary', function ($excel) use ($export) {
            $excel->sheet('Pms Investment Summary', function ($sheet) use ($export) {
                $sheet->fromArray($export);
            });
        })->store('xlsx', $_SERVER['DOCUMENT_ROOT']);


        $file = public_path() . "/Pms Investment Summary.xlsx";


        return response()->download($file, 'Pms Investment Summary.xlsx');

    }


    public function editPmsInvestment(Request $request){
        $validator = \Validator::make($request->all(), [
            'pms-id' => 'required',
            'current-value' => 'required',
            'investor-id' => 'required',
            'investor-type' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());

        }

        $pms_details = PmsDetails::find($request['pms-id'])->update(['current_value' => $request['current-value']]);

        if ($request['investor_-ype'] == "group_member") {
            $investor = GroupMembers::where('id', $request['investor-id'])->first();
        } else {
            $investor = Person::where('id', $request['investor-id'])->first();
        }




        $pms_overall_details = $this->calculatePmsOverallDetails($investor);


        return response()->json(['msg' => true, 'investments' => $pms_overall_details['investment'],
            'total_amount_invested' => $pms_overall_details['total_amount_invested'],
            'total_current_value' => $pms_overall_details['total_current_value'],
            'total_profit_or_loss' => $pms_overall_details['total_profit_or_loss'],

        ]);

//        dd($pms_details->pmsInvestment());
    }


    public function accountStatementUpload(Request $request){
        $validator = \Validator::make($request->all(), [
            'inv-id' => 'required',
            'file' => 'required',
            'upload-type' => 'required',
            'u-type' => 'required',

        ]);

        if ($validator->fails()) {

            return $validator->errors();

        }


        if($request->hasFile('file')){

            $name = date('d-m-Y H:i:s').'-'.$request->file('file')->getClientOriginalName();

//            dd($name);
            $request->file('file')->move(
                storage_path().'/acc_statement', $name
            );

            if($request['upload-type'] == 'mf'){

                if($request['u-type'] == "inv"){
                    InvestmentDetails::find($request['inv-id'])->update(['acc_statement' => $name]);
                }else{

//                    dd($name);
                    WithdrawDetails::find($request['inv-id'])->update(['acc_statement' => $name]);

                }
            }elseif ($request['upload-type'] == 'bond'){
                BondDetails::find($request['inv-id'])->update(['acc_statement' => $name]);
            }else{
                PmsDetails::find($request['inv-id'])->update(['acc_statement' => $name]);
            }

            return response()->json(['msg' => true, 'response' => 'Account Statement Uploaded Succesfully']);
        }

        return response()->json(['msg' => false, 'response' => 'Account Statement Upload Failed']);

    }


    public function accountStatementDownload($inv_id,$d_type, $doc_type = null){


        if($d_type == 'mf'){

            if($doc_type == 'inv'){
                $acc_statement = InvestmentDetails::where('id', $inv_id)->pluck('acc_statement')[0];

            }else{
                $acc_statement = WithdrawDetails::where('id', $inv_id)->pluck('acc_statement')[0];

            }
        }else if($d_type == 'bond'){
            $acc_statement = BondDetails::where('id', $inv_id)->pluck('acc_statement')[0];
        }else{
            $acc_statement = PmsDetails::where('id', $inv_id)->pluck('acc_statement')[0];
        }


        $headers = array(
            'Content-Type: application/pdf',
        );

        if($acc_statement == null){
            return "Account Statement Not uploaded for this investment Yet";
        }


        try{
            return response()->download(storage_path().'/acc_statement/'.$acc_statement, $acc_statement, $headers);
        }catch(FileNotFoundException $e){
            return "File Not found";
        }


    }

    public function findUser(Request $request)
    {
            $user_name = $request['search-input'];
            $users = [];
            // dd(\Auth::id());
            $user_group = GroupMembers::where('member_name','LIKE','%'.$user_name.'%')
                ->orwhere('member_pan','LIKE',$user_name.'%')
                ->limit(10)
                ->get(['id','group_id','member_name','member_pan'])->toArray();
            // $users[] = Person
            $user_person = Person::where('name','LIKE','%'.$user_name.'%')
                ->orwhere('person_pan','LIKE',$user_name.'%')
                ->limit(10)
                ->get(['id','name','person_pan'])->toArray();

            $users = array_merge($user_group,$user_person);
            return json_encode($users);
    }

    public function addDividend(Request $request)
    {
        $investment = InvestmentDetails::where('id',$request['inv-id'])->get()->first()->toArray();
        $update_array = ['user_id'=>\Auth::id(),
                        'investment_id'=>$investment['id'],
                        'investor_id'=>$investment['investor_id'],
                        'investor_type'=>$investment['investor_type'],
                        'scheme_name'=>$investment['scheme_name'],
                        'investment_amount'=>$investment['amount_inv'],
                        'investment_date'=>$investment['purchase_date'],
                        'dividend_date'=>date('Y-m-d',strtotime($request['dividend-date'])),
                        'per_unit'=>$request['dividend-units'],
                        'dividend_amount'=>$request['dividend-amount']
                    ];
        $insert = Dividend::insert($update_array);
        if ($insert) {
            return response()->json(['msg'=>true,'response'=>'Dividend Added Succesfully','data'=>$update_array]);
        }else{
            return response()->json(['msg'=>false,'response'=>'Adding Dividend Failed']);
        }
    }

    public function deleteDividend(Request $request)
    {
        $delete = Dividend::where('id',$request['id'])->delete();
        if ($delete) {
            return response()->json(['msg'=>true,'response'=>'Dividend Deleted Succesfully']);
        }else{
            return response()->json(['msg'=>false,'response'=>'Deleting Dividend Failed']);
        }
    }

    public function addSip(Request $request){
        $validator = \Validator::make($request->all(), [
            'scheme_name' => 'required',
            'sip_amount' => 'required',
            'sip_investor_id' => 'required',
            'sip_investor_type' => 'required',
        ]);

        if ($validator->fails()) {

            return $validator->errors();

        }


        $create = Sip::create([
            'scheme_code' => $request['scheme_name'],
            'sip_amount' => $request['sip_amount'],
            'investor_id' => $request['sip_investor_id'],
            'investor_type' => $request['sip_investor_type']
        ]);

        if ($create) {
            return response()->json(['msg'=>true,'response'=>'Sip Added Successfully to the Investor.']);
        }else{
            return response()->json(['msg'=>false,'response'=>'Sip cannot be added. Please try again later.']);
        }


    }

    public function showSips($investor_type, $investor_id){
        $sips = Sip::where('investor_type',$investor_type)->where('investor_id',$investor_id)->get();
        if($investor_type == 'subperson'){
            $investorDetails = GroupMembers::where('id', $investor_id)->first();
        }else{
            $investorDetails = Person::where('id', $investor_id)->first();
        }

        foreach ($sips as $sip){
            $sip->scheme_name = SchemeDetails::where('scheme_code',$sip->scheme_code)->value('scheme_name');
        }

        return view('sip_list')->with(compact('investorDetails','sips'));
    }


    public function getBondInterestDetails(Request $request){
        $validator = \Validator::make($request->all(), [
           'bond_id' => 'required',
        ]);

        if ($validator->fails()) {

            return $validator->errors();

        }

        $bond = BondDetails::where('id',$request['bond_id'])->first();
        $bondInterests = BondInterest::where('bond_id',$request['bond_id'])->sum('interest_paid');
        if($bondInterests == null){
            $bondInterests = 0;
        }

        $noOfDaysSinceInvestment = date_diff(date_create($bond->date_of_issue),date_create(date('Y-m-d')))->format('%a');
//        dd($bond->initial_value);
        $accruedInterestsTillDate = $bond->initial_value * (pow(((100 + $bond->interest_rate)/100), ($noOfDaysSinceInvestment/($bond->bond_term * 365))));
        $accruedInterestsTillDate = round($accruedInterestsTillDate - $bond->initial_value, 2);

        $pendingInterests = $accruedInterestsTillDate - $bondInterests;

        return response()->json([
            'pendingInterest' => round($pendingInterests, 2),
            'paidInterest' => round($bondInterests, 2),
            'accruedInterest' => round($accruedInterestsTillDate, 2),
        ]);

    }

}
