<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\SchemeDetails;
use App\HistoricNavs;
use App\InvestmentDetails;
use App\WithdrawDetails;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function schemeNameUpdate()
    {
        $file = fopen('schemes.csv',"r");
        $column=fgetcsv($file);
        while(!feof($file)){
         $rowData[]=fgetcsv($file);
        }
        foreach ($rowData as $rec) {
            WithdrawDetails::where('scheme_code',$rec[1])->update(['scheme_name'=>$rec[2]]);
        }
        dd('SchemeDetails Updated!!!');
    }   

}
