<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BondRedemption extends Model
{
    public function bonds(){
        return $this->belongsTo('App\BondDetails','bond_id');
    }
}
