<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BondDetails extends Model
{

    protected $fillable = [
        'name',
        'bond_payout_type',
        'bond_type',
        'date_of_issue',
        'bond_term',
        'initial_value',
        'final_value',
        'real_interest_rate',
//        'due_date',
        'interest_rate',
//        'interest_frequency',
//        'date_of_interest',
//        'date_of_purchase',
        'amount_invested',
//        'moc',
        'amc',
        'bond_receipt',
        'credit_rating',
        'acc_statement'
    ];

    public function bondInvestment(){
        return $this->morphTo();
    }

    public function interestPaid(){
        return $this->hasMany('App\BondInterest', 'bond_id');
    }

    public function bondRedemptions(){
        return $this->hasOne('App\BondRedemption', 'bond_id');
    }
}
