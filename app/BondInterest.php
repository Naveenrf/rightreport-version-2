<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BondInterest extends Model
{

    protected $fillable = ['interest_payout_date', 'interest_paid'];

    public function bondDetails(){
        return $this->belongsTo('App\BondDetails', 'bond_id');
    }
}
