<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentDetails extends Model
{

    protected $fillable = ['acc_statement', 'last_fee_update'];

    public function withdrawDetails(){
        return $this->hasMany('App\WithdrawDetails');
    }
}
