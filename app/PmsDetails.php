<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmsDetails extends Model
{

    protected $fillable = [
        'name',
        'corporation',
        'investment_date',
        'coi',
        'amount_invested',
        'current_value',
        'capital_invested',
        'acc_statement'
    ];


    public function pmsInvestment(){
        return $this->morphTo();
    }
}
