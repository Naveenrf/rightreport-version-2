<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMembers extends Model
{
    protected $fillable = ['active_state'];
    public function bondInvestment(){
        return $this->morphMany('App\BondDetails', 'bondInvestor');
//        return $this->morphMany('App\BondDetails');

    }


    public function pmsInvestment(){

        return $this->morphMany('App\PmsDetails', 'pmsInvestor');

    }
}
