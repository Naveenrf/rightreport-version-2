<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class BackUpEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:backupEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command send backup notification email once the backup has been pushed to gDrive';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $to_name = 'Ops Team';
        $to_email = 'operationsmf@pwm-india.com';
        $data = array('name'=>'PWM team', 'body' => 'Dummy');
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {

            $message->to($to_email, $to_name)
            ->subject('Rightreport backup - '.date('d/m/Y'));
            $message->from('backuprr2020@gmail.com','Backup Notification');
});
    }
}
