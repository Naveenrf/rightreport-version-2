<?php

namespace App\Console\Commands;

use App\BondDetails;
use App\BondInterest;
use App\BondRedemption;
use App\Dividend;
use App\Group;
use App\GroupMembers;
use App\HistoricNavs;
use App\InvestmentDetails;
use App\Person;
use App\PmsDetails;
use App\WithdrawDetails;
use Carbon\Carbon;
use Illuminate\Console\Command;
use ZipArchive;
use PDF;
use \NumberFormatter;


class GenerateCis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:cis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   

            $allInvestors = [];

            $groups = Group::where('active_state',1)->get();

            foreach($groups as $group){

                $groupMembers = GroupMembers::where('group_id',$group->id)->get();


                $allInvestors['cis'][$group->name] = $group->id;

            }

            foreach(Person::where('active_state',1)->get() as $person){
                $allInvestors['individual'][$person->name] = $person->id;
            
            }

            
            
            foreach($allInvestors as $reportType => $investors){
                //ReportType -> CIS or Indivudual
                foreach($investors as $investorGroupName => $ginvestor){
                    

                    $investor_id = $ginvestor;
                    $investor_type = $reportType;

                    $download_option = 'with_redemption';
                    $download_format = 'old_format';

                    if ($investor_type == 'cis') {


                        $group_id = $ginvestor;
                        $group_mem_id = array();
                        $group_mem_id = GroupMembers::where('group_id', $group_id)->pluck('id')->toArray();

                        $count = 0;

                        $dividends_paid = Dividend::whereIn('investor_id', $group_mem_id)->where('investor_type', 'subperson')->orderBy('dividend_date','asc')->get();

                        $cis_dividends = $dividends_paid->groupBy('scheme_name');

                        $cis_dividends_pass = [];

                        foreach ($cis_dividends as $scheme_name => $dividends){
                            foreach ($dividends as $dividend){


                                if(array_key_exists($dividend->investment_id, $cis_dividends_pass)){
                                    $cis_dividends_pass[$dividend->investment_id]['scheme_name'] = $dividend->scheme_name;
                                    $cis_dividends_pass[$dividend->investment_id]['investment_amount'] += $dividend->investment_amount;
                                    $cis_dividends_pass[$dividend->investment_id]['dividend_paid'] += $dividend->dividend_amount;

                                }else{
                                    $cis_dividends_pass[$dividend->investment_id]['scheme_name'] = $dividend->scheme_name;
                                    $cis_dividends_pass[$dividend->investment_id]['investment_amount'] = $dividend->investment_amount;
                                    $cis_dividends_pass[$dividend->investment_id]['dividend_paid'] = $dividend->dividend_amount;
                                }
                            }
                        }


                        $group_pms_investment = PmsDetails::whereIn('pmsinvestor_id',$group_mem_id)->where('pmsinvestor_type', 'App\GroupMembers')->get()->groupBy('name');
                        $pms_pass_entry = [];

                        foreach($group_pms_investment as $pms_name => $pms_entry){

                            $pms_pass_entry[] = array(
                                'name' => $pms_name,
                                'investment_amount' => $pms_entry->sum('amount_invested'),
                                'realised_profit' => ($pms_entry->sum('current_value') - $pms_entry->sum('amount_invested')),
                                'current_value' => $pms_entry->sum('current_value'),
                            );


                        }


                        $group_bond_investment = BondDetails::whereIn('bondinvestor_id',$group_mem_id)
                        ->where('bondinvestor_type', 'App\GroupMembers')
                        ->where('status','=','1')
                        ->get()->groupBy('name');
                        $bond_pass_entry = [];


                        foreach($group_bond_investment as $bond_name => $bond_entry){
                            $interest_accumalated = 0;
                            $interest_paid = 0;


                            foreach($bond_entry as $bond_inv){
                                $interest_accumalated += $this->getInterestAccumalated($bond_inv);
                                $interest_paid += $bond_inv->interestPaid()->sum('interest_paid');
                            }

                            $bond_pass_entry[] = array(
                                'name' => $bond_name,
                                'investment_amount' => $bond_entry->sum('amount_invested'),
                                'realised_profit' => $interest_paid,
                                'current_value' => $interest_accumalated + $bond_entry->sum('amount_invested'),
                                'interest_accumalated' => $interest_accumalated,
                            );


                        }



                        $grand_total = 0;
                        $grand_market_value = 0;

                        $investor_id = array();
                        $separate_inv = array();


                        $inv_pass = array();

                        $change_date = date('Y-m-d');
                        $consolidatedInvestment = [];
                        foreach ($group_mem_id as $member_id) {
                            $investor_id[] = $member_id;
                            $consolidatedInvestment[$member_id]['SIP'] = 0;
                            $consolidatedInvestment[$member_id]['Mutual Funds'] = 0;
                            $consolidatedInvestment[$member_id]['Liquid Funds'] = 0;
                            $consolidatedInvestment[$member_id]['Fixed Income Product'] = 0;

                            $consolidatedGroupInvestment['SIP'] = 0;
                            $consolidatedGroupInvestment['Mutual Funds'] = 0;
                            $consolidatedGroupInvestment['Liquid Funds'] = 0;
                            $consolidatedGroupInvestment['Fixed Income Product'] = 0;
                        }


                        // Group id 6 is for Excel group since they wanted CIS report in Scheme Name order and rest of them in date wise order
                        if ($group_id == 6) {
                            $investment_detail = InvestmentDetails::where('status', '!=', 'inactive')->whereIn('investor_id', $investor_id)->where('investor_type', 'subperson')->where('purchase_date', '<=', $change_date)->orderBy('scheme_name', 'ASC')->get();
                        } else {
                            $investment_detail = InvestmentDetails::where('status', '!=', 'inactive')->whereIn('investor_id', $investor_id)->where('investor_type', 'subperson')->where('purchase_date', '<=', $change_date)->orderBy('purchase_date', 'asc')->get();

                        }


                        $today = Carbon::now()->subDays(0)->toDateString();


                        foreach ($investment_detail as $sep_inv) {

                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Scheme Name'] = $sep_inv->scheme_name;
                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Amount Invested'] = $sep_inv->amount_inv;
                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date'] = date('d-M-Y', strtotime($sep_inv->purchase_date));

                            $current_nav = HistoricNavs::where('scheme_code', $sep_inv->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();

                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase NAV'] = round($sep_inv->purchase_nav, 4);
                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Units'] = round($sep_inv->units, 3);

                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current NAV'] = round($current_nav['nav'], 4);



                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'] = round($current_nav['nav'] * $sep_inv->units, 2);

                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'] = round(($current_nav['nav'] * $sep_inv->units) - ($sep_inv->amount_inv), 2);

                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss']) / ($sep_inv->amount_inv)) * 100, 2);


                            $date_diff = date_diff(date_create($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date']), date_create($today));
                            $date_diff = $date_diff->format("%a");
                            if ($date_diff == 0) {
                                $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = 0;
                            } else {
                                $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns']) * (365 / $date_diff)), 2);
                            }

                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Folio Number'] = $sep_inv->folio_number;
                            $separate_inv[$sep_inv->investor_id][$sep_inv->id]['invest_type'] = $sep_inv->investment_type;

                            $grand_market_value += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

        //                    $grand_market_value = round($grand_market_value, 2);

                            if($sep_inv->investment_type == 2){
                                //SIP
                                $consolidatedInvestment[$sep_inv->investor_id]['SIP'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                                $consolidatedGroupInvestment['SIP'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                            }else if($sep_inv->investment_type == 3){
                                $consolidatedInvestment[$sep_inv->investor_id]['Liquid Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                                $consolidatedGroupInvestment['Liquid Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                            }else{
                                //Onetime
                                if($sep_inv->scheme_type == 'equity'){
                                    $consolidatedInvestment[$sep_inv->investor_id]['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                                    $consolidatedGroupInvestment['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                                }else if($sep_inv->scheme_type == 'balanced'){
                                    $consolidatedInvestment[$sep_inv->investor_id]['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                                    $consolidatedGroupInvestment['Mutual Funds'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                                }else{
                                    $consolidatedInvestment[$sep_inv->investor_id]['Fixed Income Product'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];
                                    $consolidatedGroupInvestment['Fixed Income Product'] += $separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'];

                                }
                            }

                        }

                        foreach ($consolidatedInvestment as $id => $investment){


                            $bond_entry = BondDetails::where('bondinvestor_id',$id)->where('bondinvestor_type', 'App\GroupMembers')->where('status','=','1')->get();
                            $interest_accumalated = 0;
                            $interest_paid = 0;


                            foreach($bond_entry as $bond_inv){

                                if($bond_inv->bondRedemptions != null){
                                    $realised_profit_or_loss += BondRedemption::with('bonds')->where('bond_id',$bond_inv->id)->value('realised_profit'); 
                                }

                                $interest_accumalated = $this->getInterestAccumalated($bond_inv);
                                $consolidatedInvestment[$id]['Fixed Income Product'] += $interest_accumalated + $bond_inv->amount_invested;
                                $consolidatedGroupInvestment['Fixed Income Product'] += $interest_accumalated + $bond_inv->amount_invested;

                            }
                        }

                        //Consolidated Data calculation should get over here.

                        if(count(array_diff_key(array_flip($investor_id), $separate_inv)) > 0){
                            foreach(array_flip(array_diff_key(array_flip($investor_id), $separate_inv)) as $difference_key){
                                $separate_inv[$difference_key] = array();
                            }
                        }



                        foreach (array_keys($separate_inv) as $id){
                            $separate_inv[$id]['one_time'] = InvestmentDetails::where('investor_id', $id)->where('investor_type', 'subperson')->where('status', '!=', 'inactive')->where('investment_type', 1)->count();

                            $separate_inv[$id]['sip'] = InvestmentDetails::where('investor_id', $id)->where('investor_type', 'subperson')->where('status', '!=', 'inactive')->where('investment_type', 2)->count();

                            $separate_inv[$id]['liquid'] = InvestmentDetails::where('investor_id', $id)->where('investor_type', 'subperson')->where('status', '!=', 'inactive')->where('investment_type', 3)->count();

                        }



                        foreach ($investment_detail as $investments) {
                            if (array_key_exists($investments->scheme_code, $inv_pass)) {


                                $inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

                                $inv_pass[$investments->scheme_code]['Amount Invested'] += $investments->amount_inv;

                                $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();

                                $inv_pass[$investments->scheme_code]['Units'] += round(($investments->units), 4);
                                $inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

                                $current_market_value = (($inv_pass[$investments->scheme_code]['Units']) * $current_nav['nav']);

                                $current_market_value = round($current_market_value, 2);
                                $inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

                                $inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

                                $inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested']) / ($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100, 2);


                            } else {

                                $inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

                                $inv_pass[$investments->scheme_code]['Amount Invested'] = $investments->amount_inv;

                                $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
                                $inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

                                $inv_pass[$investments->scheme_code]['Units'] = round(($investments->units), 4);

                                $current_market_value = (($investments->units) * $current_nav['nav']);

                                $current_market_value = round($current_market_value, 2);

                                $inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

                                $inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

                                $inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested']) / ($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100, 2);


                            }

                            $grand_total += $investments->amount_inv;

                            $current_nav = HistoricNavs::where('scheme_code', $investments->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
                            $current_nav = $current_nav['nav'];
                            $units_held = $investments->units;

                            $current_market_value = $units_held * $current_nav;



                            $count++;
                        }



                        $profit_or_loss_total = $grand_market_value - $grand_total;

                        $abs_total = (($grand_market_value - $grand_total) / $grand_total) * 100;
                        $abs_total = round($abs_total, 2);

                        $cis_wd_export = [];
                        $realised_profit_or_loss = 0;
                        foreach ($investor_id as $id) {
                            $withdraw_details = WithdrawDetails::where('investor_id', $id)->where('investor_type', 'subperson')->orderBy('withdraw_date','asc')->get();
                            $wd_export = [];
                            foreach ($withdraw_details as $withdraw) {
                                $realised_profit_or_loss += ($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount;

                                $cis_wd_export[] = array(
                                    'Investor Name' => GroupMembers::where('id', $withdraw->investor_id)->value('member_name'),
                                    'Scheme Name' => $withdraw->scheme_name,
                                    'Amount Invested' => $withdraw->invested_amount,
                                    'Purchase Date' => $withdraw->purchase_date,
                                    'Purchase NAV' => $withdraw->purchase_nav,
                                    'Units' => $withdraw->units,
                                    'Redemption NAV' => bcdiv(($withdraw->withdraw_amount - $withdraw->stt) / $withdraw->units, 1, 4),
                                    'Withdraw Value' => $withdraw->withdraw_amount,
                                    'Realised Profit/Loss' => (($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount),
                                    'Redemption Date' => $withdraw->withdraw_date,
                                    //                                       'Absolute Returns' => round((($withdraw->withdraw_amount - $withdraw->invested_amount)/$withdraw->invested_amount)*100, 2),
                                    'Exit Load' => $withdraw->exit_load,
                                    'STT' => $withdraw->stt,
                                    'Folio Number' => $withdraw->folio_number,

                                );
                            }
                        }



                        $mutual_fund_inv_total = $grand_total;
                        $mutual_fund_current_value = $grand_market_value;
                        $mutual_fund_unreal_p_or_loss = $profit_or_loss_total;






                        foreach($bond_pass_entry as $bond){
                            $realised_profit_or_loss += $bond['realised_profit'];
                            $grand_market_value += ($bond['investment_amount'] + $bond['interest_accumalated']);
                            $grand_total += $bond['investment_amount'];

                            //Adding Bond Interest Accumalated to Unrealised Profit or Loss
                            $profit_or_loss_total += $bond['interest_accumalated'];
                        

                        }

                        


                        foreach($pms_pass_entry as $pms){
                            $grand_market_value += $pms['current_value'];
                            $grand_total += $pms['investment_amount'];

                            $profit_or_loss_total += ($pms['current_value'] - $pms['investment_amount']);
                        }


                        $realised_profit_or_loss += $dividends_paid->sum('dividend_amount');

                        $pdf = PDF::loadView('mastercispdf', [
                            'inv' => $inv_pass,
                            'dividends'=> $cis_dividends_pass,
                            'mf_inv_total'=>$mutual_fund_inv_total,
                            'mf_current_value'=> $mutual_fund_current_value,
                            'mf_p_or_loss'=>$mutual_fund_unreal_p_or_loss,
                            'bonds' => $bond_pass_entry,
                            'pms' => $pms_pass_entry,
                            'date' => $change_date,
                            'total' => $grand_total,
                            'current' => $grand_market_value,
                            'unreal_pl' => $profit_or_loss_total,
                            'real_pl' => $realised_profit_or_loss,
                            'group_id' => $group_id,
                            'consolidated_investment' => $consolidatedInvestment,
                            'consolidated_group_inv' => $consolidatedGroupInvestment,
                            'download_format' => 'old_format',
                            'download_option' => 'with_redemption',
                            'abs' => $abs_total])->setPaper('a4', 'landscape')->setWarnings(false)->save(public_path().'/backup/'.'CIS Report.pdf');


                        $names = GroupMembers::whereIn('id', array_keys($separate_inv))->pluck('member_name');

                        $file_content = \Excel::create('CIS Report', function ($excel) use ($inv_pass, $cis_wd_export, $separate_inv, $change_date, $names, $download_option) {
                            $excel->sheet('CIS Report', function ($sheet) use ($inv_pass, $cis_wd_export) {
                                $sheet->fromArray($inv_pass);
                                $sheet->setWidth('A', 50);
                                $sheet->setWidth('B', 15);
                                $sheet->setWidth('C', 20);
                                $sheet->setWidth('D', 18);
                                $sheet->setWidth('E', 20);
                                $sheet->setWidth('F', 20);
                                $sheet->setWidth('G', 20);


                                $sheet->fromArray($cis_wd_export);
                                $sheet->setWidth('A', 20);
                                $sheet->setWidth('B', 50);
                                $sheet->setWidth('C', 20);
                                $sheet->setWidth('D', 15);
                                $sheet->setWidth('E', 15);
                                $sheet->setWidth('F', 15);
                                $sheet->setWidth('G', 15);
                                $sheet->setWidth('H', 25);
                                $sheet->setWidth('I', 25);
                                $sheet->setWidth('J', 18);
                                $sheet->setWidth('K', 25);
                                $sheet->setWidth('L', 20);


                            });

                            $bondRedemptions = [];


                            foreach ($separate_inv as $investor_id => $inv) {


                                //Dividends Calculation here

                                $dividends = Dividend::where('investor_id', $investor_id)->where('investor_type', 'subperson')->orderBy('dividend_date','asc')->get()->toArray();

                                //bond calculation starts here


                                $bond_overall_details = $this->calculateBondOverallDetails($investor_id, 'App\GroupMembers');


                                $bond_investor = GroupMembers::where('id', $investor_id)->first();


                                $bond_investment = $bond_investor->bondInvestment;

                                foreach ($bond_investment as $bond_inv){

                                    if($bond_inv->bondRedemptions != null){
                                        $bondRedemptions[] = BondRedemption::with('bonds')->where('bond_id',$bond_inv->id)->first(); 
                                    }

                                    unset($bond_inv['interest_paid']);
                                    $bond_inv['interest_accumalated'] = $this->getInterestAccumalated($bond_inv);
                                    $bond_inv['total_interest_paid'] = $bond_inv->interestPaid()->sum('interest_paid');
                                }


                                $bond_paid_out_interest = [];
                                $count = 0;

                                foreach ($bond_investment as $bond_inv){
        //                dd($inv->interestPaid);

                                    foreach ($bond_inv->interestPaid as $interest){
                                        $bond_paid_out_interest[$count]['id'] = $interest->id;
                                        $bond_paid_out_interest[$count]['bond_name'] = $bond_inv->name;
                                        $bond_paid_out_interest[$count]['bond_id'] = $bond_inv->id;
                                        $bond_paid_out_interest[$count]['investment_amount'] = $bond_inv->amount_invested;
                                        $bond_paid_out_interest[$count]['date_of_purchase'] = date('d-m-Y', strtotime($bond_inv->date_of_purchase));
                                        $bond_paid_out_interest[$count]['date_of_issue'] = date('d-m-Y', strtotime($bond_inv->date_of_issue));
                                        $bond_paid_out_interest[$count]['interest_frequency'] = $bond_inv->interest_frequency;
                                        $bond_paid_out_interest[$count]['interest_rate'] = $bond_inv->interest_rate;
                                        $bond_paid_out_interest[$count]['interest_accumalated'] = 0;
                                        $bond_paid_out_interest[$count]['interest_paid'] = $interest->interest_paid;
                                        $bond_paid_out_interest[$count]['bond_receipt'] = $bond_inv->bond_receipt;
                                        $bond_paid_out_interest[$count]['interest_payout_date'] = date('d-m-Y', strtotime($interest->interest_payout_date));

                                        $count++;
                                    }


                                }



        //                Bond calculation ends here


                                //pms starts here


                                $pms_investor = GroupMembers::where('id', $investor_id)->first();




                                $pms_overall_details = $this->calculatePmsOverallDetails($pms_investor);

                                $pms_investment = $pms_overall_details['investment'];


                                //pms ends here



                                $member_name = $pms_investor->member_name;
                                $member_pan =  $pms_investor->member_pan;
                                $member_email = $pms_investor->email;
                                $member_contact = $pms_investor->contact;
                                $member_add = $pms_investor->address;



                                $withdraw_details = WithdrawDetails::where('investor_id', $investor_id)->where('investor_type', 'subperson')->orderBy('withdraw_date','asc')->get();
                                $inv_name = GroupMembers::where('id', $investor_id)->get()->toArray();
                                $wd_export = [];
                                foreach ($withdraw_details as $withdraw) {
                                    $wd_export[] = array(
                                        'Investor Name' => $inv_name[0]['member_name'],
                                        'Scheme Name' => $withdraw->scheme_name,
                                        'Amount Invested' => $withdraw->invested_amount,
                                        'Purchase Date' => $withdraw->purchase_date,
                                        'Purchase NAV' => $withdraw->purchase_nav,
                                        'Units' => $withdraw->units,
                                        'Redemption NAV' => bcdiv(($withdraw->withdraw_amount - $withdraw->stt) / $withdraw->units, 1, 4),
                                        'Withdraw Value' => $withdraw->withdraw_amount,
                                        'Realised Profit/Loss' => (($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount),
                                        'Redemption Date' => $withdraw->withdraw_date,
                                        //                                       'Absolute Returns' => round((($withdraw->withdraw_amount - $withdraw->invested_amount)/$withdraw->invested_amount)*100, 2),
                                        'Exit Load' => $withdraw->exit_load,
                                        'STT' => $withdraw->stt,
                                        'Folio Number' => $withdraw->folio_number,

                                    );
                                }


                                $pdf = PDF::loadView('cispdf', [
                                    'inv' => $inv,
                                    'withdraw' => $wd_export,
                                    'pms_inv'=>$pms_investment,
                                    'dividends'=> $dividends,
                                    'bond_inv' => $bond_investment,
                                    'bond_interest' => $bond_paid_out_interest ,
                                    'bond_redemptions' => $bondRedemptions,
                                    'name' => $member_name,
                                    'date' => $change_date,
                                    'pan' => $member_pan,
                                    'email' => $member_email,
                                    'contact' => $member_contact,
                                    'download_option' => $download_option,
                                    'address' => $member_add])->setPaper('a4', 'landscape')->setWarnings(false)->save(public_path().'/backup/'.$member_name . '.pdf');

                                unset($inv['one_time']);
                                unset($inv['sip']);
                                unset($inv['liquid']);

                                $excel->sheet($member_name, function ($sheet) use ($inv, $wd_export) {
                                    $sheet->fromArray($inv);
                                    $sheet->setWidth('A', 20);
                                    $sheet->setWidth('B', 50);
                                    $sheet->setWidth('C', 20);
                                    $sheet->setWidth('D', 15);
                                    $sheet->setWidth('E', 15);
                                    $sheet->setWidth('F', 15);
                                    $sheet->setWidth('G', 15);
                                    $sheet->setWidth('H', 25);
                                    $sheet->setWidth('I', 25);
                                    $sheet->setWidth('J', 18);
                                    $sheet->setWidth('K', 25);
                                    $sheet->setWidth('L', 20);


                                    $sheet->fromArray($wd_export);
                                    $sheet->setWidth('A', 20);
                                    $sheet->setWidth('B', 50);
                                    $sheet->setWidth('C', 20);
                                    $sheet->setWidth('D', 15);
                                    $sheet->setWidth('E', 15);
                                    $sheet->setWidth('F', 15);
                                    $sheet->setWidth('G', 15);
                                    $sheet->setWidth('H', 25);
                                    $sheet->setWidth('I', 25);
                                    $sheet->setWidth('J', 18);
                                    $sheet->setWidth('K', 25);
                                    $sheet->setWidth('L', 20);

                                });


                            }
                        })->store('xlsx', public_path().'/backup/');




                        $zip = new ZipArchive;
                        if ($zip->open(public_path().'/backup/'.$investorGroupName.' - cis.zip', ZipArchive::CREATE) === TRUE) {
                            foreach ($names as $value) {
                                $zip->addFile(public_path() . '/backup/' . $value . '.pdf');
                            }
                            $zip->addFile(public_path().'/backup/'.'CIS Report.xlsx');
                            $zip->addFile('CIS Report.pdf');
                            $zip->close();
                            foreach ($names as $value) {
                                $path = public_path() . '/backup/' . $value . '.pdf';

                                if(file_exists($path)){
                                    unlink($path);
                                }
                            }
                            $xls = public_path() . '/backup/' . '/CIS Report.xlsx';
                            // unlink($xls);


                            if(file_exists($xls)){
                                unlink($xls);
                            }

                            $pdf = public_path() . '/backup/' . '/CIS Report.pdf';
                            

                            if(file_exists($pdf)){
                                unlink($pdf);
                            }
                        }


                    }else if($reportType == 'individual'){

                        // dd($reportType);

                        $investor_id = $ginvestor;
                        $investor_type = $reportType;


                        $change_date = date('Y-m-d');
                        $investment_details = InvestmentDetails::where('status', '!=', 'inactive')
                            ->where('investor_type', $investor_type)
                            ->where('investor_id', $investor_id)
                            ->where('purchase_date', '<=', $change_date)->orderBy('purchase_date', 'asc')->get();


                        $investor_detail = [];
                        $name = '';
                        $email = '';
                        $address = '';
                        $contact = '';
                        $pan = '';
                        if ($investor_type == 'individual') {

                            $pms_details = PmsDetails::where('pmsinvestor_type','App\Person')->where('pmsinvestor_id', $investor_id)->get();
                            $bond_details = BondDetails::where('bondinvestor_type','App\Person')->where('bondinvestor_id', $investor_id)->where('status','=','1')->get();
                            $dividends = Dividend::where('investor_type', 'individual')->where('investor_id', $investor_id)->get();

                            $investor_detail = Person::where('id', $investor_id)->get();
                            $name = $investor_detail[0]['name'];
                            $email = $investor_detail[0]['email'];
                            $address = $investor_detail[0]['address'];
                            $contact = $investor_detail[0]['contact'];
                            $pan = $investor_detail[0]['person_pan'];
                        } elseif ($investor_type == 'subperson') {

                            $pms_details = PmsDetails::where('pmsinvestor_type','App\GroupMembers')->where('pmsinvestor_id', $investor_id)->get();
                            $bond_details = BondDetails::where('bondinvestor_type','App\GroupMembers')->where('bondinvestor_id', $investor_id)->where('status','=','1')->get();
                            $dividends = Dividend::where('investor_type', 'subperson')->where('investor_id', $investor_id)->get();


                            $investor_detail = GroupMembers::where('id', $investor_id)->get();
                            $name = $investor_detail[0]['member_name'];
                            $email = $investor_detail[0]['email'];
                            $address = $investor_detail[0]['address'];
                            $contact = $investor_detail[0]['contact'];
                            $pan = $investor_detail[0]['member_pan'];
                        }



        //                bond calculation starts here


                        $bond_overall_details = $this->calculateBondOverallDetails($investor_id, ($investor_type == "individual" ? 'App\Person' : 'App\GroupMembers'));


                        if ($investor_type == "subperson") {
                            $bond_investor = GroupMembers::where('id', $investor_id)->first();
                        } else {
                            $bond_investor = Person::where('id', $investor_id)->first();
                        }

                        $bond_investment = $bond_investor->bondInvestment;
                        $bondRedemptions = [];

                        // dd('sadf');
                        foreach ($bond_investment as $inv){

                            if($inv->bondRedemptions != null){
                                $bondRedemptions[] = BondRedemption::with('bonds')->where('bond_id',$inv->id)->first(); 
                            }

                            unset($inv['interest_paid']);
                            $inv['interest_accumalated'] = $this->getInterestAccumalated($inv);
                            $inv['total_interest_paid'] = $inv->interestPaid()->sum('interest_paid');
                        }


                        $bond_paid_out_interest = [];
                        $count = 0;

                        foreach ($bond_investment as $inv){
        //                dd($inv->interestPaid);

                            foreach ($inv->interestPaid as $interest){
                                $bond_paid_out_interest[$count]['id'] = $interest->id;
                                $bond_paid_out_interest[$count]['bond_name'] = $inv->name;
                                $bond_paid_out_interest[$count]['bond_id'] = $inv->id;
                                $bond_paid_out_interest[$count]['investment_amount'] = $inv->amount_invested;
                                $bond_paid_out_interest[$count]['date_of_purchase'] = date('d-m-Y', strtotime($inv->date_of_purchase));
                                $bond_paid_out_interest[$count]['date_of_issue'] = date('d-m-Y', strtotime($inv->date_of_issue));
                                $bond_paid_out_interest[$count]['interest_frequency'] = $inv->interest_frequency;
                                $bond_paid_out_interest[$count]['interest_rate'] = $inv->interest_rate;
                                $bond_paid_out_interest[$count]['interest_accumalated'] = 0;
                                $bond_paid_out_interest[$count]['interest_paid'] = $interest->interest_paid;
                                $bond_paid_out_interest[$count]['bond_receipt'] = $inv->bond_receipt;
                                $bond_paid_out_interest[$count]['interest_payout_date'] = date('d-m-Y', strtotime($interest->interest_payout_date));

                                $count++;
                            }


                        }


        //                Bond calculation ends here



                        //pms starts here


                        if ($investor_type == "subperson") {
                            $pms_investor = GroupMembers::where('id', $investor_id)->first();
                        } else {
                            $pms_investor = Person::where('id', $investor_id)->first();
                        }




                        $pms_overall_details = $this->calculatePmsOverallDetails($pms_investor);

                        $pms_investment = $pms_overall_details['investment'];


                        //pms ends here




                        $export = array();


                        $export['one_time'] = InvestmentDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->where('investment_type', 1)->where('status', '!=', 'inactive')->count();
                        $export['sip'] = InvestmentDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->where('investment_type', 2)->where('status', '!=', 'inactive')->count();
                        $export['liquid'] = InvestmentDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->where('investment_type', 3)->where('status', '!=', 'inactive')->count();


                        //$investment_details = $investment_details->toArray();

        //                dd($investment_details);

                        $yesterday_date = Carbon::now()->subDay(1)->toDateString();
                        $yesterday_date = date('Y-m-d', strtotime($yesterday_date));
                        foreach ($investment_details as $investment_detail) {

                            $current_nav = HistoricNavs::where('scheme_code', $investment_detail->scheme_code)->where('date', '<=', $change_date)->orderBy('date', 'aesc')->first();
        //                        dd($current_nav, $change_date);

                            $current_market_value = ($investment_detail->units) * $current_nav['nav'];
                            $p_or_loss = (($investment_detail->units) * $current_nav['nav']) - (($investment_detail->units) * ($investment_detail->purchase_nav));

                            $today = Carbon::now()->subDay(0)->toDateString();
                            $today = date('Y-m-d', strtotime($today));
                            $purchase_date = $investment_detail->purchase_date;

                            $date_diff = date_diff(date_create($purchase_date), date_create($today));
                            $date_diff = $date_diff->format("%a");

                            $abs_returns = (($current_market_value - $investment_detail->amount_inv) / $investment_detail->amount_inv) * 100;
                            if ($date_diff == 0) {
                                $ann_returns = 0;
                            } else {
                                $ann_returns = (($abs_returns * 365) / $date_diff);
                            }

                            $date = date_create($investment_detail->purchase_date);
                            $purchase_date = date_format($date, "d-M-y");

                            $export[] = array(

                                'Scheme Name' => $investment_detail->scheme_name,
                                //'Scheme Type' => $investment_detail->scheme_type,
                                'Folio Number' => $investment_detail->folio_number,
                                'Purchase date' => $purchase_date,
                                'Amount Invested' => $investment_detail->amount_inv,
                                'Purchase NAV' => $investment_detail->purchase_nav,
                                'Units' => round($investment_detail->units, 4),
                                'Current NAV' => $current_nav['nav'],
                                'Current Market Value' => round($current_market_value, 2),
                                'Unrealised Profit/Loss' => round($p_or_loss, 2),
                                'Absolute Returns' => round($abs_returns, 2),
                                'Annualised Returns' => round($ann_returns, 2),
                                'invest_type' => $investment_detail->investment_type
                            );
                        }

                        $withdraw_details = WithdrawDetails::where('investor_id', $investor_id)->where('investor_type', $investor_type)->orderBy('withdraw_date','asc')->get();
                        $wd_export = [];
                        foreach ($withdraw_details as $withdraw) {

                            if($withdraw->units == 0){
                                dd($withdraw);
                            }
                            $wd_export[] = array(

                                'Scheme Name' => $withdraw->scheme_name,
                                'Folio Number' => $withdraw->folio_number,
                                'Purchase Date' => $withdraw->purchase_date,
                                'Amount Invested' => $withdraw->invested_amount,
                                'Purchase NAV' => $withdraw->purchase_nav,
                                'Units' => $withdraw->units,
                                'Redemption NAV' => bcdiv(($withdraw->withdraw_amount - $withdraw->stt) / $withdraw->units, 1, 4),
                                'Redemption Date' => $withdraw->withdraw_date,
                                'Withdraw Value' => $withdraw->withdraw_amount,
                                'Realised Profit/Loss' => (($withdraw->withdraw_amount - (2 * $withdraw->stt)) - $withdraw->invested_amount),
        //                                       'Absolute Returns' => round((($withdraw->withdraw_amount - $withdraw->invested_amount)/$withdraw->invested_amount)*100, 2),
                                'Exit Load' => $withdraw->exit_load,
                                'STT' => $withdraw->stt,


                            );
                        }



                    $pdf = PDF::loadView('cispdf', ['inv' => $export,
                            'dividends'=>$dividends,
                            'pms_inv'=>$pms_investment,
                            'bond_inv'=>$bond_investment,
                            'bond_interest' => $bond_paid_out_interest,
                            'bond_redemptions' => $bondRedemptions,
                            'withdraw' => $wd_export,
                            'date' => $change_date,
                            'name' => $name,
                            'address' => $address,
                            'email' => $email,
                            'contact' => $contact,
                            'download_option' => $download_option,
                            'pan' => $pan])->setPaper('a4', 'landscape')->setWarnings(false)->save(public_path().'/backup/Investment Summary.pdf');
                        //print_r($export);
                        unset($export['one_time']);
                        unset($export['sip']);
                        unset($export['liquid']);
                        $file_content = \Excel::create('Investment Summary', function ($excel) use ($export, $wd_export) {


                            $excel->sheet('Investment Summary', function ($sheet) use ($export, $wd_export) {
                                $sheet->fromArray($export);
                                $sheet->setWidth('A', 20);
                                $sheet->setWidth('B', 50);
                                $sheet->setWidth('C', 20);
                                $sheet->setWidth('D', 15);
                                $sheet->setWidth('E', 15);
                                $sheet->setWidth('F', 15);
                                $sheet->setWidth('G', 15);
                                $sheet->setWidth('H', 25);
                                $sheet->setWidth('I', 25);
                                $sheet->setWidth('J', 18);
                                $sheet->setWidth('K', 25);
                                $sheet->setWidth('L', 20);


                                $sheet->fromArray($wd_export);
                                $sheet->setWidth('A', 20);
                                $sheet->setWidth('B', 50);
                                $sheet->setWidth('C', 20);
                                $sheet->setWidth('D', 15);
                                $sheet->setWidth('E', 15);
                                $sheet->setWidth('F', 15);
                                $sheet->setWidth('G', 15);
                                $sheet->setWidth('H', 25);
                                $sheet->setWidth('I', 25);
                                $sheet->setWidth('J', 18);
                                $sheet->setWidth('K', 25);
                                $sheet->setWidth('L', 20);

                            });


                        })->store('xlsx', public_path().'/backup/');

                        $zip = new ZipArchive;
                        if ($zip->open(public_path().'/backup/'.$investorGroupName.' - cis.zip', ZipArchive::CREATE) === TRUE) {
                            // ADDING FILES TO ZIP
                            $zip->addFile(public_path().'/backup/Investment Summary.pdf');
                            $zip->addFile(public_path().'/backup/Investment Summary.xlsx');
                            $zip->close();
                            // REMOVING FILES FROM SERVER
                            $pdf = public_path().'/backup/Investment Summary.pdf';
                            $xls = public_path().'/backup/Investment Summary.xlsx';

                            if(file_exists($pdf)){
                                unlink($pdf);
                            }

                            if(file_exists($xls)){
                                unlink($xls);
                            }
                            
                        }


                    }
                    
                }

            }


            $masterZip = new ZipArchive;

            foreach($allInvestors as $type => $investors){
                foreach($investors as $name => $investor){
                    if ($masterZip->open(public_path().'/backup/cis_backup.zip', ZipArchive::CREATE) === TRUE) {
                        // ADDING FILES TO ZIP
                        $masterZip->addFile(public_path().'/backup/'.$name.' - cis.zip');
                        $masterZip->close();
                        // REMOVING FILES FROM SERVER
                        $pdf = public_path().'/backup/'.$name.' - cis.zip';
        
                        if(file_exists($pdf)){
                            unlink($pdf);
                        }
    
                        
                    }
        
                }
            }

         shell_exec("curl -X POST --header 'Content-Type: multipart/form-data' --header 'X-Kloudless-Metadata: {\"name\": \"cis_backup_".date('d-m-Y').".zip\", \"parent_id\": \"FgzDPEyJRhb-xZLC9yVhGOyaVQypI2TDSO6NQsnS8nHbKNkCeicZmsiNKT5iqBfwv\"}' --header 'Authorization: APIKey kMqkK69oxzfHzCvKl_2OwGx3x77Z_I_FK0OCWLpFyK3ypPY2' -F file=@\"/home/master/applications/eeakmnukzn/public_html/public/backup/cis_backup.zip\"  'https://api.kloudless.com/v1/accounts/365442359/storage/files/?overwrite=false'");
            
//        shell_exec("curl -X POST --header 'Content-Type: multipart/form-data' --header 'X-Kloudless-Metadata: {\"name\": \"cis_backup_".date('d-m-Y').".zip\", \"parent_id\": \"FgzDPEyJRhb-xZLC9yVhGOyaVQypI2TDSO6NQsnS8nHbKNkCeicZmsiNKT5iqBfwv\"}' --header 'Authorization: APIKey kMqkK69oxzfHzCvKl_2OwGx3x77Z_I_FK0OCWLpFyK3ypPY2' -F file=@\"/home/ravi/Desktop/Naveen/rightreport/public/backup/cis_backup.zip\"  'https://api.kloudless.com/v1/accounts/365442359/storage/files/?overwrite=false'");


            // dd('uploaded successfully');

        
    }

    public function getInterestAccumalated($bond){

        try{
            $bondInterests = BondInterest::where('bond_id',$bond->id)->sum('interest_paid');
            if($bondInterests == null){
                $bondInterests = 0;
            }


            $noOfDaysSinceInvestment = date_diff(date_create($bond->date_of_issue),date_create(date('Y-m-d')))->format('%a');

            $accruedInterestsTillDate = $bond->amount_invested * (pow( ((100 + $bond->interest_rate)/100), ($noOfDaysSinceInvestment/365) ));
        

            $accruedInterestsTillDate = round($accruedInterestsTillDate - $bond->amount_invested, 2);

            $pendingInterests = $accruedInterestsTillDate - $bondInterests;
        }catch(\Exception $e){

            dd($e, $bond);

        }

        return round($pendingInterests, 2);

    }


    private function calculateBondOverallDetails($investor_id, $investor_type){

        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);


        $bond_inv_details = BondDetails::where('bondinvestor_id', $investor_id)
        ->where('bondinvestor_type', $investor_type)
        ->where('status','=',1)
        ->get();

        // dd($bond_inv_details);


        $interest_accumalated = 0;
        foreach ($bond_inv_details as $bond){
            // dd($this->getInterestAccumalated($bond), $bond);
            $interest_till_date = $this->getInterestAccumalated($bond);
            $interest_accumalated += $interest_till_date;

        }

       





        $bond_investments = BondDetails::where('bondinvestor_id', $investor_id)->where('bondinvestor_type', $investor_type)->where('status','=','1')->pluck('id');

        $total_amount = BondDetails::whereIn('id',$bond_investments)->sum('amount_invested');


        $realisedProfit = BondDetails::where('bondinvestor_id', $investor_id)->where('bondinvestor_type', $investor_type)->where('status','=','0')->pluck('id');
        
        $realisedProfit = BondRedemption::whereIn('bond_id',$realisedProfit)->sum('realised_profit');

        $total_interest_paid = BondInterest::whereIn('bond_id',$bond_investments)->sum('interest_paid');

        $total_interest_paid += $realisedProfit;



//        dd($bond_investments, $total_interest_paid);
        return ['total_amount'=>$fmt->format($total_amount), 'total_interest_paid' => $fmt->format($total_interest_paid), 'total_interest_accumalated' => $fmt->format(round($interest_accumalated,2))];
    }


    private function calculatePmsOverallDetails($investor){
        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

        $investment = $investor->pmsInvestment;

        $total_amount_invested = $investment->sum('amount_invested');
        $total_current_value = $investment->sum('current_value');
        $total_profit_or_loss =$total_current_value - $total_amount_invested;





        foreach ($investment as $inv){
            $inv['total_gain'] = $inv->current_value - $inv->amount_invested;
            $inv['abs_return'] = round(($inv['total_gain']/$inv->amount_invested) * 100, 2);

            $today = Carbon::now()->subDay(0)->toDateString();
            $today = date('Y-m-d', strtotime($today));

            $date_diff = date_diff(date_create($inv->investment_date), date_create($today));
            $date_diff = $date_diff->format("%a");

            if ($date_diff == 0) {
                $annualised_return = 0;
            } else {
                $annualised_return = round((($inv['abs_return'] * 365) / $date_diff), 2);
            }

            $inv['ann_return'] = $annualised_return;
        }


        return ['investment'=>$investment, 'total_amount_invested' => $total_amount_invested, 'total_current_value' => $total_current_value, 'total_profit_or_loss'=>$total_profit_or_loss];

    }

}