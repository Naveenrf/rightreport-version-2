<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\historic_nse;
use App\historic_bse;
use App\smallcap;
use App\midcap;

class IndexUpdates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get daily Index value from google and bse.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('https://www.nseindia.com/live_market/dynaContent/live_watch/stock_watch/niftyStockWatch.json');
        $data = json_decode($res->getBody(), true); 
        $niftydate = date('Y-m-d', strtotime($data['time']));
        $latest_data = $data['latestData'];
        $open_nifty = str_replace(',', '', $latest_data[0]['open']);
        $high_nifty = str_replace(',', '', $latest_data[0]['high']);
        $low_nifty = str_replace(',', '', $latest_data[0]['low']);
        $close_hifty = str_replace(',', '', $latest_data[0]['ltp']);
        // dd($open_nifty);
        $save = historic_nse::insert(['date' => $niftydate, 
                                         'opening_index' => $open_nifty,
                                         'high' => $high_nifty,
                                         'low' => $low_nifty,
                                         'closing_index' => $close_hifty
                                    ]);


        $sensex = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=16');

        foreach ($sensex as $value) {

                $array = explode("@",$value);
                $date_split = explode("|", $array[9]);
                $mydate = date('Y-m-d', strtotime($date_split[0]));
                // dd($array)
                $save = historic_bse::insert(['date' => $mydate, 
                                                 'opening_index' => $array[2],
                                                 'high' => $array[3],
                                                 'low' => $array[4],
                                                 'closing_index' => $array[5]
                                            ]);
        }



        $smallcap_data = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=82');

        foreach ($smallcap_data as $value) {

                $array = explode("@",$value);
                $date_split = explode("|", $array[9]);
                $mydate = date('Y-m-d', strtotime($date_split[0]));
                $save = smallcap::insert(['date' => $mydate, 
                                                 'open' => $array[2],
                                                 'high' => $array[3],
                                                 'low' => $array[4],
                                                 'close' => $array[5]
                                            ]);


        }

        $midcap_data = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=81');

        foreach ($midcap_data as $value) {

                $array = explode("@",$value);
                $date_split = explode("|", $array[9]);
                $mydate = date('Y-m-d', strtotime($date_split[0]));
                $save = midcap::insert(['date' => $mydate, 
                                                 'open' => $array[2],
                                                 'high' => $array[3],
                                                 'low' => $array[4],
                                                 'close' => $array[5]
                                            ]);


        }

    }
}
