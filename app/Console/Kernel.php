<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Spatie\Backup\Commands\BackupCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
           Commands\GetNavValues::class,
           Commands\IndexUpdates::class,
           Commands\BackUpEmail::class,
           Commands\GenerateCis::class,
           Commands\GenerateCis::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('get:nav')
                 ->dailyAt("06:00")
                 ->timezone('Asia/Kolkata');

        $schedule->command('generate:cis')
                 ->dailyAt("23:00")
                 ->timezone('Asia/Kolkata');

        $schedule->command('get:index')
                 ->daily()
                 ->timezone('Asia/Kolkata');
                 
        $schedule->command('backup:run')
            ->daily()
            ->timezone('Asia/Kolkata');

        $schedule->command('backup:clean')
            ->daily()
            ->timezone('Asia/Kolkata');

        $schedule->command('send:backupEmail')
        ->daily()
        ->timezone('Asia/Kolkata');


    }
}
