<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawDetails extends Model
{
    protected $fillable = ['acc_statement'];
}


