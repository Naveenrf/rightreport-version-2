$(document).ready(function(){ 
	var users=new Bloodhound({
		datumTokenizer:Bloodhound.tokenizers.whitespace('name'),
		queryTokenizer:Bloodhound.tokenizers.whitespace,
		remote:{
			url:'/find?search-input=%QUERY%',
			wildcard:'%QUERY%',},
		});
	$('#search-input').typeahead({hint:!1,highlight:!1,minLength:1,},
		{
			source:users.ttAdapter(),
			name:'usersList',
			source:users,
			displayKey:'name',
			templates:{
				empty:['<div class = "suggestion no-user">No User Found!</div>'],
				header:['<div class="list-group search-results-dropdown">'],
				suggestion:function(data){
					var key = 'group_id';
					if (key in data) {
						return '<a href="#" class="list-group-item suggested-user sub-person investor-name" data-groupid="'+data.group_id+'" data-subid="'+data.id+'" data-subperson="'+data.id+'">'+data.member_name+'</a>';
					}else{
						return '<a href="#" data-id="'+data.id+'" class="list-group-item suggested-user individual investor-name" data-person="'+data.id+'">'+data.name+'</a>';
					}
					// return '<a data-id="'+user_data.id+'" data-name = '+user_data.name+' class="list-group-item suggested-user"> <span class="suggested-name">'+user_data.name+'</span> <br><span class="suggested-email">'+user_data.email+'</span><br><span class="suggested-email">'+user_data.mobile+'</span></a>'
				}
			}
		});

	var users_admin=new Bloodhound({
		datumTokenizer:Bloodhound.tokenizers.whitespace('name'),
		queryTokenizer:Bloodhound.tokenizers.whitespace,
		remote:{
			url:'/findAdmin?search-input=%QUERY%',
			wildcard:'%QUERY%',},
		});
	$('#search-input-admin').typeahead({hint:!1,highlight:!1,minLength:1,},
		{
			source:users_admin.ttAdapter(),
			name:'usersList',
			source:users_admin,
			displayKey:'name',
			templates:{
				empty:['<div class = "suggestion no-user">No User Found!</div>'],
				header:['<div class="list-group search-results-dropdown">'],
				suggestion:function(data){
					var key = 'group_id';
					if (key in data) {
						return '<a href="#" class="list-group-item suggested-user sub-person investor-name" data-groupid="'+data.group_id+'" data-subid="'+data.id+'" data-subperson="'+data.id+'">'+data.member_name+'</a>';
					}else{
						return '<a href="#" data-id="'+data.id+'" class="list-group-item suggested-user individual investor-name" data-person="'+data.id+'">'+data.name+'</a>';
					}
					// return '<a data-id="'+user_data.id+'" data-name = '+user_data.name+' class="list-group-item suggested-user"> <span class="suggested-name">'+user_data.name+'</span> <br><span class="suggested-email">'+user_data.email+'</span><br><span class="suggested-email">'+user_data.mobile+'</span></a>'
				}
			}
		});
});