$(document).ready(function(){

    // $('#downloadModal').modal('show');
   $('#export_btn').on('click', function(e){
       e.preventDefault();

       if($('#ex_investor_type').val() == 'cis'){
           //Trigger Pop Up to get the export type

           $('#downloadModal').modal('show');


       }else{
           $('#downloadIndModal').modal('show');

       }
      console.log($(this).serialize());
   });


   $('.set_download_format').on('submit', function(e){
       e.preventDefault();

       if($('#ex_investor_type').val() == 'individual'){

           $('#ex_download_format').val('old_format');
           $('#ex_download_option').val($('#downloadIndModal').find('#download_option').val());
           $('#downloadIndModal').modal('hide');

       }else{

           $('#ex_download_format').val($('#download_format').val());
           $('#ex_download_option').val($('#download_option').val());
           $('#downloadModal').modal('hide');

       }
       // console.log($('#download_format').val(), $('#download_option').val());
       $('#export_form').submit();

   })
});