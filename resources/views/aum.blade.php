<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login-responsive.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <script src="js/loader.js"></script>

    <style>
        .table-wrapper{
            min-height: 80vh;
        }

        #navigation .dropdown{
            display: inline-block;
            margin: 15px;
            font-size: 16px;
        }

        .form-card{
            width: 30%;
            margin-left: auto;
            margin-right: auto;
            display: block;
            margin-top: 5%;
            background-color: white;
            padding: 2% ;
            border-radius: 5px;
            box-shadow: 0px 0px 5px #e2e2e2;
        }

        .green-btn, .green-btn:hover{
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
    </style>
</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
            <a class="navbar-brand" href="/admin_home"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
            <a href="/home" class="module-links">MF</a>
            <a href="/admin_bonds" class="module-links">Bonds</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <div class="col-xs-5 padding-lr-zero">
                    <span id = "user_name">{{\Auth::user()->name}}</span>
                </div>
                <div class="col-xs-2 text-center padding-lr-zero">

                    @include('layouts.admin_sidemenu')
                </div>
            </li>
        </ul>

    </div>
</nav>

<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>

<div class = "container-fluid">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" id="contentbar_wrapper">
            <div id="contentbar">
                <div class="col-lg-12 col-md-12 " id="client_relbar">
                    <p id="client_det"><span id="client_parent">AUM Management</span></p>
                </div>


                <div class="col-lg-12 col-md-12 padding-lr-zero">
                    @if(\Illuminate\Support\Facades\Session::has('failure-message'))
                        <div class="alert alert-info alert-danger alert-su">{{ Session::get('failure-message') }}</div>
                    @endif

                    @if(\Illuminate\Support\Facades\Session::has('success-message'))
                        <div class="alert alert-info alert-success alert-su">{{ Session::get('success-message') }}</div>
                    @endif
                    <div class="table-wrapper">

                        <h4 style="margin-left: 15px; margin-top: 20px;">Total AUM</h4>
                        <table class="table table-bordered">
                            <thead>
                            <th>Type</th>
                            <th>Value</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total MF AUM</td>
                                    <td>{{$fmt->format(round($totalMfAum, 2))}}</td>
                                </tr>

                                <tr>
                                    <td>Total Bond AUM</td>
                                    <td>{{$fmt->format(round($totalBondAum, 2))}}</td>
                                </tr>

                                <tr style="font-weight: bold; background-color: #d3fff2">
                                    <td>Total AUM</td>
                                    <td>{{$fmt->format(round($totalCompanyAum, 2))}}</td>
                                </tr>

                            </tbody>
                        </table>

                        <h4 style="margin-left: 15px; margin-top: 20px;">Mutual Fund AUM - Group</h4>
                        <table class="table table-bordered">
                            <thead>
                            <th>Type</th>
                            <th>Value</th>
                            </thead>
                            <tbody>
                            @foreach($mfAum['group'] as $type => $gaum)
                                <tr>
                                    <td>{{$type}}</td>
                                    <td>{{$fmt->format(round($gaum, 2))}}</td>
                                </tr>
                            @endforeach
                            <tr style="font-weight: bold; background-color: #d3fff2">
                                <td>Total</td>
                                <td>{{$fmt->format(round($totalGroupAum, 2))}}</td>
                            </tr>
                            </tbody>
                        </table>

                        <h4 style="margin-left: 15px; margin-top: 20px;">Mutual Fund AUM - Individual</h4>
                        <table class="table table-bordered">
                            <thead>
                            <th>Type</th>
                            <th>Value</th>
                            </thead>
                            <tbody>
                            @foreach($mfAum['ind'] as $type => $gaum)
                                <tr>
                                    <td>{{$type}}</td>
                                    <td>{{$fmt->format(round($gaum, 2))}}</td>
                                </tr>
                            @endforeach
                            <tr style="font-weight: bold; background-color: #d3fff2">
                                <td>Total</td>
                                <td>{{$fmt->format(round($totalIndividualAum, 2))}}</td>
                            </tr>
                            </tbody>
                        </table>


                        <h4 style="margin-left: 15px; margin-top: 20px;">Bond AUM</h4>
                        <table class="table table-bordered">
                            <thead>
                            <th>Type</th>
                            <th>Value</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Individual</td>
                                <td>{{$fmt->format(round($personBondDetails, 2))}}</td>
                            </tr>
                            <tr>
                                <td>Group</td>
                                <td>{{$fmt->format(round($groupMembersBondDetails, 2))}}</td>
                            </tr>
                            <tr style="font-weight: bold; background-color: #d3fff2">
                                <td>Total</td>
                                <td>{{$fmt->format(round($totalBondAum, 2))}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>



<div id="addNewSchemeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add New Scheme</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="add_new_scheme_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" class="mont-reg" name="new_scheme_name" id = "new_scheme_name" required placeholder="Scheme Name">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="scheme_code" id = "scheme_code" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="scheme_type" id = "scheme_type" required>

                        </select>
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="amc_name" id = "amc_name" required>

                        </select>
                    </div>

                    <div class="form-group">
                        <input type="file" class="mont-reg" name="scheme_file" id = "scheme_file" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_user_btn" id = "add_user_btn" class="btn btn-primary blue-btn center-block" value="Add Scheme">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>

<div id="userStatusModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Status</h4>
            </div>
            <div class="modal-body">
                <p id="addition_status" style="margin-top: 20px;
    font-size: 16px;" class="mont-reg text-center"></p>

                <div class="center-block">
                    <button type="button" style="margin-left: auto; margin-right: auto; font-size: 16px;" onClick="window.location.reload();" class="green-btn">Okay</button>
                </div>
            </div>
        </div>

    </div>
</div>


<script>

</script>

</body>
</html>

