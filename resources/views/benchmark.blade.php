
<!DOCTYPE html>
<html lang="en">
<head>
  <title>RightReport</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <link rel="stylesheet" href="{{url('css/index.css')}}">
  <link rel="stylesheet" href="{{url('css/login-responsive.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
  <link rel="stylesheet" href="{{url('css/sector.css')}}">
  <!-- <script src="https://use.fontawesome.com/8fa68942ad.js"></script> -->
  <style type="text/css">
    .amc-wrapper{
      max-height: 60vh;
      overflow-y: scroll;
    }

    #benchmark-list{
    	border-bottom: 2px solid gainsboro;
    	margin-left: 0px;
    }

    #benchmark-list li {
    	padding: 15px;
    }

    .active-class{
    	border-bottom: 3px solid #00AF64;
    }

    .benchmark-name{
    	MARGIN-BOTTOM: 5px;
    	font-size: 14px;
    	font-weight: bold;
    }
    .benchmark-perf{
    	font-size: 18px;
    	font-weight: 400;
    }

    .green{
    	color: #67D484;
    }	

    .red{
    	color: red;
    }

    .table{
    	margin-top: 20px;
    	border-top: 2px solid gainsboro;
    }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid" id="navbar_container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "{{url('icons/sidebar.png')}}"/></a>
      <a class="navbar-brand" href="#"><img class="logo" src = "{{url('img/Right-repor-logo.svg')}}"/></a>
    </div>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <div class="col-xs-3 "><p class="img-circle" id = "profile_text">V</p></div>
        <div class="col-xs-2 padding-lr-zero">
          <span id = "user_name">Vasudev</span>
        </div>
        <div class="col-xs-2 text-center padding-lr-zero">
          <div class="dropdown" id="drop">
                <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
                <ul class="dropdown-menu">
                  <li><a href="#" id="settings">Setting</a></li>
                  <li><a href="#" id="logout">Logout</a></li>
                </ul>
            </div>
        </div>
      </li>
    </ul>
  </div>
</nav>

<div class = "container-fluid">

<div class="row">

  <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
      <div class = "col-lg-12 col-md-12 col-sm-12">
        <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="sector-text-container">
          <ul class="list-inline" id="benchmark-list">
		  	<li class="active-class"><a href="#" id="equity">EQUITY</a></li>
		  	<li><a href="#" id="debt">DEBT</a></li>
		  	<li><a href="#" id="balanced">BALANCED</a></li>
		  </ul>

		  <div class = "col-lg-4 col-md-4 col-sm-4">
		  	<p class="benchmark-name" id="col-one-top-title">SENSEX</p>
		  	<p class="benchmark-perf" id="col-one-top-value">Rs.{{$sensex}}</p>
		  </div>
		  <div class = "col-lg-4 col-md-4 col-sm-4">
		  	<p class="benchmark-name" id="col-two-top-title">NIFTY</p>
		  	<p class="benchmark-perf" id="col-two-top-value">Rs.{{$nifty}}</p>
		  </div>
		  <div class = "col-lg-4 col-md-4 col-sm-4">
		  	<p class="benchmark-name" id="col-three-top-title">RIGHTFUNDS</p>
		  	<p class="benchmark-perf" id="col-three-top-value">Rs.{{$RFtotal_equity}}</p>
		  </div>

		  <div class = "col-lg-4 col-md-4 col-sm-4">
		  	<p class="benchmark-name" id="col-one-bottom-title">RF DIFFERENCE</p>
        @if($sensex_diff > 0)
		  	<p class="benchmark-perf" style="color: green" id="col-one-bottom-value">{{$sensex_diff}}</p>
        @else
        <p class="benchmark-perf" style="color: red" id="col-one-bottom-value">{{$sensex_diff}}</p>
        @endif
		  </div>
		  <div class = "col-lg-4 col-md-4 col-sm-4">
		  	<p class="benchmark-name" id="col-two-bottom-title">RF DIFFERENCE</p>
		  	@if($nifty_diff > 0)
        <p class="benchmark-perf" style="color: green" id="col-two-bottom-value">{{$nifty_diff}}</p>
        @else
        <p class="benchmark-perf" style="color: red" id="col-two-bottom-value">{{$nifty_diff}}</p>
        @endif
		  </div>

          <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero amc-wrapper">
            <table class="table table-bordered">
            <thead id="inv_table_head">
              <tr>
                <th>Investor Name</th>
                <th>Date of Investment</th>
                <th>Amount Invested</th>
                <th>Sensex</th>
                <th>NIFTY</th>  
              </tr>
            </thead>
            <tbody id="investment_table_body">
              @foreach ($equity as $set)
              <tr>
                <td><a href="#" class = "investor-name">{{$set['name']}}</a></td>
                <td>{{$set['dop']}}</td>
                <td>{{$set['inv_amt']}}</td>
                <td>{{$set['if_sensex']}}</td>
                <td>{{$set['if_nifty']}}</td>
              </tr>
              @endforeach
            </tbody >
            <tfoot id="investment_table_foot"> 
              <tr style="background-color: #f1f8fd">
                <th>Total</th>
                <th></th>
                <th>{{$invest_amount_equity}}</th>
                <th>{{$sensex}}</th>
                <th>{{$nifty}}</th>  
              </tr>
            </tfoot>  
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

<script type="text/javascript">
  $('#add_scheme_btn').on('click',function(){
      $('#addSchemeModal').modal('show');
  });

  $(document).on('click','#sidebar_icon',function(){

        var img_src = $('#sidebar_icon>img').attr('src');
        if (img_src == "icons/sidebar.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar_closed.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
            $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
          });
        }

        if (img_src == "icons/sidebar_closed.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-12 col-md-12 col-sm-12');
            $('#contentbar_wrapper').addClass('col-lg-9 col-md-9 col-sm-9');            
          });
        }

  });

  $(document).ready(function(){
    var equity = <?php echo json_encode($equity);?>;
    var debt = <?php echo json_encode($debt);?>;
    var balanced = <?php echo json_encode($balanced);?>;
    var RFtotal_equity = "{{$RFtotal_equity}}";
    var RFtotal_debt = "{{$RFtotal_debt}}";
    var RFtotal_balanced = "{{$RFtotal_balanced}}";
    var sensex = "{{$sensex}}";
    var nifty = "{{$nifty}}";
    var rbi_current = "{{$rbi_current}}";
    var compound_current = "{{$compound}}";
    var total_e = "{{$invest_amount_equity}}";
    var total_d = "{{$invest_amount_debt}}";
    var total_b = "{{$invest_amount_balanced}}";
    var diff_sensex = "{{$sensex_diff}}";
    var diff_nifty = "{{$nifty_diff}}";
    var diff_rbi = "{{$rbi_deff}}";
    var diff_compound = "{{$compound_deff}}";
    console.log(RFtotal_equity);

    $('#equity').on('click',function(){
      
      $('.active-class').removeClass( 'active-class' );
      $( this ).parent().addClass( 'active-class' );
      $('#col-three-top-value').show();
      $('#col-three-top-title').show();
      $('#col-two-bottom-title').show();
      $('#col-two-bottom-value').show();

      $('#col-one-top-title').text('SENSEX');
      $('#col-one-top-value').text('Rs.'+sensex);
      $('#col-one-bottom-title').text('RF DIFFERENCE');
      if (diff_sensex > 0) {
        $('#col-one-bottom-value').text(diff_sensex).css('color','green');
      }else{
        $('#col-one-bottom-value').text(diff_sensex).css('color','red');
      }

      $('#col-two-top-title').text('NIFTY');
      $('#col-two-top-value').text('Rs.'+nifty);
      $('#col-two-bottom-title').text('RF DIFFERENCE');
      if (diff_nifty > 0) {
        $('#col-two-bottom-value').text(diff_nifty).css('color','green');
      }else{
        $('#col-two-bottom-value').text(diff_nifty).css('color','red');
      }

      $('#col-three-top-title').text('RIGHTFUNDS');
      $('#col-three-top-value').text('Rs.'+RFtotal_equity);


      $('#inv_table_head').empty();
      $('#investment_table_body').empty();
      $('#investment_table_foot').empty();


          var append_head = '<tr>'+
            '<th>Investor Name</th>'+
            '<th>Date of Investment</th>'+
            '<th>Amount Invested</th>'+
            '<th>Sensex</th>'+
            '<th>NIFTY</th>'+
          '</tr>';

          $('#inv_table_head').append(append_head);

          $.each(equity,function(key,value){
              var append_data = '<tr>'+
                '<td><p class="table_data scheme_name_p">'+value.name+'</p></td>'+
                '<td class="dop"><p class="table_data">'+value.dop+'</p></td>'+
                '<td><p class="table_data">'+value.inv_amt+'</p></td>'+
                '<td><p class="table_data">'+value.if_sensex+'</p></td>'+
                '<td><p class="table_data">'+value.if_nifty+'</p></td>'+
              '</tr>';

              $('#investment_table_body').append(append_data);
          });

          var append_foot = '<tr style="background-color: #f1f8fd">'+
            '<th>Total</th>'+
            '<th></th>'+
            '<th>'+total_e+'</th>'+
            '<th>'+sensex+'</th>'+
            '<th>'+nifty+'</th>'+
          '</tr>';

          $('#investment_table_foot').append(append_foot);
    });

    $('#debt').on('click',function(){

      $('.active-class').removeClass( 'active-class' );
      $( this ).parent().addClass( 'active-class' );
      $('#col-three-top-value').hide();
      $('#col-three-top-title').hide();
      $('#col-two-bottom-title').hide();
      $('#col-two-bottom-value').hide();

      $('#col-one-top-title').text('RBI');
      $('#col-one-top-value').text('Rs.'+rbi_current);
      $('#col-one-bottom-title').text('RF DIFFERENCE');
      if (diff_rbi > 0) {
        $('#col-one-bottom-value').text(diff_rbi).css('color','green');
      }else{
        $('#col-one-bottom-value').text(diff_rbi).css('color','red');
      }

      $('#col-two-top-title').text('RIGHTFUNDS');
      $('#col-two-top-value').text('Rs.'+RFtotal_debt);

      $('#inv_table_head').empty();
      $('#investment_table_body').empty();
      $('#investment_table_foot').empty();


          var append_head = '<tr>'+
            '<th>Investor Name</th>'+
            '<th>Date of Investment</th>'+
            '<th>Amount Invested</th>'+
            '<th>RBI</th>'+
          '</tr>';

          $('#inv_table_head').append(append_head);

          $.each(debt,function(key,value){
              var append_data = '<tr>'+
                '<td><p class="table_data scheme_name_p">'+value.name+'</p></td>'+
                '<td class="dop"><p class="table_data">'+value.dop+'</p></td>'+
                '<td><p class="table_data">'+value.inv_amt+'</p></td>'+
                '<td><p class="table_data">'+value.rbi_amt+'</p></td>'+
              '</tr>';

              $('#investment_table_body').append(append_data);
          });

          var append_foot = '<tr style="background-color: #f1f8fd">'+
            '<th>Total</th>'+
            '<th></th>'+
            '<th>'+total_d+'</th>'+
            '<th>'+rbi_current+'</th>'+
          '</tr>';

          $('#investment_table_foot').append(append_foot);
    });

    $('#balanced').on('click',function(){

      $('.active-class').removeClass( 'active-class' );
      $( this ).parent().addClass( 'active-class' );
      $('#col-three-top-value').hide();
      $('#col-three-top-title').hide();
      $('#col-two-bottom-title').hide();
      $('#col-two-bottom-value').hide();

      $('#col-one-top-title').text('compound');
      $('#col-one-top-value').text('Rs.'+compound_current);
      $('#col-one-bottom-title').text('RF DIFFERENCE');
      if (diff_compound > 0) {
        $('#col-one-bottom-value').text(diff_compound).css('color','green');
      }else{
        $('#col-one-bottom-value').text(diff_compound).css('color','red');
      }

      $('#col-two-top-title').text('RIGHTFUNDS');
      $('#col-two-top-value').text('Rs.'+RFtotal_balanced);


      $('#inv_table_head').empty();
      $('#investment_table_body').empty();
      $('#investment_table_foot').empty();


          var append_head = '<tr>'+
            '<th>Investor Name</th>'+
            '<th>Date of Investment</th>'+
            '<th>Amount Invested</th>'+
            '<th>compound</th>'+
          '</tr>';

          $('#inv_table_head').append(append_head);

          $.each(balanced,function(key,value){
              var append_data = '<tr>'+
                '<td><p class="table_data scheme_name_p">'+value.name+'</p></td>'+
                '<td class="dop"><p class="table_data">'+value.dop+'</p></td>'+
                '<td><p class="table_data">'+value.inv_amt+'</p></td>'+
                '<td><p class="table_data">'+value.compound+'</p></td>'+
              '</tr>';

              $('#investment_table_body').append(append_data);
          });

          var append_foot = '<tr style="background-color: #f1f8fd">'+
            '<th>Total</th>'+
            '<th></th>'+
            '<th>'+total_b+'</th>'+
            '<th>'+compound_current+'</th>'+
          '</tr>';

          $('#investment_table_foot').append(append_foot);
    });    


  });


</script>


</body>
</html>
