<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login-responsive.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <script src="js/loader.js"></script>

    <style>
        .table-wrapper{
            min-height: 80vh;
        }

        #navigation .dropdown{
            display: inline-block;
            margin: 15px;
            font-size: 16px;
        }

        .form-card{
            width: 30%;
            margin-left: auto;
            margin-right: auto;
            display: block;
            margin-top: 5%;
            background-color: white;
            padding: 2% ;
            border-radius: 5px;
            box-shadow: 0px 0px 5px #e2e2e2;
        }

        .green-btn, .green-btn:hover{
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
    </style>
</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
            <a class="navbar-brand" href="/admin_home"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
            <a href="/home" class="module-links">MF</a>
            <a href="/admin_bonds" class="module-links">Bonds</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <div class="col-xs-5 padding-lr-zero">
                    <span id = "user_name">{{\Auth::user()->name}}</span>
                </div>
                <div class="col-xs-2 text-center padding-lr-zero">

                    @include('layouts.admin_sidemenu')
                </div>
            </li>
        </ul>

    </div>
</nav>

<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>

<div class = "container-fluid">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" id="contentbar_wrapper">
            <div id="contentbar">
                <div class="col-lg-12 col-md-12 " id="client_relbar">
                    <p id="client_det"><span id="client_parent">Transaction Management</span></p>
                </div>


                <div class="col-lg-12 col-md-12 padding-lr-zero">
                    @if(\Illuminate\Support\Facades\Session::has('failure-message'))
                        <div class="alert alert-info alert-danger alert-su">{{ Session::get('failure-message') }}</div>
                    @endif

                    @if(\Illuminate\Support\Facades\Session::has('success-message'))
                        <div class="alert alert-info alert-success alert-su">{{ Session::get('success-message') }}</div>
                    @endif
                    <div class="form-card">
                        <form action="/generate_transaction" method="POST">

                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="investor">Investor</label>
                                <select name="investor" id="investor" class="input-field">
                                   @foreach(\App\Group::all() as $group)
                                        <option value="{{$group->id}}|group">{{$group->name}}</option>
                                    @endforeach

                                   @foreach(\App\Person::all() as $person)
                                       <option value="{{$person->id}}|person">{{$person->name}}</option>
                                   @endforeach</select>
                            </div>
                            <div class="form-group">
                                <label for="password">From</label>
                                <input type="text" name="from-year" id="from-year" class="input-field" required>
                            </div>

                            <div class="form-group">
                                <label for="password">To </label>
                                <input type="text" name="to-year" id="to-year" class="input-field" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary green-btn center-block" value="Download">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>



<script>
$('#from-year,#to-year').datepicker({
    changeMonth : true,
    changeYear : true,
    dateFormat : 'dd-mm-yy'
})
</script>

</body>
</html>

