<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login-responsive.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <script src="js/loader.js"></script>

    <style>
        .table-wrapper{
            min-height: 80vh;
        }

        #navigation .dropdown{
            display: inline-block;
            margin: 15px;
            font-size: 16px;
        }
    </style>
</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
            <a class="navbar-brand" href="/admin_home"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <div class="col-xs-5 padding-lr-zero">
                    <span id = "user_name">{{\Auth::user()->name}}</span>
                </div>
                <div class="col-xs-2 text-center padding-lr-zero">
                    @include('layouts.admin_sidemenu')

                </div>
            </li>
        </ul>

{{--        @include('layouts.scheme_navigation')--}}

    </div>
</nav>

<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>

<div class = "container-fluid">
a
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" id="contentbar_wrapper">
            <div id="contentbar">
                <div class="col-lg-12 col-md-12 " id="client_relbar">
                    <p id="client_det"><span id="client_parent">User Management</span></p>
                    <p class="pull-right"><button type="button"  class="green-btn btn btn-primary" data-toggle="modal" data-target="#addNewSchemeModal">Add User</button></p>
                </div>
                @if($errors->has())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                @if(\Illuminate\Support\Facades\Session::has('message'))
                    <div class="alert alert-info alert-success alert-su">{{ Session::get('message') }}</div>
                @endif

                <div class="col-lg-12 col-md-12 padding-lr-zero">
                    <div class="table-wrapper" id="table-wrapper">
                        <h4 class="table-name-header purchase-header">Users</h4>
                        <table class="table table-bordered" id="investment_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Name</p></th>
                                <th><p class="table_heading">Email</p></th>
                                <th><p class="table_heading">Password</p></th>
                                <th><p class="table_heading">Role</p></th>
                                <th><p class="table_heading">Edit</p></th>
                                <th><p class="table_heading">Delete</p></th>
                            </tr>
                            </thead>
                            <tbody id="investment_table_body">
                                @foreach(\App\User::all() as $user)
                                    <tr data-id="{{$user->id}}">
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>*******</td>
                                        <td>{{$user->role == 1 ? 'Admin' : 'Normal User'}}</td>
                                        <td><a href="#" class="edit-user"><i class="material-icons">edit</i></a></td>
                                        <td><a href="delete_user/{{$user->id}}"><i class="material-icons">delete</i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>


</div>



<div id="addNewSchemeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add New User</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="/add_new_user" id="add_new_scheme_form" >
                    {{csrf_field()}}
                    <input type="hidden" name="c_type" id="c_type" value="create">
                    <input type="hidden" name="user_id" id="user_id" >
                    <div class="form-group">
                        <input type="text" class="mont-reg input-field" name="name" id = "name" required placeholder="User Name">
                    </div>

                    <div class="form-group">
                        <input type="email" class="mont-reg input-field" name="email" id = "email" required placeholder="Email">
                    </div>

                    <div class="form-group">
                        <select class="mont-reg input-field" name="type" id = "type" required>
                            <option value="1">Admin</option>
                            <option value="0">Normal</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="password" id = "password" required placeholder="Password">

                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_user_btn" id = "add_user_btn" class="btn btn-primary blue-btn center-block" value="Add User">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>

<div id="userStatusModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Status</h4>
            </div>
            <div class="modal-body">
                <p id="addition_status" style="margin-top: 20px;
    font-size: 16px;" class="mont-reg text-center"></p>

                <div class="center-block">
                    <button type="button" style="margin-left: auto; margin-right: auto; font-size: 16px;" onClick="window.location.reload();" class="green-btn">Okay</button>
                </div>
            </div>
        </div>

    </div>
</div>


<script>
    $(document).ready(function(){
        $('.edit-user').on('click', function(){
            $parent = $(this).parent().parent();
           $userId = $parent.data('id');
           $children = $parent.children();

           $('#name').val($children[0].innerHTML);
           $('#user_id').val($userId);
            $('#email').val($children[1].innerHTML);
            $('#c_type').val('update');
            $('#add_user_btn').val('Update User');
            $('#modal_header').text('Update User');
            if($children[3].innerHTML == 'Normal User'){
                $('#type').val(0);
            }else{
                $('#type').val(1);

            }

           $('#addNewSchemeModal').modal('show');
        });
    })
</script>

</body>
</html>

