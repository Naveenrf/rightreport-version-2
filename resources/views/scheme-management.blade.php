<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login-responsive.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <script src="js/loader.js"></script>

    <style>
        .table-wrapper{
            min-height: 80vh;
        }

        #navigation .dropdown{
            display: inline-block;
            margin: 15px;
            font-size: 16px;
        }
    </style>
</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
            <a class="navbar-brand" href="/admin_home"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <div class="col-xs-5 padding-lr-zero">
                    <span id = "user_name">{{\Auth::user()->name}}</span>
                </div>
                <div class="col-xs-2 text-center padding-lr-zero">

                    @include('layouts.admin_sidemenu')
                </div>
            </li>
        </ul>

    @include('layouts.scheme_navigation')

    </div>
</nav>

<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>

<div class = "container-fluid">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" id="contentbar_wrapper">
            <div id="contentbar">
                <div class="col-lg-12 col-md-12 " id="client_relbar">
                    <p id="client_det"><span id="client_parent">Scheme Management</span></p>
                    <p class="pull-right"><button type="button"  class="green-btn btn btn-primary" data-toggle="modal" data-target="#addNewSchemeModal">Add Scheme</button></p>

                    <span id="all-container">
        <span id="pan_container" style="display: none;">
              <span id="xirr">
              </span>
        </span>
      </span>
                </div>


                <div class="col-lg-12 col-md-12 padding-lr-zero">
                    <div class="table-wrapper" id="table-wrapper">
                        <h4 class="table-name-header purchase-header">Invested Scheme</h4>
                        <table class="table table-bordered" id="investment_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Scheme Type</p></th>
                                <th><p class="table_heading">Amt. Invested</p></th>
                                <th><p class="table_heading">Units</p></th>
                                <th><p class="table_heading">Current Nav.</p></th>
                                <th><p class="table_heading">Current Mrkt Val.</p></th>
                            </tr>
                            </thead>
                            <tbody id="investment_table_body">
                                @foreach($investment_array as $inv)
                                    <tr>
                                        <td>{{$inv['scheme_name']}}</td>
                                        <td>{{$inv['scheme_type']}}</td>
                                        <td>{{$fmt->format($inv['amount_invested'])}}</td>
                                        <td>{{$fmt->format($inv['units'])}}</td>
                                        <td>{{$inv['current_nav']}}</td>
                                        <td>{{$fmt->format($inv['current_value'])}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <h4 class="table-name-header purchase-header">Not Invested Scheme</h4>
                        <table class="table table-bordered" id="investment_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Scheme Type</p></th>
                                <th><p class="table_heading">Amt. Invested</p></th>
                                <th><p class="table_heading">Units</p></th>
                                <th><p class="table_heading">Current Mrkt Val.</p></th>
                            </tr>
                            </thead>
                            <tbody id="investment_table_body">
                            @foreach($not_invested_scheme as $name => $inv)
                                <tr>
                                    <td>{{$inv[0]['scheme_name']}}</td>
                                    <td>{{$inv[0]['scheme_type']}}</td>
                                    <td>{{$fmt->format(0)}}</td>
                                    <td>{{0}}</td>
                                    <td>{{$fmt->format(0)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>

            </div>
        </div>
    </div>


</div>



<div id="addNewSchemeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add New Scheme</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="add_new_scheme_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" class="mont-reg" name="new_scheme_name" id = "new_scheme_name" required placeholder="Scheme Name">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="scheme_code" id = "scheme_code" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="scheme_type" id = "scheme_type" required>
                                @foreach($scheme_types as $schemes)
                                    <option value="{{$schemes['id']}}">{{$schemes['scheme_sub']}}</option>
                                @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="amc_name" id = "amc_name" required>
                                @foreach($amc_names as $amc)
                                    <option value="{{$amc['id']}}">{{$amc['name']}}</option>
                                @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="file" class="mont-reg" name="scheme_file" id = "scheme_file" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_user_btn" id = "add_user_btn" class="btn btn-primary blue-btn center-block" value="Add Scheme">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>

<div id="userStatusModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Status</h4>
            </div>
            <div class="modal-body">
                <p id="addition_status" style="margin-top: 20px;
    font-size: 16px;" class="mont-reg text-center"></p>

                <div class="center-block">
                    <button type="button" style="margin-left: auto; margin-right: auto; font-size: 16px;" onClick="window.location.reload();" class="green-btn">Okay</button>
                </div>
            </div>
        </div>

    </div>
</div>


<script>


    $(document).on('submit','#add_new_scheme_form',function(e){
        $('#add_scheme_btn').css({'display':'none'});

        var scheme_name = $('#new_scheme_name').val();
        var scheme_code = $('#scheme_code').val();
        var scheme_type = $('#scheme_type').find(":selected").val();
        var amc_name = $('#amc_name').find(":selected").val();
        var scheme_file = $('#scheme_file').get(0).files[0];


        var formData = new FormData();

        formData.append('new_scheme_name',scheme_name);
        formData.append('scheme_code',scheme_code);
        formData.append('scheme_file',scheme_file);
        formData.append('scheme_type',scheme_type);
        formData.append('amc_name',amc_name);


        //var formData = 'new_scheme_name='+scheme_name+'&scheme_code='+scheme_code+'&scheme_file='+scheme_file;
        console.log(formData);

        $.ajax({
            type: "POST",
            url: "/add_scheme",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            //async : false,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                console.log(data);
                //addInvestment(data);
                $('#addNewSchemeModal').modal('hide');
                if (data.msg == "1") {
                    $('#addition_status').text(data.response);
                    $('#userStatusModal').modal('show');
                }
                if (data.msg == "0") {
                    //alert('user addition failed');
                    $('#addition_status').text(data.response);
                    $('#userStatusModal').modal('show');
                }
            },
            error: function(xhr, status, error) {

            },
        });

        e.preventDefault();

    });
</script>

</body>
</html>

