<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>
        <!DOCTYPE html>
<html>
<head>
    <title>CIS PDF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    {{--<link rel="stylesheet" href="css/bootstrap.min.css">--}}
    <link rel="stylesheet" href="css/cispdf.css">

    <style>
        table{
            border-collapse: collapse;
        }

        body{
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        #address-div p{
            margin-bottom: 0px !important;
            padding-bottom: 0px;
        }
    </style>
</head>


<body>
<header>
    <div id="header-view">
        <img class="logo" src = "icons/pro_logo.png"/>
        <div id="address-div">
            <p class="m-b-0">Level 1, No 1, Balaji First Avenue,</p>
            <p class="m-b-0"  style="margin-top: 2px;">T.Nagar,</p>
            <p class="m-b-0" style="margin-top: 2px;">Chennai - 600017</p>
            <p class="m-b-0" style="margin-top: 2px;">Ph: +91 8825888200</p>
        </div>
    </div>
</header>
<div id="info-div">
    <div id="title-div">
        <p id="name" style="padding-bottom: 5px;"><strong>Consolidated Investment Summary</strong> <span style="font-size: 12px; font-weight: normal !important;">(as on {{date('d-m-Y',strtotime($date))}})</span></p>
    </div>


</div>
<main>

    <?php $net_profit = round($unreal_pl + $real_pl, 2);
    //        $net_amount_invested = $current - ($net_profit);

    ?>

    <?php
    $realised_profit_or_loss = round($real_pl, 2);
    $current_market_value = round($current, 2);
    $unreal_profit_or_loss = round($unreal_pl, 2);

    if($realised_profit_or_loss < 0){

        if($unreal_profit_or_loss > 0){
            $net_amount_invested = round(($current_market_value - $unreal_profit_or_loss) + abs($realised_profit_or_loss), 2);
            //            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
        }

        if($unreal_profit_or_loss < 0){
            $net_amount_invested = round($current_market_value + (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss)), 2);
        }

    }

    //        if($realised_profit_or_loss < 0){
    //
    //            if($unreal_profit_or_loss < 0){
    //                $net_amount_invested = $current_market_value + (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss));
    //            }
    //
    //        }


    if($realised_profit_or_loss > 0){

        if($unreal_profit_or_loss < 0){
            $net_amount_invested = round(($current_market_value + abs($unreal_profit_or_loss)) - $realised_profit_or_loss, 2);
            //            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
        }

        if($unreal_profit_or_loss > 0){
            $net_amount_invested = round($current_market_value - (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss)), 2);
            //            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
        }

    }

    //        if($realised_profit_or_loss > 0){
    //
    //            if($unreal_profit_or_loss > 0){
    //                $net_amount_invested = $current_market_value - (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss));
    //                //            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
    //            }
    //
    //        }




    if($realised_profit_or_loss == 0){
        if($unreal_profit_or_loss > 0){
            $net_amount_invested = round($current_market_value - $unreal_profit_or_loss, 2);
        }

        if($unreal_profit_or_loss < 0){
            $net_amount_invested = round($current_market_value + (abs($unreal_profit_or_loss)), 2);
        }
    }

    //        if($realised_profit_or_loss == 0){
    //            if($unreal_profit_or_loss < 0){
    //                $net_amount_invested = $current_market_value + (abs($unreal_profit_or_loss));
    //            }
    //        }

    ?>

        <div id="info-div">
            <div id="title-div">
                <p id="name" style="padding-bottom: 5px;"><strong>Net Profit : </strong>(as on {{date('d-m-Y',strtotime($date))}})</p>
            </div>
        </div>


    <table class="table table-bordered" style="width: 49%">
        <tbody>

        <tr>
            <td style=" width: 33%; text-align: center;">Current Market Value</td>
            <td style="width: 33%; text-align: center;"><?php echo $fmt->format($current); ?></td>
        </tr>

        <tr>
            <td style="width: 33%; text-align: center;">Net Amount Invested</td>
            <td style="width: 33%; text-align: center;"><?php echo $fmt->format($net_amount_invested); ?></td>
        </tr>
        <tr>
            <td style="width: 33%; text-align: center;">Realised Profit/Loss</td>
            <td style="width: 33%; text-align: center;"><?php echo $fmt->format($real_pl); ?></td>
        </tr>
        <tr>
            <td style="width: 33%; text-align: center;">Unrealised Profit/Loss</td>
            <td style="width: 33%; text-align: center;"><?php echo $fmt->format($unreal_pl); ?></td>
        </tr>

        <tr id="total-tr">
            <td style="width: 33%; text-align: center;">Net Profit/Loss <p>(Realised + Unrealised)</p></td>
            <td style="width: 33%; text-align: center;"><?php echo $fmt->format($net_profit); ?></td>
        </tr>

        </tbody>
    </table>


        @if($download_format == 'new_format')
        {{--Only for Excel group--}}
            <div style="width: 50%; display: block; margin-right: 5px; margin-top: 10px;">
                <p id="name" style="padding-bottom: 5px;"><strong>Consolidated Group Summary</strong></p>
                <table style="width: 100%;" class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Investment Type</td>
                            <td>Amount</td>
                            <td>%</td>
                        </tr>
                    @foreach($consolidated_group_inv as $key => $amount)
                        <tr>
                            <td>{{$key}}</td>
                            <td>{{$fmt->format($amount)}}</td>
                            <td>{{round(($amount/array_sum(array_values($consolidated_group_inv)))*100, 2)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            @foreach($consolidated_investment as $memberId => $inves)
                <div style="margin-right: 5px; width: 50%; margin-top: 10px;">
                    @if(array_sum(array_values($inves)) > 0)
                        <p style="padding-bottom: 0px;"><strong>{{\App\GroupMembers::where('id',$memberId)->value('member_name')}}</strong></p>
                        <table style="width: 100%;" class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Investment Type</td>
                                    <td>Amount</td>
                                    <td>%</td>
                                </tr>


                                @foreach($inves as $key => $amount)
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{$fmt->format($amount)}}</td>
                                        @if(array_sum(array_values($inves)) != 0)
                                        <td>{{round(($amount/array_sum(array_values($inves)))*100, 2)}}</td>
                                            @else
                                            <td>0</td>
                                        @endif
                                    </tr>
                                @endforeach
                    @endif

                        </tbody>
                    </table>
                </div>
            @endforeach
        @endif
        {{--<p style="margin-bottom: 5px; font-size: 13px;">Note : Mutual funds does not include SIP/Liquid/FIP</p>--}}
        {{--<p style="margin-bottom: 5px; font-size: 13px;">Note : Fixed Income Product includes Debt Mutual funds and Govt. Bond</p>--}}
        {{--<p style="margin-bottom: 10px; font-size: 13px;">Note : Mutual funds includes only equity and balanced mutual funds.</p>--}}
        <p style="padding-bottom: 0px;"><strong>Consolidated Mutual fund Investments</strong></p>
        <table class="table" style="width: 97%;">
            <thead class="table-head">
            <tr>
                <th>Scheme Name</th>
                <th>Amount Invested</th>
                <th>Current Market Value</th>
                <th>Unrealised Profit/Loss</th>
            </tr>
            </thead>
            <tbody>
            @foreach($inv as $in)
                <tr style="padding: 0px; height: 20px!important;">
                    <td style="width: 300px;">{{$in['Scheme Name']}}</td>
                    <td style="float: right; width: 100px; text-align: right;"><?php echo $fmt->format($in['Amount Invested']); ?></td>
                    <td style="float: right; width: 150px; text-align: right;"><?php echo $fmt->format($in['Current Market Value']) ?></td>
                    <td style="float: right; width: 130px; text-align: right;"><?php echo $fmt->format($in['Unrealised Profit/Loss']) ?></td>
                </tr>
            @endforeach
            </tbody>
            <tr id="total-tr">
                <th style="text-align: left !important;">Total</th>
                <th style="text-align: right !important;"><?php echo $fmt->format($mf_inv_total); ?></th>
                <th style="text-align: right !important;"><?php echo $fmt->format($mf_current_value); ?></th>
                <th style="text-align: right !important;"><?php echo $fmt->format($mf_p_or_loss); ?></th>
            </tr>

        </table>



</main>


<div id="info-div">
    <div id="title-div">
        <p id="name" style="padding-bottom: 5px;"><strong>Consolidated Dividends Paid</strong> <span style="font-size: 12px; font-weight: normal !important;">(as on {{date('d-m-Y',strtotime($date))}})</span></p>
    </div>


</div>
<main>
    <table class="table" style="width: 100%;">
        <thead class="table-head">
        <tr>
            <th>Scheme Name</th>
            <th>Dividends Paid</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dividends as $dividend)

            <?php

            ?>
            <tr style="padding: 0px; height: 20px!important;">
                <td style="width: 300px;">{{$dividend['scheme_name']}}</td>
                <td style="float: right; width: 150px; text-align: right;"><?php echo $fmt->format($dividend['dividend_paid']) ?></td>
            </tr>
        @endforeach
        </tbody>
        <tr id="total-tr">
            <th style="text-align: left !important;">Total</th>
            <th style="text-align: right !important;"><?php echo $fmt->format(array_sum(array_column($dividends, 'dividend_paid'))); ?></th>
        </tr>

    </table>
</main>



<div id="info-div">
    <div id="title-div">
        <p id="name" style="padding-bottom: 5px;"><strong>Consolidated Bond Investments</strong>(as on {{date('d-m-Y',strtotime($date))}})</p>
    </div>
</div>


<main>
    <table class="table" style="width: 100%;">
        <thead class="table-head">
        <tr>
            <th>Bond Name</th>
            <th>Amount Invested</th>
            <th>Current Market Value</th>
            <th>Realised Profit/Loss</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $bond_total_investment = array_sum(array_column($bonds, 'investment_amount'));
        $bond_total_current = array_sum(array_column($bonds, 'current_value'));
        //                $bond_total_p_or_l = $bond_total_current - $bond_total_investment;
        $bond_total_p_or_l = array_sum(array_column($bonds, 'realised_profit'));;

        ?>
        @foreach($bonds as $in)
            <tr style="padding: 0px; height: 20px!important;">
                <td style="width: 300px;">{{$in['name']}}</td>
                <td style="float: right; width: 100px; text-align: right;"><?php echo $fmt->format($in['investment_amount']); ?></td>
                <td style="float: right; width: 150px; text-align: right;"><?php echo $fmt->format($in['current_value']) ?></td>
                <td style="float: right; width: 130px; text-align: right;"><?php echo $fmt->format($in['realised_profit']) ?></td>
            </tr>
        @endforeach
        </tbody>
        <tr id="total-tr">
            <th style="text-align: left !important;">Total</th>
            <th style="text-align: right !important;"><?php echo $fmt->format($bond_total_investment); ?></th>
            <th style="text-align: right !important;"><?php echo $fmt->format($bond_total_current); ?></th>
            <th style="text-align: right !important;"><?php echo $fmt->format($bond_total_p_or_l); ?></th>
        </tr>

    </table>
</main>




<div class="underline"></div>
<div class="underline"></div>
<footer>
    <div id="footer-view">
        <p id="footer-info">Disclaimer: Mutual Fund investments are subject to market risks, read all scheme related documents carefully before investing.</p>
    </div>
</footer>
</body>
</html>