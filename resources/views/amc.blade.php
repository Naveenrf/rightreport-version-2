<!DOCTYPE html>
<html lang="en">
<head>
  <title>RightReport</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/login-responsive.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
  <link rel="stylesheet" href="css/sector.css">
  <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
  <style type="text/css">
    .amc-wrapper{
      max-height: 70vh;
      overflow-y: scroll;
    }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid" id="navbar_container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
      <a class="navbar-brand" href="#"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
    </div>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <div class="col-xs-3 "><p class="img-circle" id = "profile_text">V</p></div>
        <div class="col-xs-2 padding-lr-zero">
          <span id = "user_name">Vasudev</span>
        </div>
        <div class="col-xs-2 text-center padding-lr-zero">
          <div class="dropdown" id="drop">
                <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
                <ul class="dropdown-menu">
                  <li><a href="#" id="settings">Setting</a></li>
                  <li><a href="#" id="logout">Logout</a></li>
                </ul>
            </div>
        </div>
      </li>
    </ul>
  </div>
</nav>

<div class = "container-fluid">

<div class="row">
  <p id="header">AMC Allocation</p>
  <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
      <div class = "col-lg-6 col-md-6 col-sm-6">
        <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="sector-text-container">
          <p class="inner-header">AMC Allocation</p>
          <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero amc-wrapper">
            <table class="table table-bordered">
            <thead>
              <tr>
                <th>AMC Name</th>
                <th>Amount Invested</th>
                <th>Current Value</th>
                <th>Percentage</th>  
              </tr>
            </thead>
            <tbody>
              @foreach($amc_arrya as $amc)
              <tr>
                <td><a href="#" class = "amcname">{{$amc['name']}}</a></td>
                <td>{{$amc['AmountInvet']}}</td>
                <td>{{$amc['CurrentVal']}}</td>
                <td>{{$amc['Avg']}}%</td>
              </tr>
              @endforeach
              <tr class="total">
                <td>Grand Total</td>
                <td>{{$grand_total['AmountInvet']}}</td>
                <td>{{$grand_total['CurrentVal']}}</td>
                <td>{{$grand_total['Avg']}}%</td>
              </tr>
            </tbody> 
          </table>
          </div>
        </div>
      </div>
            <div class = "col-lg-6 col-md-6 col-sm-6">
        <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="sector-text-container">
          <p class="inner-header">AMC Allocation</p>
          <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero amc-wrapper">
            <table class="table table-bordered">
            <thead id="inv_table_head">
              <tr>
                <th>Scheme Name</th>
                <th>PAN</th>
                <th>DOP</th>
                <th>Inv.Amount</th>
                <th>Current Value</th>  
              </tr>
            </thead>
            <tbody id="investment_table_body">


            </tbody> 
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

<script type="text/javascript">
  $('#add_scheme_btn').on('click',function(){
      $('#addSchemeModal').modal('show');
  });

  $(document).on('click','#sidebar_icon',function(){

        var img_src = $('#sidebar_icon>img').attr('src');
        if (img_src == "icons/sidebar.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar_closed.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
            $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
          });
        }

        if (img_src == "icons/sidebar_closed.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-12 col-md-12 col-sm-12');
            $('#contentbar_wrapper').addClass('col-lg-9 col-md-9 col-sm-9');            
          });
        }


    /*$('#sidebar_wrapper').toggle(500,function(){
      $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
      $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
    });*/




  })

  $(document).ready(function(){

    $('.amcname').on('click',function(){
        var name = $(this).text();
        var formData = 'name='+name;
    $.ajax({
                type: "POST",
                url: "/getAmcdetail",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async : false,
                data: formData,
                cache: false,
                processData: false,
                success: function(data) {
                    if (data['msg'] === '1') {
                      tableInsert()
                      console.log(data.investments);
                      $('#investment_table_body').empty();
                $.each(data.investments,function(key,object){

                var insert = '<tr>'+
                '<td><p class="table_data">'+object.name+'</p></td>'+
                '<td><p class="table_data">'+object.date+'</p></td>'+
                '<td><p class="table_data">'+object.pan+'</p></td>'+
                '<td><p class="table_data">'+object.invest_amount+'</p></td>'+
                '<td><p class="table_data">'+object.current_val+'</p></td>'+
              '</tr>';
                $('#investment_table_body').append(insert);

                
              });

                    var insert = '<tr class="total">'+
                    '<td>Grand Total</td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td>'+data.investamt+'</td>'+
                    '<td>'+data.CurrentVal+'</td>'+
                    '</tr>';
                    $('#investment_table_body').append(insert);

                  }
                },
                error: function(xhr, status, error) {
                    
                },
            });    
    });

    function tableInsert(){
          $('#inv_table_head').empty();


          var append_head = '<tr>'+
            '<th><p class="table_heading">Scheme Name</p></th>'+
            '<th><p class="table_heading">DOP</p></th>'+
            '<th><p class="table_heading">PAN</p></th>'+
            '<th><p class="table_heading">Inv.Amount</p></th>'+
            '<th><p class="table_heading">Current Value</p></th>'+
          '</tr>';

          $('#inv_table_head').append(append_head);
      }

  });


</script>


</body>
</html>
