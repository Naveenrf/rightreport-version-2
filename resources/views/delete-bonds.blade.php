<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login-responsive.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <script src="js/loader.js"></script>

    <style>
        .table-wrapper{
            min-height: 80vh;
        }

        #navigation .dropdown{
            display: inline-block;
            margin: 15px;
            font-size: 16px;
        }

    </style>
</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
            <a class="navbar-brand" href="/admin_home"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
            {{--            <a href="/bond_management" class="module-links">Bonds</a>--}}
            {{--            <a href="/pms_management" class="module-links">PMS</a>--}}
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <div class="col-xs-5 padding-lr-zero">
                    <span id = "user_name">{{\Auth::user()->name}}</span>
                </div>
                <div class="col-xs-2 text-center padding-lr-zero">

                    @include('layouts.admin_sidemenu')
                </div>
            </li>
        </ul>

        @include('layouts.scheme_navigation')

    </div>
</nav>

<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>

<div class = "container-fluid">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" id="contentbar_wrapper">
            <div id="contentbar">
                <div class="col-lg-12 col-md-12 " id="client_relbar">
                    <p id="client_det"><span id="client_parent">Bond Management - Delete Bond</span></p>

                    @if($errors->has())
                        <h4 style="margin-left: 15px">Investments are held by the following investors on the scheme.</h4>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <p style="margin-left: 15px">NOTE : Scheme can only be deleted if there is no investor holdings.</p>
                    @endif

                    @if(\Illuminate\Support\Facades\Session::has('message'))
                        <div class="alert alert-info alert-success alert-su">{{ Session::get('message') }}</div>
                    @endif

                </div>


                <div class="col-lg-12 col-md-12 padding-lr-zero">
                    <div class="table-wrapper" id="table-wrapper">
                        <h4 class="table-name-header purchase-header">Bonds</h4>
                        <table class="table table-bordered" id="investment_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Bond Name</p></th>
                                <th><p class="table_heading">Bond Type</p></th>
                                <th><p class="table_heading">Delete</p></th>
                                <th><p class="table_heading">Edit</p></th>
                            </tr>
                            </thead>
                            <tbody id="investment_table_body">
                            @foreach(\App\BondMaster::orderBy('name')->get() as $bond)
                                <tr>
                                    <td>{{$bond['name']}}</td>
                                    <td>{{$bond['type']}}</td>
                                    <td><a href="delete_bond/{{$bond['id']}}"><i class="material-icons">delete</i></a></td>
                                    <td data-id="{{$bond['id']}}"><i class="material-icons edit-bond">edit</i></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>

            </div>
        </div>
    </div>


</div>

<div id="userStatusModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Status</h4>
            </div>
            <div class="modal-body">
                <p id="addition_status" class="mont-reg text-center"></p>
            </div>
        </div>

    </div>
</div>

<div id="editBondModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Edit Bond</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="/edit_bond">
                    {{csrf_field()}}
                    <input type="hidden" id="edit-bond-id" name="bond_id">

                    <div class="form-group">
                        <label for="edit-bond-name" style="margin-left:50px; margin-top:30px;">Bond Name</label>
                        <input type="text" id="edit-bond-name" name="bond_name" value="" class="input-field" required>
                    </div>

                    <div class="form-group">
                        <select name="bond_type" id="bond-type" class="input-field">
                            <option value="government">Government</option>
                            <option value="corporate">Corporate</option>
                        </select>
                    </div>
                

                    <div class="form-group">
                        <input type="submit" style="display:block; margin-left: auto; margin-right: auto;" class="btn btn-primary green-btn" value="Edit">
                    </div>


                </form>
            </div>
        </div>

    </div>
</div>


<script>

    $('.edit-bond').click(function(){
        // console.log($(this).attr('data-id'));
        // console.log($(this).parent().parent().find('td:first-child')[0].innerText);
        $('#edit-bond-name').val($(this).parent().parent().find('td:first-child')[0].innerText);
        $('#edit-bond-id').val($(this).parent().data('id'));
        $('#editBondModal').modal('show');


    })

</script>

</body>
</html>