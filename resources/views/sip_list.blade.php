<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{url('css/index.css')}}">
    <link rel="stylesheet" href="{{url('css/login-responsive.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/sector.css')}}">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <style type="text/css">
        .amc-wrapper{
            max-height: 70vh;
            overflow-y: scroll;
        }

        #sip-div{
            height: 80vh;
            border-radius: 10px;
            background-color: white;
            padding: 20px;
        }

        .table{
            border-top: 1px solid gainsboro;
            margin-top: 20px;
        }
    </style>
</head>
<body>

<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "{{url('icons/sidebar.png')}}"/></a>
            <a class="navbar-brand" href="#"><img class="logo" src = "{{url('img/Right-repor-logo.svg   ')}}"/></a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            {{--<li>--}}
                {{--<div class="col-xs-3 "><p class="img-circle" id = "profile_text">V</p></div>--}}
                {{--<div class="col-xs-2 padding-lr-zero">--}}
                    {{--<span id = "user_name">Vasudev</span>--}}
                {{--</div>--}}
                {{--<div class="col-xs-2 text-center padding-lr-zero">--}}
                    {{--<div class="dropdown" id="drop">--}}
                        {{--<i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#" id="settings">Setting</a></li>--}}
                            {{--<li><a href="#" id="logout">Logout</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</li>--}}
        </ul>
    </div>
</nav>

<div class = "container-fluid">

    <div class="row">
        <p id="header">SIPs</p>
        <div class="col-md-12 col-sm-12 col-lg-12" >
            <div class="sip-details bg-white" id="sip-div">
                <h3>{{$investorDetails->member_name}}</h3>
                <p style="margin-bottom: 0px;">PAN : {{$investorDetails->member_pan}}</p>
                <p style="margin-bottom: 0px;">Email : {{$investorDetails->email}}</p>
                <p style="margin-bottom: 0px;">Contact : {{$investorDetails->contact}}</p>

                @if(count($sips) > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th><strong>Scheme Name</strong></th>
                            <th><strong>Amount</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $count = 0; ?>
                        @foreach($sips as $sip)
                            <tr>
                                <td>{{++$count}}</td>
                                <td>{{$sip->scheme_name}}</td>
                                <td>{{$sip->sip_amount}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                    @else
                    <h4 class="text-center">No SIP Registered for the Client.</h4>
                @endif
            </div>
        </div>
    </div>
</div>


</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

<script type="text/javascript">



</script>


</body>
</html>
