

<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login-responsive.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    <script src="js/admin.js"></script>
    <script src="js/loader.js"></script>
</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
            <a class="navbar-brand" href="/admin_home"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
            <a href="/scheme_management" class="module-links">Schemes</a>
            <a href="/bond_management" class="module-links">Bonds</a>
            <a href="/pms_management" class="module-links">PMS</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <div class="col-xs-5 padding-lr-zero">
                    <span id = "user_name">{{\Auth::user()->name}}</span>
                </div>
                <div class="col-xs-2 text-center padding-lr-zero">

                    @include('layouts.admin_sidemenu')

                </div>
            </li>
        </ul>
    </div>
</nav>


<div class = "container-fluid">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" id="contentbar_wrapper">
            <div id="contentbar">
                <div class="col-lg-12 col-md-12 " id="client_relbar">
                    <p id="client_det"><span id="client_parent">Group</span><span><img src="icons/arrow.png" id="arrow_img" /></span><span id="client_child">Member</span></p>
                    <p id="client-inv-details"><span id="client-amount-inv"></span><span id="client-current-val"></span>
                    </p>
                    <div id="scheme_export">
                        <button type="button" class="btn btn-primary" id="add_scheme_btn" style="display: none;">Add Scheme</button>

                        <form target="_blank" id="export_form" action="/download_investments" method="POST" style="display: inline;">
                            {{csrf_field()}}
                            <input type="hidden" name="ex_nav_date" value="" id="ex_nav_date" />
                            <input type="hidden" name="investor_id" value="" id="ex_investor_id" />
                            <input type="hidden" name="ex_download_format" value="" id="ex_download_format" />
                            <input type="hidden" name="ex_download_option" value="" id="ex_download_option" />
                            <input type="hidden" name="investor_type" value="" id="ex_investor_type" />
                            <button type="submit" class="btn btn-primary" id="export_btn">Export</button>
                        </form>

                    </div>
                    <span id="all-container">
        <span id="pan_container" style="display: none;">
              <span id="xirr">
              </span>
        </span>
      </span>
                </div>

                <div class="col-lg-12 col-md-12" id="invdetails_bar">
                    <div class="col-lg-2 col-md-2"><p class="content_header">Current Amount Invested</p><p class="amount_header">Rs. <span id="total_amount_inv">-</span></p></div>
                    <div class="col-lg-2 col-md-2"><p class="content_header">Current Market Value</p><p class="amount_header">Rs. <span id="current_market_value">-</span></p></div>
                    <div class="col-lg-2 col-md-2"><p class="content_header">Unrealised Profit/loss</p><p class="amount_header">Rs. <span id="profit_or_loss">-</span></p></div>
                    <div class="col-lg-2 col-md-2"><p class="content_header">Net Amount Invested</p><p class="amount_header">Rs. <span id="net_amount_inv">-</span></p></div>
                    <div class="col-lg-2 col-md-2"><p class="content_header">Realised Profit/Loss</p><p class="amount_header">Rs. <span id="real_p_or_l">-</span></p></div>


                    <div class="col-lg-3 col-md-3"><p class="content_header">Absolute Returns</p><p class="amount_header"><span id="absolute_ret_avg">-</span>%</p></div>

                </div>

                <div class="col-lg-12 col-md-12 padding-lr-zero">
                    <div class="table-wrapper" id="table-wrapper">
                        <h4 class="table-name-header purchase-header">Mutual Fund Investments</h4>
                        <table class="table table-bordered" id="investment_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Scheme Type</p></th>
                                <th><p class="table_heading">Date of Purchase</p></th>
                                <th><p class="table_heading">Amt. Invested</p></th>
                                <th><p class="table_heading">Purchase NAV</p></th>
                                <th><p class="table_heading">Units</p></th>
                                <th><p class="table_heading">Current Nav.</p></th>
                                <th><p class="table_heading">Current Mrkt Val.</p></th>
                                <th><p class="table_heading">Unrealised Profit/Loss</p></th>
                                <th><p class="table_heading">Absolute Returns(%)</p></th>
                                <th><p class="table_heading">Annualised Returns</p></th>
                                <th><p class="table_heading">Folio Number</p></th>
                            </tr>
                            </thead>
                            <tbody id="investment_table_body">


                            </tbody>
                        </table>



                        <h4 class="table-name-header purchase-header" style="background-color:#036cf44d">Systematic Investment Plan</h4>
                        <table class="table table-bordered" id="sip_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Scheme Type</p></th>
                                <th><p class="table_heading">Date of Purchase</p></th>
                                <th><p class="table_heading">Amt. Invested</p></th>
                                <th><p class="table_heading">Purchase NAV</p></th>
                                <th><p class="table_heading">Units</p></th>
                                <th><p class="table_heading">Current Nav.</p></th>
                                <th><p class="table_heading">Current Mrkt Val.</p></th>
                                <th><p class="table_heading">Unrealised Profit/Loss</p></th>
                                <th><p class="table_heading">Absolute Returns(%)</p></th>
                                <th><p class="table_heading">Annualised Returns</p></th>
                                <th><p class="table_heading">Folio Number</p></th>
                            </tr>
                            </thead>
                            <tbody id="sip_table_body">


                            </tbody>
                        </table>


                        <h4 class="table-name-header purchase-header" style="background-color: #00bcd480">Liquid Investments</h4>
                        <table class="table table-bordered" id="liquid_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Scheme Type</p></th>
                                <th><p class="table_heading">Date of Purchase</p></th>
                                <th><p class="table_heading">Amt. Invested</p></th>
                                <th><p class="table_heading">Purchase NAV</p></th>
                                <th><p class="table_heading">Units</p></th>
                                <th><p class="table_heading">Current Nav.</p></th>
                                <th><p class="table_heading">Current Mrkt Val.</p></th>
                                <th><p class="table_heading">Unrealised Profit/Loss</p></th>
                                <th><p class="table_heading">Absolute Returns(%)</p></th>
                                <th><p class="table_heading">Annualised Returns</p></th>
                                <th><p class="table_heading">Folio Number</p></th>
                                <th><p class="table_heading">Trail Fee(%)</p></th>
                                <th><p class="table_heading">Upfront Fee(%)</p></th>
                                <th><p class="table_heading">Last Updated</p></th>
                            </tr>
                            </thead>
                            <tbody id="liquid_table_body">


                            </tbody>
                        </table>



                        <h4 class="table-name-header redeem-header">Redemption Detail</h4>
                        <table class="table table-bordered" id="withdraw_table">
                            <thead id="wd_table_head">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Folio Number</p></th>
                                <th><p class="table_heading">Date of Purchase</p></th>
                                <th><p class="table_heading">Date of Withdraw</p></th>
                                <th><p class="table_heading">Amt. Invested</p></th>
                                <th><p class="table_heading">Purchase NAV</p></th>
                                <th><p class="table_heading">Units</p></th>
                                <th><p class="table_heading">Redemption Nav.</p></th>
                                <th><p class="table_heading">Withdraw Mrkt Val.</p></th>
                                <th><p class="table_heading">Realised Profit/Loss</p></th>
                                <th><p class="table_heading">Exit Load</p></th>
                                <th><p class="table_heading">STT</p></th>
                                <th><p class="table_heading">Absolute Returns(%)</p></th>

                            </tr>
                            </thead>
                            <tbody id="withdraw_table_body">


                            </tbody>
                        </table>

                        <h4 class="table-name-header dividend-header">Dividend Detail</h4>
                        <table class="table table-bordered" id="dividend_table">
                            <thead id="div_thead">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Date of Purchase</p></th>
                                <th><p class="table_heading">Amt. Inveated</p></th>
                                <th><p class="table_heading">Dividend Date</p></th>
                                <th><p class="table_heading">Dividend per Unit</p></th>
                                <th><p class="table_heading">Divident Amount</p></th>
                            </tr>
                            </thead>
                            <tbody id="dividend_tbody">

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>



<div id="addNewSchemeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add New Scheme</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="add_new_scheme_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" class="mont-reg" name="new_scheme_name" id = "new_scheme_name" required placeholder="Scheme Name">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="scheme_code" id = "scheme_code" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="scheme_type" id = "scheme_type" required>
{{--                            @foreach($scheme_types as $schemes)--}}
{{--                                <option value="{{$schemes['id']}}">{{$schemes['scheme_sub']}}</option>--}}
{{--                            @endforeach--}}
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="amc_name" id = "amc_name" required>
{{--                            @foreach($amc_names as $amc)--}}
{{--                                <option value="{{$amc['id']}}">{{$amc['name']}}</option>--}}
{{--                            @endforeach--}}
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="file" class="mont-reg" name="scheme_file" id = "scheme_file" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_user_btn" id = "add_user_btn" class="btn btn-primary blue-btn center-block" value="Add Scheme">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>


</body>
</html>

