<!DOCTYPE html>
<html lang="en">
<head>
  <title>RightReport</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <link rel="stylesheet" href="{{url('css/index.css')}}">
  <link rel="stylesheet" href="{{url('css/login-responsive.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
  <link rel="stylesheet" href="{{url('css/sector.css')}}">
  <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid" id="navbar_container">
    <div class="navbar-header">
      <!-- <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a> -->
      <a class="navbar-brand" href="/"><img class="logo" src = "{{url('img/Right-repor-logo.svg')}}"/></a>
    </div>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <div class="col-xs-3 "><p class="img-circle" id = "profile_text">V</p></div>
        <div class="col-xs-5 padding-lr-zero">
          <span id = "user_name">{{\Auth::user()->name}}</span>
        </div>
        <div class="col-xs-2 text-center padding-lr-zero">
          <div class="dropdown" id="drop">
                <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
                <ul class="dropdown-menu">
                  <li><a href="#" id="settings">Setting</a></li>
                  <li><a href="{{url('/logout')}}" id="logout">Logout</a></li>
                  <!-- <li><a id="add_new_scheme" id="add_new_scheme">Add Scheme</a></li> -->
                </ul>
            </div>
        </div>
      </li>
    </ul>
  </div>
</nav>

<div class = "container-fluid">

<div class="row">
  <p id="header">Sector Allocation</p>
  <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
    <div class = "col-lg-6 col-md-6 col-sm-6">
      <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="sector-text-container">
        <p class="inner-header">Sector Allocation</p>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Sector Name</th>
              <th>Current Value</th>
              <th>Percentage</th>  
            </tr>
          </thead>
          <tbody>
            @foreach($sector_array as $val)
            <tr>
              <td>{{$val[0]}}</td>
              <td>{{$val[1]}}</td>
              <td>{{$val[2]}}%</td>
            </tr>
            @endforeach
            <tr class="total">
              <td>Grand Total</td>
              <td>{{$total}}</td>
              <td>100%</td>
            </tr>
          </tbody> 
        </table>
      </div>
    </div>
    <div class = "col-lg-6 col-md-6 col-sm-6">
      <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="sector-chart">
      <p class="inner-header">Sector Allocation Chart</p>
        <div class="sector-chart-container">
          <canvas id="sector-pie-chart">
            
          </canvas>
        </div>
      </div>
      <div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero" id="category-alloc">
      <p class="inner-header">Category Allocation</p>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Sector Name</th>
              <th>Current Value</th>
              <th>Percentage</th>  
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Equity</td>
              <td>{{$equity}}</td>
              <td>{{$equity_avg}}</td>
            </tr>
            <tr>
              <td>Debt</td>
              <td>{{$debt}}</td>
              <td>{{$debt_avg}}</td>
            </tr>
{{--            <tr>--}}
{{--              <td>Balanced</td>--}}
{{--              <td>{{$balanced}}</td>--}}
{{--              <td>{{$balanced_avg}}</td>--}}
{{--            </tr>--}}
            <tr class="total">
              <td>Grand Total</td>
              <td>{{$total}}</td>
              <td>100%</td>
            </tr>
          </tbody> 
        </table>
      </div>
    </div>
  </div>
</div>


</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

<script type="text/javascript">
  $('#add_scheme_btn').on('click',function(){
      $('#addSchemeModal').modal('show');
  });

  // $(document).on('click','#sidebar_icon',function(){

  //       var img_src = $('#sidebar_icon>img').attr('src');
  //       if (img_src == "icons/sidebar.png") {
  //         $('#sidebar_icon>img').attr('src','icons/sidebar_closed.png');
  //         $('#sidebar_wrapper').toggle(250,function(){
  //           $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
  //           $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
  //         });
  //       }

  //       if (img_src == "icons/sidebar_closed.png") {
  //         $('#sidebar_icon>img').attr('src','icons/sidebar.png');
  //         $('#sidebar_wrapper').toggle(250,function(){
  //           $('#contentbar_wrapper').removeClass('col-lg-12 col-md-12 col-sm-12');
  //           $('#contentbar_wrapper').addClass('col-lg-9 col-md-9 col-sm-9');            
  //         });
  //       }


  //   /*$('#sidebar_wrapper').toggle(500,function(){
  //     $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
  //     $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
  //   });*/




  // })

  $(document).ready(function(){
    var temp = [];
    var ctx1 = document.getElementById("sector-pie-chart");
    var data1 = {

        labels: ["{{$sector_array[0][0]}} ({{$sector_array[0][2]}}%)", "{{$sector_array[1][0]}} ({{$sector_array[1][2]}}%)", "{{$sector_array[2][0]}} ({{$sector_array[2][2]}}%)", "{{$sector_array[3][0]}} ({{$sector_array[3][2]}}%)", "{{$sector_array[4][0]}} ({{$sector_array[4][2]}}%)","{{$sector_array[5][0]}} ({{$sector_array[5][2]}}%)","{{$sector_array[7][0]}} ({{$sector_array[7][2]}}%)","{{$sector_array[8][0]}} ({{$sector_array[8][2]}}%)","{{$sector_array[6][0]}} ({{$sector_array[6][2]}}%)"],
        datasets: [
            {
                label: "TeamA Score",
                data: ["{{$sector_array[0][2]}}", "{{$sector_array[1][2]}}", "{{$sector_array[2][2]}}", "{{$sector_array[3][2]}}", "{{$sector_array[4][2]}}","{{$sector_array[5][2]}}","{{$sector_array[7][2]}}","{{$sector_array[8][2]}}","{{$sector_array[6][2]}}"],
                backgroundColor: [
                    "#36A2EB",
                    "#FF6384",
                    "#FF9F40",
                    "#FFCD56",
                    "#4BC0C0",
                    "#edff00",
                    "#00ffa3",
                    "#f53b5c",
                    "#b73bf5",
                ],
                borderColor: [
                    "#CDA776",
                    "#FF6384",
                    "#FF9F40",
                    "#FFCD56",
                    "#4BC0C0",
                    "#edff00",
                    "#00ffa3",
                    "#f53b5c",
                    "#b73bf5",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    var options = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            text: "",
            fontSize: 18,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "right",
            labels: {
                fontColor: "#333",
                fontSize: 18
            },
         tooltips: {
            titleFontSize: 50
            }
        }
    };

    var chart1 = new Chart(ctx1, {
        type: "pie",
        data: data1,
        options: options
    });

    Chart.NewLegend = Chart.Legend.extend({
      afterFit: function() {
        this.height = this.height + 100;
      },
    });
  });


</script>


</body>
</html>
