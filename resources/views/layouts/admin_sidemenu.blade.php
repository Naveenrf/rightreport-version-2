<div class="dropdown" id="drop">
    <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
    <ul class="dropdown-menu">
        <li><a href="#" id="settings">Setting</a></li>
        <li><a href="{{url('/logout')}}" id="logout">Logout</a></li>
        <li><a href="scheme_management">I Manager</a></li>
        <li><a href="/password_management">PWD Manag.</a></li>
        <li><a href ="user_management" >User Manag.</a></li>
        <li><a href ="show_aum">AUM Manag.</a></li>
        <li><a href ="total_transaction">Total Tran.</a></li>
        <li><a id="access">Access</a></li>
    </ul>
</div>