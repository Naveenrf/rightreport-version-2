<div id="navigation">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Schemes <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="/scheme_management">Schemes</a></li>
            <li><a href="/show_schemes">Manage Schemes</a></li>
        </ul>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Bonds <span class="caret"></span></a>
        <ul class="dropdown-menu">
            <li><a href="/bond_management">Bonds</a></li>
            <li><a href="/show_bonds">Manage Bonds</a></li>
        </ul>
    </li>

{{--    <li class="dropdown">--}}
{{--        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PMS <span class="caret"></span></a>--}}
{{--        <ul class="dropdown-menu">--}}
{{--            <li><a href="/pms_management">PMS</a></li>--}}
{{--            <li><a href="/show_pms">Delete PMS</a></li>--}}
{{--        </ul>--}}
{{--    </li>--}}
</div>