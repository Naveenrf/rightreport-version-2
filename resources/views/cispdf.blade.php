
<!DOCTYPE html>
<html>
<head>
    <title>CIS PDF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    {{--<link rel="stylesheet" href="css/bootstrap.min.css">--}}
    <link rel="stylesheet" href="css/cispdf.css">

    <style>
        table{
            border-collapse: collapse;
        }

        body{
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        #address-div p{
            margin-bottom: 0px !important;
            padding-bottom: 0px;
        }
    </style>
</head>
<body>
<header>
    <div id="header-view">
        <img class="logo" style="margin-top: 10px;" src = "icons/pro_logo.png"/>
        <div id="address-div">
            <p class="m-b-0">Level 1, No 1, Balaji First Avenue,</p>
            <p class="m-b-0"  style="margin-top: 2px;">T.Nagar,</p>
            <p class="m-b-0" style="margin-top: 2px;">Chennai - 600017</p>
            <p class="m-b-0" style="margin-top: 2px;">Ph: +91 8825888200</p>

        </div>
        
    </div>
</header>
<div id="info-div">
    <div id="title-div">
        <p id="name" style="padding-bottom: 0px; margin-top: 0px;"><strong>Investment Statement </strong><span style="font-size: 12px; font-weight: normal !important;">(as on {{date('d-m-Y',strtotime($date))}})</span> </p>
    </div>

    <div id="user-info">
        <div id="user-info-one">
            <p class="m-b-0" style="margin-top: 5px;"><strong>Name of Investor       : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$name}}</span></p>
            <p class="m-b-0" style="margin-top: 5px;"><strong>Address of Investor    : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$address}}</span></p>
        </div>
        <div id="user-info-two">
            <p class="m-b-0" style="margin-top: 5px;"><strong>E-mail ID      : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$email}}</span></p>
            <p class="m-b-0" style="margin-top: 5px;"><strong>Contact Number : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$contact}}</span></p>
            <p class="m-b-0" style="margin-top: 5px;"><strong>PAN Number     : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$pan}}</span></p>
        </div>
    </div>
    <?php


        $net_amount_invested = 0;

//        dd($pms_inv);
    $sip_inv_total = 0;
    $sip_current_total = 0;
    $one_time_inv_total = 0;
    $one_time_current_total = 0;
    $liquid_inv_total = 0;
    $liquid_current_total = 0;
    $fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
    $unreal_profit_or_loss = 0;
    $dividend_total = 0;

    //    $wd_key = 'Realised Profit/Loss';
    $realised_profit_or_loss = array_sum(array_column($withdraw,'Realised Profit/Loss'));

//    dd($pms_inv, $bond_inv);



    foreach ($dividends as $dividend){
        $realised_profit_or_loss += $dividend['dividend_amount'];
    }

    foreach($bond_redemptions as $br){
        // dd($br);
        $realised_profit_or_loss += $br['realised_profit'];
        
    }

    //    $inv_key = 'Unrealised Profit/Loss';
    $unreal_profit_or_loss = array_sum(array_column($inv,'Unrealised Profit/Loss'));

    $current_market_value = array_sum(array_column($inv,'Current Market Value'));

    foreach ($pms_inv as $pms){
        $current_market_value += $pms->current_value ;
        $unreal_profit_or_loss += $pms->total_gain;
    }

    foreach ($bond_inv as $bond){
        $realised_profit_or_loss += $bond['total_interest_paid'];
        $current_market_value += ($bond->amount_invested + $bond->interest_accumalated) ;
        $unreal_profit_or_loss += $bond->interest_accumalated;
    }



    if($realised_profit_or_loss < 0){

        if($unreal_profit_or_loss > 0){
            $net_amount_invested = ($current_market_value - $unreal_profit_or_loss) + abs($realised_profit_or_loss);
//            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
        }

    }


    if($realised_profit_or_loss > 0){

        if($unreal_profit_or_loss < 0){
            $net_amount_invested = ($current_market_value + abs($unreal_profit_or_loss)) - $realised_profit_or_loss;
//            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
        }

    }

    if($realised_profit_or_loss > 0){

        if($unreal_profit_or_loss > 0){
            $net_amount_invested = $current_market_value - (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss));
//            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
        }

    }


    if($realised_profit_or_loss < 0){

        if($unreal_profit_or_loss < 0){
            $net_amount_invested = $current_market_value + (abs($unreal_profit_or_loss) + abs($realised_profit_or_loss));
//            dd($unreal_profit_or_loss, $realised_profit_or_loss, $current_market_value);
        }

    }

    if($realised_profit_or_loss == 0){
        if($unreal_profit_or_loss > 0){
            $net_amount_invested = $current_market_value - $unreal_profit_or_loss;
        }
    }

    if($realised_profit_or_loss == 0){
        if($unreal_profit_or_loss < 0){
            $net_amount_invested = $current_market_value + (abs($unreal_profit_or_loss));
        }
    }









//    dd($current_market_value, abs($unreal_profit_or_loss), abs($realised_profit_or_loss));




    ?>
</div>
<main>

    <?php if ($inv['one_time'] > 0): ?>
    <p>
        <strong>MUTUAL FUND INVESTMENTS</strong>
    </p>
    <table class="table table-bordered" style="width: 97%;">
        <thead class="table-head">
        <tr>
            <th>Scheme Name</th>
            <th>Folio Number</th>
            <th>Purchase date</th>
            <th>Amount Invested</th>
            <th>Purchase NAV</th>
            <th>Units</th>
            <th>Current NAV</th>
            <th>Current Market Value</th>
            <th>Unrealised Profit/Loss</th>
            <th>Absolute Returns</th>
            <th>Annualised Returns</th>
        </tr>
        </thead>
        <tbody>


        @foreach($inv as $in)
            @if($in['invest_type'] == '1')

                <?php
                $one_time_current_total += $in['Current Market Value'];
                $one_time_inv_total += $in['Amount Invested'];
                //                $unreal_profit_or_loss += round($in['Unrealised Profit/Loss'],2);

                //                      dd($one_time_inv_total);
                ?>
                <tr>
                    <td style="width: 250px;">{{$in['Scheme Name']}}</td>
                    <td style="width: 80px;">{{$in['Folio Number']}}</td>
                    <td style="width: 70px;">{{$in['Purchase date']}}</td>
                    <td>{{number_format($in['Amount Invested'], 2)}}</td>
                    <td style="width: 50px;">{{number_format($in['Purchase NAV'], 4)}}</td>
                    <td>{{number_format($in['Units'], 4)}}</td>
                    <td style="width: 50px;">{{number_format($in['Current NAV'], 4)}}</td>
                    <td><?php echo $fmt->format($in['Current Market Value']); ?></td>
                    <td><?php echo $fmt->format($in['Unrealised Profit/Loss']); ?></td>
                    <td><?php echo number_format($in['Absolute Returns'], 2); ?></td>
                    <td><?php echo number_format($in['Annualised Returns'], 2); ?></td>
                </tr>
            @endif
        @endforeach
        </tbody>
        @if($one_time_inv_total > 0)
            <tr id="total-tr">
                <th style="text-align: left !important;">Total</th>
                <th></th>
                <th></th>
                <th><?php echo $fmt->format($one_time_inv_total); ?></th>
                <th></th>
                <th></th>
                <th></th>
                <th style="text-align: right;"><?php echo $fmt->format($one_time_current_total); ?></th>
                <th style="text-align: right; width: 80px;"><?php echo $fmt->format($one_time_current_total - $one_time_inv_total); ?></th>
                <th style="text-align: right;"><?php echo number_format((($one_time_current_total - $one_time_inv_total)/$one_time_inv_total)*100, 2); ?></th>
                <th></th>
            </tr>
        @endif
    </table>
        @else
            <p>
                <strong>MUTUAL FUND INVESTMENTS</strong>
            </p>
            <table class="table" style="width: 100%;">
                <thead class="table-head">
                <tr>
                    <th>Scheme Name</th>
                    <th>Amount Invested</th>
                    <th>Current Market Value</th>
                    <th>Unrealised Profit/Loss</th>
                </tr>
                </thead>
                <tbody>
                <tr id="total-tr">
                    <td style="width: 200px">Total</td>
                    <td style="width: 150px">0</td>
                    <td style="width: 50px">0</td>
                    <td style="width: 50px">0</td>
                </tr>
                </tbody>
            </table>
    <?php endif ?>



    <?php if ($inv['sip'] > 0): ?>
    <p>
        <strong>SYSTEMATIC INVESTMENT PLAN</strong>
    </p>
    <table class="table table-bordered">
        <thead class="table-head">
        <tr>
            <th>Scheme Name</th>
            <th>Folio Number</th>
            <th>Purchase date</th>
            <th>Amount Invested</th>
            <th>Purchase NAV</th>
            <th>Units</th>
            <th>Current NAV</th>
            <th>Current Market Value</th>
            <th>Unrealised Profit/Loss</th>
            <th>Absolute Returns</th>
            <th>Annualised Returns</th>
        </tr>
        </thead>
        <tbody>
        @foreach($inv as $in)
            @if($in['invest_type'] == '2')
                <?php
                $sip_current_total += $in['Current Market Value'];
                $sip_inv_total += $in['Amount Invested'];
                //                $unreal_profit_or_loss += round($in['Unrealised Profit/Loss'], 2);

                ?>
                <tr>
                    <td style="width: 250px;">{{$in['Scheme Name']}}</td>
                    <td style="width: 80px;">{{$in['Folio Number']}}</td>
                    <td style="width: 70px;">{{$in['Purchase date']}}</td>
                    <td style="width: 60px;">{{number_format($in['Amount Invested'], 2)}}</td>
                    <td style="width: 50px;">{{number_format($in['Purchase NAV'], 4)}}</td>
                    <td>{{number_format($in['Units'], 4)}}</td>
                    <td style="width: 50px;">{{number_format($in['Current NAV'], 4)}}</td>
                    <td style="width: 70px;">{{number_format($in['Current Market Value'], 2)}}</td>
                    <td>{{number_format($in['Unrealised Profit/Loss'], 2)}}</td>
                    <td>{{number_format($in['Absolute Returns'], 2)}}</td>
                    <td>{{number_format($in['Annualised Returns'], 2)}}</td>
                </tr>
            @endif
        @endforeach
        </tbody>
        @if($sip_inv_total > 0)
            <tr id="total-tr">
                <th style="text-align: left !important;">Total</th>
                <th></th>
                <th></th>
                <th><?php echo $fmt->format($sip_inv_total); ?></th>
                <th></th>
                <th></th>
                <th></th>
                <th style="text-align: right;"><?php echo $fmt->format($sip_current_total) ?></th>
                <th style="text-align: right; width: 80px;"><?php echo $fmt->format($sip_current_total - $sip_inv_total); ?></th>
                <th style="text-align: right;"><?php echo number_format((($sip_current_total - $sip_inv_total)/$sip_inv_total)*100, 2); ?></th>
                <th></th>
            </tr>
        @endif
    </table>

        @else
            <p>
                <strong>SYSTEMATIC INVESTMENT PLAN</strong>
            </p>
                <table class="table" style="width: 100%;">
                    <thead class="table-head">
                    <tr>
                        <th>Scheme Name</th>
                        <th>Amount Invested</th>
                        <th>Current Market Value</th>
                        <th>Unrealised Profit/Loss</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr id="total-tr">
                        <td style="width: 200px">Total</td>
                        <td style="width: 150px">0</td>
                        <td style="width: 50px">0</td>
                        <td style="width: 50px">0</td>
                    </tr>
                    </tbody>
                </table>
    <?php endif ?>


    <?php if ($inv['liquid'] > 0): ?>
        <p><strong>LIQUID FUND</strong></p>
        <table class="table table-bordered">
            <thead class="table-head">
            <tr>
                <th>Scheme Name</th>
                <th>Folio Number</th>
                <th>Purchase date</th>
                <th>Amount Invested</th>
                <th>Purchase NAV</th>
                <th>Units</th>
                <th>Current NAV</th>
                <th>Current Market Value</th>
                <th>Unrealised Profit/Loss</th>
                <th>Absolute Returns</th>
                <th>Annualised Returns</th>
            </tr>
            </thead>
            <tbody>
            @foreach($inv as $in)
                @if($in['invest_type'] == '3')
                    <?php
                    $liquid_current_total += $in['Current Market Value'];
                    $liquid_inv_total += $in['Amount Invested'];
                    //                $unreal_profit_or_loss += round($in['Unrealised Profit/Loss'],2);


                    ?>
                    <tr>
                        <td style="width: 250px;">{{$in['Scheme Name']}}</td>
                        <td style="width: 80px;">{{$in['Folio Number']}}</td>
                        <td style="width: 70px;">{{$in['Purchase date']}}</td>
                        <td style="width: 60px;">{{number_format($in['Amount Invested'], 2)}}</td>
                        <td style="width: 50px;">{{number_format($in['Purchase NAV'], 4)}}</td>
                        <td>{{number_format($in['Units'], 4)}}</td>
                        <td style="width: 50px;">{{number_format($in['Current NAV'], 4)}}</td>
                        <td style="width: 70px;">{{number_format($in['Current Market Value'], 2)}}</td>
                        <td>{{number_format($in['Unrealised Profit/Loss'], 2)}}</td>
                        <td>{{number_format($in['Absolute Returns'], 2)}}</td>
                        <td>{{$in['Annualised Returns']}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
            @if($liquid_inv_total > 0)
                <tr id="total-tr">
                    <th style="text-align: left !important;">Total</th>
                    <th></th>
                    <th></th>
                    <th><?php echo $fmt->format($liquid_inv_total); ?></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th style="text-align: right;"><?php echo $fmt->format($liquid_current_total); ?></th>
                    <th style="text-align: right;  width: 80px;"><?php echo $fmt->format($liquid_current_total - $liquid_inv_total); ?></th>
                    <th style="text-align: right;"><?php echo number_format((($liquid_current_total - $liquid_inv_total)/$liquid_inv_total)*100, 2); ?></th>
                    <th></th>
                </tr>
            @endif
        </table>

                @else
                    <p>
                        <strong>LIQUID FUND</strong>
                    </p>
                    <table class="table" style="width: 100%;">
                        <thead class="table-head">
                        <tr>
                            <th>Scheme Name</th>
                            <th>Amount Invested</th>
                            <th>Current Market Value</th>
                            <th>Unrealised Profit/Loss</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="total-tr">
                            <td style="width: 200px">Total</td>
                            <td style="width: 150px">0</td>
                            <td style="width: 50px">0</td>
                            <td style="width: 50px">0</td>
                        </tr>
                        </tbody>
                    </table>
        <?php endif ?>



        <?php if (count($dividends) > 0): ?>
        <p><strong>DIVIDENDS</strong></p>
        <table class="table table-bordered">
            <thead class="table-head">
            <tr>
                <th>Scheme Name</th>
                <th>Amount Invested</th>
                <th>Investment Date</th>
                <th>Dividend Date</th>
                <th>Dividend Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dividends as $dividend)

                    <?php
                    $dividend_total += $dividend['dividend_amount'];
                    //                $unreal_profit_or_loss += round($in['Unrealised Profit/Loss'],2);


                    ?>
                    <tr>
                        <td style="width: 250px;">{{$dividend['scheme_name']}}</td>
                        <td style="width: 80px;">{{$fmt->format($dividend['investment_amount'])}}</td>
                        <td style="width: 70px;">{{date('d-M-Y', strtotime($dividend['investment_date']))}}</td>
                        <td style="width: 60px;">{{date('d-m-Y', strtotime($dividend['dividend_date']))}}</td>
                        <td style="width: 50px;">{{$fmt->format($dividend['dividend_amount'])}}</td>
                    </tr>
            @endforeach
            </tbody>
            @if($dividend_total > 0)
                <tr id="total-tr">
                    <th style="text-align: left !important;">Total</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><?php echo $fmt->format($dividend_total); ?></th>

                </tr>
            @endif
        </table>

        @else
            {{--<p>--}}
                {{--<strong>LIQUID FUND</strong>--}}
            {{--</p>--}}
            {{--<p style="margin-top: 15px; margin-bottom: 15px;">No Investment in Liquid Funds</p>--}}
            <?php endif ?>



            <?php if (count($bond_inv) > 0): ?>
            <p>
                <strong>BOND INVESTMENT</strong>
            </p>
            <table class="table table-bordered">
                <thead class="table-head">
                <tr>
                    <th>Bond Name</th>
                    <th>Investment Amount</th>
                    <th>Date of Issue</th>
                    <th>Bond term</th>
                    <th>Payout Type</th>
                    <!-- <th>Interest Freq.</th> -->
                    <th>Interest Rate</th>
                    <th>Interest Accumalated</th>
                    <th>Interest Paid</th>
                    <th>Bond Receipt</th>
                    <th>Credit Rating</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bond_inv as $in)

                    <tr>
                        <td style="width: 250px;">{{$in['name']}}</td>
                        <td style="width: 80px;">{{number_format($in['amount_invested'], 2)}}</td>
                        <td style="width: 70px;">{{date('d-M-Y', strtotime($in['date_of_issue']))}}</td>
                        <td style="width: 60px;">{{$in['bond_term']}} Years</td>
                        <td style="width: 50px;">{{$in['bond_payout_type']}}</td>
                        <!-- <td>{{$in['interest_frequency']}}</td> -->
                        <td style="width: 50px;">{{$in['interest_rate']}}%</td>
                        <td style="width: 70px;">{{number_format($in['interest_accumalated'], 2)}}</td>
                        <td>{{number_format($in['total_interest_paid'], 2)}}</td>
                        <td>{{$in['bond_receipt']}}</td>
                        <td>{{$in['credit_rating']}}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>


            @if(count($bond_interest) > 0)
                <p>
                    <strong>BOND INTEREST DETAILS</strong>
                </p>
                <table class="table table-bordered">
                    <thead class="table-head">
                    <tr>
                        <th>Bond Name</th>
                        <th>Investment Amount</th>
                        <th style="width: 100px;">Date of Issue</th>
                        <!-- <th>Interest Freq.</th> -->
                        <th style="width: 100px;">Interest Rate</th>
                        <th>Interest Paid</th>
                        <th style="width: 100px;">Bond Receipt</th>
                        <th>Interest Payout Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bond_interest as $in)

                        <tr>
                            <td style="width: 250px;">{{$in['bond_name']}}</td>
                            <td style="width: 80px;">{{number_format($in['investment_amount'], 2)}}</td>
                            <td style="width: 70px;">{{date('d-M-Y', strtotime($in['date_of_issue']))}}</td>
                            <!-- <td style="width: 50px;">{{$in['interest_frequency']}}</td> -->
                            <td>{{$in['interest_rate']}}%</td>
                            <td style="width: 70px;">{{number_format($in['interest_paid'], 2)}}</td>
                            <td>{{$in['bond_receipt']}}</td>
                            <td>{{$in['interest_payout_date']}}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            @endif

            @else
                <p>
                    <strong>Bond Investments</strong>
                </p>
                <table class="table" style="width: 100%;">
                    <thead class="table-head">
                    <tr>
                        <th>Bond Name</th>
                        <th>Amount Invested</th>
                        <th>Current Market Value</th>
                        <th>Realised Profit/Loss</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr id="total-tr">
                            <td style="width: 200px">Total</td>
                            <td style="width: 150px">0</td>
                            <td style="width: 50px">0</td>
                            <td style="width: 50px">0</td>
                        </tr>
                    </tbody>
                </table>

                <?php endif ?>








        <div id="title-div">
            <p id="name" style="padding-bottom: 5px;"><strong>Net Profit </strong><span style="font-size: 12px; font-weight: normal !important;">(as on {{$date}})</span> </p>
        </div>

        <?php $net_profit = $unreal_profit_or_loss + $realised_profit_or_loss; ?>
            <table class="table table-bordered" style="width: 50%">
                <tbody>

                <tr>
                    <td style="width: 33%; text-align: center;">Current Market Value</td>
                    <td style="width: 33%; text-align: center;"><?php echo $fmt->format($current_market_value); ?></td>
                </tr>

                <tr>
                    <td style="width: 33%; text-align: center;">Net Amount Invested</td>
                    <td style="width: 33%; text-align: center;"><?php echo $fmt->format($net_amount_invested); ?></td>
                </tr>
                <tr>
                    <td style="width: 33%; text-align: center;">Unrealised Profit/Loss</td>
                    <td style="width: 33%; text-align: center;"><?php echo $fmt->format($unreal_profit_or_loss); ?></td>
                </tr>
                <tr>
                    <td style="width: 33%; text-align: center;">Realised Profit/Loss</td>
                    <td style="width: 33%; text-align: center;"><?php echo $fmt->format($realised_profit_or_loss); ?></td>
                </tr>
                <tr id="total-tr">
                    <td style="width: 33%; text-align: center;"><strong>NetProfit/Loss <p>(Realised + Unrealised)</p></strong></td>
                    <td style="width: 33%; text-align: center;"><strong><?php echo $fmt->format($net_profit); ?></strong></td>
                </tr>


                </tbody>
            </table>
            <div class="underline"></div>
            <div class="underline"></div>
        <?php if (count($withdraw) > 0): ?>

            @if($download_option == 'with_redemption')
            <div id="title-div">
                <p id="name" style="padding-bottom: 5px;"><strong>Redemption Statement </strong><span style="font-size: 12px; font-weight: normal !important;">(as on {{$date}})</span> </p>
            </div>

            <p>
                <strong>MUTUAL FUND REDEMPTIONS</strong>
            </p>
            <table class="table table-bordered">
                <thead class="table-head">
                <tr>
                    <th>Scheme Name</th>
                    <th>Folio Number</th>
                    <th style="width: 80px;">Purchase date</th>
                    <th>Amount Invested</th>
                    <th>Purchase NAV</th>
                    <th>Units</th>
                    <th>Redemption NAV</th>
                    <th style="width: 80px;">Redemption Date</th>
                    <th>Withdraw Value</th>
                    <th>STT</th>
                    <th>Realised Profit/Loss</th>

                </tr>
                </thead>
                <tbody>
                @foreach($withdraw as $in)
                    <tr>
                        <td style="width: 200px;">{{$in['Scheme Name']}}</td>
                        <td>{{$in['Folio Number']}}</td>
                        <td>{{date('d-M-Y', strtotime($in['Purchase Date']))}}</td>
                        <td>{{number_format($in['Amount Invested'], 2)}}</td>
                        <td>{{number_format($in['Purchase NAV'], 4)}}</td>
                        <td>{{number_format($in['Units'], 4)}}</td>
                        <td>{{number_format($in['Redemption NAV'], 4)}}</td>
                        <td>{{date('d-M-Y', strtotime($in['Redemption Date']))}}</td>
                        <td>{{number_format($in['Withdraw Value'], 2)}}</td>
                        <td>{{number_format($in['STT'], 2)}}</td>
                        <td>{{number_format($in['Realised Profit/Loss'], 2)}}</td>

                    </tr>

                @endforeach
                </tbody>
            </table>

           
        <?php endif ?>
            <!-- Bond Redemption Below -->
            @endif

            @if(count($bond_redemptions) > 0)
                <p>
                    <strong>BOND REDEMPTIONS</strong>
                </p>
                <table class="table table-bordered">
                    <thead class="table-head">
                    <tr>
                        <th>Bond Name</th>
                        <th>Investment Amount</th>
                        <th>Date of Issue</th>
                        <th>Bond term</th>
                        <th>Payout Type</th>
                        <!-- <th>Interest Freq.</th> -->
                        <th>Interest Rate</th>
                        <th>Realised Profit</th>
                        <th>Bond Receipt</th>
                        <th>Credit Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bond_redemptions as $bond_redemption)
                        <?php //dd($bond_redemption->bonds['name']); ?>
                        <tr>
                            <td style="width: 250px;">{{$bond_redemption->bonds['name']}}</td>
                            <td style="width: 80px;">{{number_format($bond_redemption->bonds['amount_invested'], 2)}}</td>
                            <td style="width: 70px;">{{date('d-M-Y', strtotime($bond_redemption->bonds['date_of_issue']))}}</td>
                            <td style="width: 60px;">{{$bond_redemption->bonds['bond_term']}} Years</td>
                            <td style="width: 50px;">{{$bond_redemption->bonds['bond_payout_type']}}</td>
                            <td style="width: 50px;">{{$bond_redemption->bonds['interest_rate']}}%</td>
                            <td style="width: 50px;">{{number_format($bond_redemption['realised_profit'], 2)}}</td>
                            <td>{{$bond_redemption->bonds['bond_receipt']}}</td>
                            <td>{{$bond_redemption->bonds['credit_rating']}}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                @endif

</main>
<footer>
    <div id="footer-view">
        <p id="footer-info">Disclaimer: Mutual Fund investments are subject to market risks, read all scheme related documents carefully before investing.</p>
    </div>
</footer>
</body>
</html>
