<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>
        <!DOCTYPE html>
<html>
<head>
    <title>Transaction PDF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    {{--<link rel="stylesheet" href="css/bootstrap.min.css">--}}
    <link rel="stylesheet" href="css/cispdf.css">

    <style>
        table{
            border-collapse: collapse;
        }

        body{
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        #address-div p{
            margin-bottom: 0px !important;
            padding-bottom: 0px;
        }
    </style>
</head>


<body>
<header>
    <div id="header-view">
        <img class="logo" src = "icons/pro_logo.png"/>
        <div id="address-div">
            <p class="m-b-0">Level 1, No 1, Balaji First Avenue,</p>
            <p class="m-b-0"  style="margin-top: 2px;">T.Nagar,</p>
            <p class="m-b-0" style="margin-top: 2px;">Chennai - 600017</p>
            <p class="m-b-0" style="margin-top: 2px;">Ph: +91 8825888200</p>
        </div>
    </div>
</header>
<div id="info-div">
    <div id="title-div">
        <p id="name" style="padding-bottom: 5px;"><strong>Transaction Summary</strong> <span style="font-size: 12px; font-weight: normal !important;">(from {{date('d-m-Y',strtotime($fromDate))}} to {{date('d-m-Y',strtotime($toDate))}})</span></p>
    </div>
    <div id="user-info">
        <div id="user-info-one">
            <p class="m-b-0" style="margin-top: 5px;"><strong>Name of Investor       : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$name}}</span></p>
            <p class="m-b-0" style="margin-top: 5px;"><strong>Address of Investor    : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$address}}</span></p>
        </div>
        <div id="user-info-two">
            <p class="m-b-0" style="margin-top: 5px;"><strong>E-mail ID      : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$email}}</span></p>
            <p class="m-b-0" style="margin-top: 5px;"><strong>Contact Number : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$contact}}</span></p>
            <p class="m-b-0" style="margin-top: 5px;"><strong>PAN Number     : </strong><span style="font-size: 12px; font-weight: normal !important;">{{$pan}}</span></p>
        </div>
    </div>

</div>
<main>



    <p>
        <strong>INVESTMENTS</strong>
    </p>
    <table class="table table-bordered" style="width: 97%;">
        <thead class="table-head">
        <tr>
            <th>Scheme Name</th>
            <th>Folio Number</th>
            <th>Purchase date</th>
            <th>Amount Invested</th>
            <th>Purchase NAV</th>
            <th>Units</th>
            <th>Current Market Value</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transactions as $memberName => $transaction)

            @foreach($transaction['investment'] as $tran)

                <tr>
                    <td style="width: 250px;">{{$tran->scheme_name}}</td>
                    <td style="width: 80px;">{{$tran->folio_number}}</td>
                    <td style="width: 70px;">{{date('d/m/Y', strtotime($tran->purchase_date))}}</td>
                    <td>{{$fmt->format(round($tran->amount_inv,2))}}</td>
                    <td style="width: 10px;">{{$tran->purchase_nav}}</td>
                    <td style="width: 100px;">{{$fmt->format(round($tran->units, 2))}}</td>
                    <td style="width: 150px;">{{$fmt->format(round($tran->current_market_value, 2))}}</td>
                </tr>
            @endforeach
        @endforeach

        </tbody>
    </table>

        <p>
            <strong>REDEMPTION</strong>
        </p>
        <table class="table table-bordered" style="width: 97%;">
            <thead class="table-head">
            <tr>
                <th>Scheme Name</th>
                <th>Folio Number</th>
                <th>Purchase date</th>
                <th>Withdraw date</th>
                <th>Amount Invested</th>
                <th>Withdrawn Amount</th>
                <th>Purchase NAV</th>
                <th>Units</th>
                <th>Realised Profit/Loss</th>
            </tr>
            </thead>
            <tbody>
            @foreach($transactions as $memberName => $transaction)

                @foreach($transaction['withdraw'] as $tran)

                    <tr>
                        <td style="width: 250px;">{{$tran->scheme_name}}</td>
                        <td style="width: 80px;">{{$tran->folio_number}}</td>
                        <td style="width: 70px;">{{date('d/m/Y', strtotime($tran->purchase_date))}}</td>
                        <td style="width: 70px;">{{date('d/m/Y', strtotime($tran->withdraw_date))}}</td>
                        <td style="width: 80px;">{{$fmt->format(round($tran->invested_amount,2))}}</td>
                        <td style="width: 80px;">{{$fmt->format(round($tran->withdraw_amount,2))}}</td>
                        <td style="width: 10px;">{{$tran->purchase_nav}}</td>
                        <td style="width: 100px;">{{$fmt->format(round($tran->units, 2))}}</td>
                        <td style="width: 150px;">{{$fmt->format(round($tran->realised_profit_or_loss, 2))}}</td>
                    </tr>
                @endforeach
            @endforeach

            </tbody>
        </table>

<div class="underline"></div>
<div class="underline"></div>
<footer>
    <div id="footer-view">
        <p id="footer-info">Disclaimer: Mutual Fund investments are subject to market risks, read all scheme related documents carefully before investing.</p>
    </div>
</footer>
</body>
</html>